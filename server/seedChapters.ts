import { Client } from 'pg';
import dotenv from 'dotenv';
import xlsx from 'xlsx';
//import knex from "knex";

dotenv.config();

export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD, // 唔想Password 寫係code 入面
})

interface Chapters {
    content: string,
    bookId: number,
    chapter: number,
    isOffShelf: boolean,
    publishAfter: string
}

async function main() {

    const workbook = xlsx.readFile('./skywalkDB.xlsx');
    const chapterSheet = workbook.Sheets['chapters'];
    const chapters: Chapters[] = xlsx.utils.sheet_to_json(chapterSheet);

    // console.log(chapters[28]);

    await client.connect();

    for (let chapter of chapters) {
        await client.query(`
        INSERT INTO chapters (id, content, book_id , chapter, is_off_shelf, publish_after) VALUES ($1, $2, $3, $4, $5, $6)
        `,
        [chapters.indexOf(chapter) + 1, chapter.content, chapter.bookId, chapter.chapter, Boolean(chapter.isOffShelf), chapter.publishAfter]);
    };

    await client.end();
}
main();
