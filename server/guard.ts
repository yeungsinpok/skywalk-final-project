import {NextFunction, Request, Response} from 'express';
import {Bearer} from 'permit';
import { JWTPayload } from './models';
import jwtSimple from 'jwt-simple';
import { userService } from './main';
import { logger } from './logger';


const permit = new Bearer({
    query:"access_token"
})


export async function isLoggedIn(req: Request, res: Response, next: NextFunction) {
    let token: string;
    try {
        token = permit.check(req);
        if (!token) {
            res.json({error: 'missing Bearer token in req'});
            return
        }
    }catch(error) {
        res.status(401).json({error: 'failed to decode Bearer token in req'});
        return
    }

    let payload: JWTPayload;
    try{
        let JWT_SECRET = process.env.JWT_SECRET;
        if(!JWT_SECRET) {
            throw new Error ('missing JWT_SECRET in env');
        }
        payload = jwtSimple.decode(token, JWT_SECRET);
    }catch(error) {
        res.status(401).json({error: 'failed to decode JWT token in req'});
        return
    }

    try{
        const user: JWTPayload = await userService.getUserForGuard(payload.id);
        if (user) {
            req.user = user;
            return next();
       } else {
           return res.status(401).json({error: 'permission denied'});
       }
    }catch(error) {
        logger.error(error);
        return;
    }

}