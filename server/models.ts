export interface JWTPayload {
    id: number;
    email?: string;
    name?: string;
    is_admin?: boolean;
    is_writer?: boolean
}

declare global{
    namespace Express{
        interface Request{
            user: JWTPayload
        }
    }
}

export type BookSummaryInfo = {
    book_id: string;
    chapter?: number;
    latest_update: string;
    title: string;
    author: string;
    abstract: string;
    is_end: boolean;
    rank: number;
    cover_image: string;
    chapter_id: number;
    latest_read_chapter?: string;
}

export type BookForm = {
    title: string;
    author: string;
    abstract: string;
    is_end: boolean;
    alien?: string; 
    magic?: string; 
    campus?: string; 
    love?: string; 
    detective?: string; 
    scific?: string; 
    adventure?: string;
    martial?: string;
    fun?: string;
    mystery?: string;
    prose?: string;
    fan?: string;
    romance?: string;
    girls?: string;
    others?: string;
}
