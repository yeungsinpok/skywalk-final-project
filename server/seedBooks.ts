import { Client } from 'pg';
import dotenv from 'dotenv';
import xlsx from 'xlsx';
//import knex from "knex";

dotenv.config();

export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD, // 唔想Password 寫係code 入面
})


interface Books {
    title: string,
    coverImage: string | null,
    isEnd: boolean,
    author: string,
    abstract: string,
    rank: number,
    hitRate: number,
    isExtra: boolean,
    extraTitle: string | null,
    authorId: number,
    isOffShelf: boolean,
    isApproved: boolean,
    isAdult: boolean
}

async function main() {

    const workbook = xlsx.readFile('./skywalkDB.xlsx');
    const usersSheet = workbook.Sheets['books'];
    const books: Books[] = xlsx.utils.sheet_to_json(usersSheet);

    //console.log(Boolean(books[1]['is_end']));

    await client.connect();

    for (let book of books) {
        await client.query(`
        INSERT INTO books (id, title, cover_image , is_end, author, abstract, rank, hit_rate, is_extra, extra_title, author_id, is_off_shelf, is_approved, is_adult)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)
        `,
         [books.indexOf(book) + 1, book.title, book.coverImage, Boolean(book.isEnd), book.author, book.abstract, book.rank, book.hitRate, Boolean(book.isExtra), book.extraTitle, book.authorId, Boolean(book.isOffShelf), Boolean(book.isApproved), Boolean(book.isAdult)]);
    };

    await client.end();
}
main();
