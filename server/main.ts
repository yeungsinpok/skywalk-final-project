import express from 'express';
import path from 'path';
// import expressSession from 'express-session';
import multer from 'multer';
import http from 'http';
import {Server as SocketIO} from "socket.io";
import Knex from 'knex';
import dotenv from 'dotenv';
// import grant from 'grant';
import { print } from 'listening-on';
import cors from 'cors';
import { UserService } from './services/UserService';
import { UserController } from './controllers/UserControllers';
import { ContentService } from './services/ContentService';
import { ContentController } from './controllers/ContentControllers';
import { DiscussService } from './services/DiscussService';
import { DiscussController } from './controllers/DiscussControllers';
import { ViewService } from './services/ViewService';
import { ViewController } from './controllers/ViewControllers';
import { CollectService } from './services/CollectService';
import { CollectController } from './controllers/CollectControllers';
import { AdminService } from './services/AdminService';
import { AdminController } from './controllers/AdminControllers';
import { SearchService } from './services/SearchService';
import { SearchController } from './controllers/SearchControllers';




dotenv.config()
const knexConfigs = require('./knexfile');
const configMode = process.env.NODE_ENV || "development";
if (!configMode) {
  throw new Error ('missing NODE ENV in process.env');
}
const knexConfig = knexConfigs[configMode]
export const knex = Knex(knexConfig)

export const app = express();
const server = new http.Server(app);
export const io = new SocketIO(server);

app.use((req,res,next)=>{
  console.log('log all:', req.method, req.url)
  next()
})

app.use(cors());
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ extended: true, limit: "50mb" }));


// const sessionMiddleware = expressSession({
//     secret: 'Skywalk novel typescript express server',
//     resave:true,
//     saveUninitialized:true,
//     cookie:{secure:false}
//   });

//   app.use(sessionMiddleware);

  export const storageProtected = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve('./public/uploads/image'));
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
  });
  export const uploadProtected = multer({ storage: storageProtected });

  // export const grantExpress = grant.express({
  //   "defaults":{
  //       "origin": "http://localhost:8080",
  //       "transport": "session",
  //       "state": true,
  //   },
  //   "google":{
  //       "key": process.env.GOOGLE_CLIENT_ID || "",
  //       "secret": process.env.GOOGLE_CLIENT_SECRET || "",
  //       "scope": ["profile","email"],
  //       "callback": "/login/google"
  //   }
  // });

export const userService = new UserService(knex);
export const userController = new UserController(userService);
export const contentService = new ContentService(knex);
export const contentController = new ContentController(contentService);
export const discussService = new DiscussService(knex);
export const discussController = new DiscussController(discussService);
export const viewService = new ViewService(knex);
export const viewController = new ViewController(viewService);
export const collectService = new CollectService(knex);
export const collectController = new CollectController(collectService);
export const adminService = new AdminService(knex);
export const adminController = new AdminController(adminService);
export const searchService = new SearchService(knex);
export const searchController = new SearchController(searchService);


import { routes } from './routes';

app.use('/', routes);

// app.use(express.static('../app/build'))
app.use(express.static('/app/build'));
app.use(express.static('./public'));

app.use((req,res,next)=>{
  console.log('catch all:', req.method, req.url)
  if(req.method==='GET'){
    res.sendFile(path.resolve('/app/build/index.html'))
    return
  }
  next()
})


const PORT = process.env.PORT;
if(!PORT) {
  throw new Error('missing PORT in env');
}
let port = +PORT
server.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`);
  print(port);
})