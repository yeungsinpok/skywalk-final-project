import express from 'express';
import { contentController, userController, viewController, discussController, collectController, adminController, searchController } from './main';
import { isLoggedIn } from './guard';
import { uploadProtected } from './main';

export const routes = express.Router();


routes.post('/login/password', userController.loginWithPassword);
routes.post('/login/facebook', userController.loginWithFacebook);
routes.post('/register', userController.register);

routes.post('/view/add_book', viewController.addBookRecord);
routes.post('/view/add_chapter', isLoggedIn, viewController.addChapterRecord);
//routes.get('/view/count_book', viewController.countBookView);

routes.get('/comment/book/:bookId', discussController.listComments);
routes.post('/comment/add/:bookId', discussController.addComment);

routes.get('/book_content/latest_update_list/offsetNextId/:offsetNextId/nextId/:nextId', contentController.loadBookSummary);
routes.get('/book_content/number_book', contentController.getNoOfBook);
routes.get('/book_content/rank', contentController.getRank);
routes.get('/book_content/:chapterId', contentController.showChapterContent);
routes.get('/book_content_next/bookId/:bookId/chapterId/:chapterId', contentController.showNextChapterContent);
routes.get('/book_content_prev/bookId/:bookId/chapterId/:chapterId', contentController.showPrevChapterContent);
routes.get('/book_content/chapter_group/:bookId', contentController.getChapterGroup);

// TK routes start -----------------------------------------------------------------------------------------------------
routes.get('/admin/adv_list', adminController.getAdvRecord);
routes.post('/admin/adv_list/up/:advId', adminController.upAdvRecord);
routes.post('/admin/adv_list/takedown/:advId', adminController.takeDownAdvRecord);
routes.get('/admin/adv_list/random', adminController.showRandomAdv);
//writer new book
routes.get('/admin/chapter_list/:bookId/:chapterId', adminController.getAdvRecord);
routes.post('/admin/book_list/on_shelf', adminController.updateIsOffShelfTrue);
routes.post('/admin/book_list/off_shelf', adminController.updateIsOffShelfFalse);

// routes.post('/writer/book_list/upload/', userController.addNewBook);
// TK routes end --------------------------------------------------------------------------------------------------------


// Wilson routes start -----------------------------------------------------------------------------------------------------
routes.get('/book_content/book_view/:userId/:offsetNextId/:nextId', contentController.getBookViewRecord);
routes.get('/book_content/writer_book_record/:userId', contentController.getWriterBookRecord);
routes.get('/book_content/bookId/:bookId/chapter/:chapter', contentController.getWriterChapterRecord);
routes.get('/book_content/bookId/:bookId', contentController.getSingleBookInfo);
routes.post('/book_content/upload_book', isLoggedIn, uploadProtected.single('cover_image'), contentController.uploadBook);
routes.post('/book_content/upload_adv_photo', isLoggedIn, uploadProtected.single('image'), contentController.uploadAdv);
routes.post('/book_content/upload_chapter', isLoggedIn, contentController.uploadChapter);
routes.post('/book_content/update_edited_chapter', isLoggedIn, contentController.updateChapter);
// Wilson routes end --------------------------------------------------------------------------------------------------------


// Enson routes start -----------------------------------------------------------------------------------------------------

routes.get('/collection/list/:userId', collectController.getCollectionRecord);
routes.post('/collection/add/:userId/:bookId', collectController.addCollectionBookRecord);
routes.delete('/collection/remove/:userId/:bookId', collectController.removeCollectionBookRecord);
routes.get('/collection/check/:userId/:bookId', collectController.checkCollectedRecord);

// admin
routes.get('/admin/user_list', adminController.getUserRecord);
routes.get('/admin/writer_list', adminController.getWriterRecord);
routes.delete('/admin/user_list/remove/:userId', adminController.removeUserRecord);
routes.post('/admin/writer_list/disable_writer/:userId', adminController.disableWriterRecord);
routes.post('/admin/writer_list/add_writer/:userId', adminController.addWriterRecord);
routes.delete('/comment/remove', adminController.removeCommentRecord);

// admin edit book
routes.get('/book_content/admin_book_record/:userId', contentController.adminGetWriterBookRecord);
routes.get('/admin/off_shelf_book_list', adminController.getOffShelfBookRecord);

// searching related 
routes.get('/search/by_category_id/:categoryId', searchController.searchBooksByCatId);
routes.post('/search/by_key_word', searchController.searchBooksByKeyWord);

// Enson routes end --------------------------------------------------------------------------------------------------------