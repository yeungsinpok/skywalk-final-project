#!/usr/bin/bash
set -e
set -o pipefail
set -x

mkdir -p ./etc/nginx/sites-available
scp skywalk:/etc/nginx/sites-available/default ./etc/nginx/sites-available