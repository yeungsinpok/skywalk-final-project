#!/usr/bin/bash
set -e
set -o pipefail
set -x

docker build -t skywalk-web-image .
docker save skywalk-web-image | zstd | ssh skywalk "unzstd | docker load"
scp docker-compose.yml skywalk:~/
ssh skywalk "docker-compose up -d"