#!/usr/bin/bash
set -e
set -o pipefail
set -x

mkdir -p ./etc/nginx/sites-available
scp ./etc/nginx/sites-available/default skywalk:/etc/nginx/sites-available/default
# sudo nginx -t
# sudo service nginx restart