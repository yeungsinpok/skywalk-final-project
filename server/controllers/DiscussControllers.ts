import { Request, Response } from 'express';
import { DiscussService } from '../services/DiscussService';
import { logger } from '../logger';

export class DiscussController {
    constructor(private discussService: DiscussService) { }
    listComments = async (req: Request, res: Response) => {
        try {
            //Get comments for books with book_id. Route is /comment/book/:bookId
            let {bookId} = req.params;            
            const commentRows = await this.discussService.getDiscuss(parseInt(bookId));
            console.log(commentRows);
            res.status(200).json({commentRows});
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    addComment = async (req: Request, res: Response) => {
        
        try {
             const bookId = req.params.bookId;
             const {userId, comment} = req.body
             console.log(req.body)
             const addCommentResult = await this.discussService.createDiscuss(userId,comment,bookId)
             res.status(200).json({ addCommentResult })
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }
}