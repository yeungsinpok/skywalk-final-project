import { Request, Response } from 'express';
import { CollectService } from '../services/CollectService';
import { logger } from '../logger';

export class CollectController{

    constructor(private collectService: CollectService) { }

    getCollectionRecord = async (req: Request, res: Response) => {
        try{
            let userId = req.params.userId;
            const collectedBookList = await this.collectService.getCollectionBook(parseInt(userId));
            console.log(collectedBookList);
            res.status(200).json({collectedBookList});
        } catch(e) {
            logger.error(e);
            res.json({error: (e as Error).toString()});
        } 
    }

    addCollectionBookRecord = async (req: Request, res: Response) => {
        try{
            let userId = req.params.userId;
            let bookId = req.params.bookId;
            await this.collectService.addCollectionBook(parseInt(userId),parseInt(bookId));
            res.status(200).json({success: false});
        } catch(e) {
            logger.error(e);
            res.json({error: (e as Error).toString()});
        } 
    }

    removeCollectionBookRecord = async (req: Request, res: Response) => {
        try{
            let userId = req.params.userId;
            let bookId = req.params.bookId;
            await this.collectService.removeCollectionBook(parseInt(userId),parseInt(bookId));
            console.log("hello");
            res.status(200).json({message: 'delete success'});
        } catch(e) {
            logger.error(e);
            res.json({error: (e as Error).toString()});
        } 
    }

    checkCollectedRecord  = async (req: Request, res: Response) => {
        try{
            let userId = req.params.userId;
            let bookId = req.params.bookId;
            const result = await this.collectService.checkCollected(parseInt(userId),parseInt(bookId));
            const checkCollectedRecord = result.length > 0 ;
            res.status(200).json({checkCollectedRecord});
        } catch(e) {
            logger.error(e);
            res.json({error: (e as Error).toString()});
        } 
    }
}