import { Request, Response } from 'express';
// import { User } from '../services/models';
// import fetch from 'node-fetch';
import { UserService } from '../services/UserService';
import { logger } from '../logger';

export class UserController {

    constructor(private userService: UserService) { }

    loginWithPassword = async (req: Request, res: Response) => {
        let {email, password} = req.body;
        console.log("user controller:", email, password);
        if (!email) {
            res.status(400).json({error: 'missing username input'});
            return;
        }
        if (!password) {
            res.status(400).json({error: 'missing password input'});
            return;
        }
        try{
            let token = await this.userService.createTokenWithPassword(email, password);
            res.status(200).json({ token })

        } catch(e) {
            logger.error(e);
            res.json({error: (e as Error).toString()});
        }    
    }

    loginWithFacebook = async (req: Request, res: Response) => {
        console.log("i am in login with facebook");
        let {accessToken} = req.body;
        if (!accessToken) {
            res.status(400).json({error: 'missing access token input'});
            return;
        }
        try{
            let token = await this.userService.createTokenWithFacebook(accessToken);
            res.status(200).json({ token })

        } catch(e) {
            logger.error(e);
            res.json({error: (e as Error).toString()});
        }    
    }

    register = async (req: Request, res: Response) => {
        let {name, email, password} = req.body;
        if (!name) {
            res.status(400).json({error: 'missing name input'});
            return;
        }
        if (!email) {
            res.status(400).json({error: 'missing username input'});
            return;
        }
        if (!password) {
            res.status(400).json({error: 'missing password input'});
            return;
        }
        try{
            let newUserInfo = await this.userService.createNewUser(name, email, password);
            if (newUserInfo) {
                console.log('New user info:',newUserInfo);
                const token = await this.userService.createTokenWithPassword((newUserInfo[0] as string), password);
                res.status(200).json({ token })
            }
        } catch(e) {
            logger.error(e);
            res.json({error: (e as Error).toString()});
        }    
    }

    // addNewBook = async (req: Request, res: Response) => {
    //     let {title, coverImage, } = req.body;
    //     if (!name) {
    //         res.status(400).json({error: 'missing name input'});
    //         return;
    //     }
    //     if (!email) {
    //         res.status(400).json({error: 'missing username input'});
    //         return;
    //     }
    //     if (!password) {
    //         res.status(400).json({error: 'missing password input'});
    //         return;
    //     }
    //     try{
    //         let newUserInfo = await this.userService.createNewBook(name, email, password);
    //         if (newUserInfo) {
    //             console.log('New user info:',newUserInfo);
    //             const token = await this.userService.createTokenWithPassword((newUserInfo[0] as string), password);
    //             res.status(200).json({ token })
    //         }
    //     } catch(e) {
    //         logger.error(e);
    //         res.json({error: (e as Error).toString()});
    //     }    
    // }
}