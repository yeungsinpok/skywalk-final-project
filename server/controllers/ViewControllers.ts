import { Request, Response } from 'express';
import { ViewService } from '../services/ViewService';
import { logger } from '../logger';

export class ViewController{

    constructor(private viewService: ViewService) { }

    addBookRecord = async (req: Request, res: Response) => {
        try{
            let {userId, bookId} = req.body;
            const bookViewAdded = await this.viewService.addBookView(userId,bookId);
            res.json(bookViewAdded);
        } catch(e) {
            logger.error(e);
            res.json({error: (e as Error).toString()});
        } 
    }

    addChapterRecord = async (req: Request, res: Response) => {
        try{
            const { userId, bookId, chapter } = req.body;
            await this.viewService.addChapterView(userId, bookId, chapter);
            res.status(200).json({success: true});
        } catch(e) {
            logger.error(e);
            res.json({error: (e as Error).toString()});
        } 
    }

    // countBookView = async (req: Request, res: Response) => {
    //     try{
    //         const countBook = await this.viewService.bookViewCount();

    //         // const priority = countBook.map((book)=>{

    //         // })
    //         res.json(countBook);
    //     } catch(e) {
    //         logger.error(e);
    //         res.json({error: (e as Error).toString()});
    //     } 
    // }
}