import { Request, Response } from 'express';
import { logger } from '../logger';
import { AdminService } from '../services/AdminService';


export class AdminController {

    constructor(private adminService: AdminService) { }

    getUserRecord = async (req: Request, res: Response) => {
        try {
            const userList = await this.adminService.getUserList();
            console.log(userList)
            res.status(200).json({ userList });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    getWriterRecord = async (req: Request, res: Response) => {
        try {
            const writerList = await this.adminService.getWriterList();
            console.log(writerList)
            res.status(200).json({ writerList });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    getAdvRecord = async (req: Request, res: Response) => {
        try {
            let advList = await this.adminService.getAdvList();
            console.log(advList);
            res.status(200).json({ advList });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    removeUserRecord = async (req: Request, res: Response) => {
        try {
            let userId = req.params.userId;
            await this.adminService.deleteUser(parseInt(userId));
            res.status(200).json({ success: true });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }
    addWriterRecord = async (req: Request, res: Response) => {
        try {
            let userId = req.params.userId;
            await this.adminService.enableWriter(parseInt(userId));
            res.status(200).json({ success: true });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    disableWriterRecord = async (req: Request, res: Response) => {
        try {
            let userId = req.params.userId;
            await this.adminService.disableWriter(parseInt(userId));
            res.status(200).json({ success: true });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }


    upAdvRecord = async (req: Request, res: Response) => {
        try {
            let advId = req.params.advId;
            await this.adminService.enableAdv(parseInt(advId));
            res.status(200).json({ success: true });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    takeDownAdvRecord = async (req: Request, res: Response) => {
        try {
            let advId = req.params.advId;
            await this.adminService.disableAdv(parseInt(advId));
            res.status(200).json({ success: true });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    updateIsOffShelfTrue = async (req: Request, res: Response) => {
        try {
            let { bookId } = req.body;
            console.log("bookId:", bookId);
            await this.adminService.onBookShelf(parseInt(bookId));
            res.status(200).json({ success: true });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    updateIsOffShelfFalse = async (req: Request, res: Response) => {
        try {
            let { bookId } = req.body;
            console.log("bookId:", bookId);
            await this.adminService.offBookShelf(parseInt(bookId));
            res.status(200).json({ success: true });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    showRandomAdv = async (req: Request, res: Response) => {
        try {
            let randomAd = await this.adminService.getRandomAdv();
            console.log(randomAd)
            res.status(200).json({ randomAd });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    getOffShelfBookRecord = async (req: Request, res: Response) => {
        try {
            const offShelfBookList = await this.adminService.getOffShelfBookList();
            console.log(offShelfBookList)
            res.status(200).json({ offShelfBookList });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    deleteUserRecord = async (req: Request, res: Response) => {
        try {
            const offShelfBookList = await this.adminService.getOffShelfBookList();
            console.log(offShelfBookList)
            res.status(200).json({ offShelfBookList });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

removeCommentRecord = async (req: Request, res: Response) => {
    try{
        let { commentId }  = req.body;
        await this.adminService.removeComment(commentId);
        res.status(200).json({message: 'delete success'});
    } catch(e) {
        logger.error(e);
        res.json({error: (e as Error).toString()});
    } 
}

}