import { Request, Response } from 'express';
import { SearchService } from '../services/SearchService';
import { logger } from '../logger';

export class SearchController {

    constructor(private searchService: SearchService) { }

    searchBooksByCatId = async (req: Request, res: Response) => {
        try {
            let category  = req.params.categoryId;
            const searchByCatResult = await this.searchService.searchByCatId(category);
            res.status(200).json({searchByCatResult});
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }


    searchBooksByKeyWord = async (req: Request, res: Response) => {
        try {
            let { word, userId }  = req.body;
            // console.log(req.body)

            console.log("word:", word);
            const searchByKeyWordResult = await this.searchService.searchByKeyWord(word);
            if(word && userId) {
                await this.searchService.recordSearchRec(word, userId);
            }
            res.status(200).json({searchByKeyWordResult});
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }
    
}