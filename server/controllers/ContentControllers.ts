import { Request, Response } from 'express';
import { ContentService } from '../services/ContentService';
import { logger } from '../logger';
import { BookSummaryInfo } from '../models';

export class ContentController {

    constructor(private contentService: ContentService) { }

    loadBookSummary = async (req: Request, res: Response) => {
        try {
            const offsetNextId = req.params.offsetNextId;
            const nextId = req.params.nextId;
            let bookSummary = await this.contentService.selectBookSummary(parseInt(offsetNextId), parseInt(nextId));
            console.log(bookSummary);
            res.status(200).json({ bookSummary });

        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }

    }

    getNoOfBook = async (req: Request, res: Response) => {
        try {
            let numberOfBook = await this.contentService.getNumberOfBook();
            res.status(200).json(numberOfBook[0]);

        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    getRank = async (req: Request, res: Response) => {
        try {
            console.log("Hello");
            let rankList = await this.contentService.getRankInfo();
            console.log('Rank list:', rankList);
            res.status(200).json({ rankList });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    showChapterContent = async (req: Request, res: Response) => {
        try {
            let chapter_id = req.params.chapterId;
            const chapterRows = await this.contentService.getChapterContent(parseInt(chapter_id));
            console.log(chapterRows);
            res.status(200).json({ chapterRows });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    showPrevChapterContent = async (req: Request, res: Response) => {
        try {
            let bookId = req.params.bookId;
            let chapterId = req.params.chapterId;
            const chapterRows = await this.contentService.getPrevChapterContent(parseInt(bookId), parseInt(chapterId));
            res.status(200).json({ chapterRows });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    showNextChapterContent = async (req: Request, res: Response) => {
        try {
            let bookId = req.params.bookId;
            let chapterId = req.params.chapterId;
            const chapterRows = await this.contentService.getNextChapterContent(parseInt(bookId), parseInt(chapterId));
            res.status(200).json({ chapterRows });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    getChapterGroup = async (req: Request, res: Response) => {
        try {
            let { bookId } = req.params;
            const chapterGroup = await this.contentService.getChapterGroupContent(parseInt(bookId));
            res.status(200).json({ chapterGroup });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    // TK controller start -----------------------------------------------------------------------------------------------------
    // getNoOfChapter = async (req: Request, res: Response) => {
    //     try {
    //         let bookId = req.params.bookId;
    //         let numberOfChapter = await this.contentService.getNumberOfChapter(bookId);
    //         res.status(200).json(numberOfChapter[0]);

    //     } catch (e) {
    //         logger.error(e);
    //         res.json({ error: (e as Error).toString() });
    //     }
    // }

    // TK controller end --------------------------------------------------------------------------------------------------------


    // Wilson controller start -----------------------------------------------------------------------------------------------------
    getBookViewRecord = async (req: Request, res: Response) => {
        try {
            let user_id = req.params.userId;
            let offsetNextId = req.params.offsetNextId;
            let nextId = req.params.nextId;
            let bookViewRec: BookSummaryInfo[] = await this.contentService.getBookViewRecordByUser(parseInt(user_id), parseInt(offsetNextId), parseInt(nextId));
            console.log('book view rec:', bookViewRec);
            const bookIdArr = bookViewRec.map(bookView => {
                return parseInt(bookView.book_id);
            })
            console.log('Book Id Array:', bookIdArr);
            for (let i in bookIdArr) {
                let latestPublishChapter = await this.contentService.getLatestPublishChapter(bookIdArr[i]);
                console.log("latest publish chapter:", latestPublishChapter);
                bookViewRec[i]['chapter'] = latestPublishChapter[0]['chapter'];
                bookViewRec[i]['latest_update'] = latestPublishChapter[0]['latest_update'];
            }
            console.log('book view rec afterward', bookViewRec);

            res.status(200).json({ bookViewRec });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    getWriterBookRecord = async (req: Request, res: Response) => {
        try {
            let userId = req.params.userId;
            const writerBookRec = await this.contentService.getWriterBookRecordByAuthorId(parseInt(userId));
            res.status(200).json({ writerBookRec });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }


    adminGetWriterBookRecord = async (req: Request, res: Response) => {
        try {
            let userId = req.params.userId;
            const writerBookRec = await this.contentService.adminGetWriterBookRecordByAuthorId(parseInt(userId));
            res.status(200).json({ writerBookRec });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    getWriterChapterRecord = async (req: Request, res: Response) => {
        try {
            let bookId = req.params.bookId;
            let chapter = req.params.chapter;
            const writerChapterRec = await this.contentService.getWriterChapterContent(parseInt(bookId), parseInt(chapter));
            res.status(200).json({ writerChapterRec });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    getSingleBookInfo = async (req: Request, res: Response) => {
        try {
            let bookId = req.params.bookId;
            const singleBook = await this.contentService.getSingleBook(parseInt(bookId));
            res.status(200).json({ singleBook });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    uploadBook = async (req: Request, res: Response) => {
        try {
            if (!req.file) throw new Error('Image blob is not found in request')

            const bookInfo = req.body;
            const coverImage = req.file['filename'];

            await this.contentService.uploadBookContent(bookInfo, coverImage);

            res.status(200).json({ success: true });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    uploadAdv = async (req: Request, res: Response) => {
        try {
            let image;
            if (req.file) {
                const bookInfo = req.body;
                image = req.file['filename'];
                console.log('Book Info:', bookInfo);
                console.log("Adv image:", image);
                await this.contentService.uploadAdvPhoto(bookInfo, image);
            }
            res.status(200).json({ success: true });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    uploadChapter = async (req: Request, res: Response) => {
        try {
            const { bookId, chapter, chapterContent } = req.body;

            console.log('req.body', req.body);
            console.log('bookId:', bookId);
            console.log('chapter:', chapter);
            console.log('chapterContent:', chapterContent['editorHtml']);
            let nextChapter: number = chapter + 1;
            await this.contentService.uploadChapterContent(bookId, chapter, nextChapter, chapterContent['editorHtml']);
            res.status(200).json({ success: true });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    updateChapter = async (req: Request, res: Response) => {
        try {
            const { bookId, chapter, chapterContent } = req.body;

            console.log('req.body', req.body);
            console.log('bookId:', bookId);
            console.log('chapter:', chapter);
            console.log('chapterContent:', chapterContent['editorHtml']);
            await this.contentService.updateChapterContent(bookId, chapter, chapterContent['editorHtml']);
            res.status(200).json({ success: true });
        } catch (e) {
            logger.error(e);
            res.json({ error: (e as Error).toString() });
        }
    }

    // Wilson controller end --------------------------------------------------------------------------------------------------------


    // Enson controller start -----------------------------------------------------------------------------------------------------


    // Enson controller end --------------------------------------------------------------------------------------------------------


}