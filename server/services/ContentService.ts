import { Knex } from "knex";
import { BookForm } from "../models";
// import { SqlTypeFile } from 'gen-sql-type';

// const sqlTypeFile = SqlTypeFile.withPrefix(__filename);

export class ContentService {

    constructor(private knex: Knex) { }

    async selectBookSummary(offsetNextId: number, nextId: number) {
        try {
            let result = (await this.knex.raw(`SELECT books.id as book_id, chapter, chapters.updated_at as latest_update, title, author, abstract, is_end,  
            rank, cover_image, chapters.id as chapter_id  
                FROM books 
                INNER JOIN chapters ON books.id = chapters.book_id 
                INNER JOIN (SELECT chapters.book_id as chapters_book_id, MAX(updated_at) maxUpdatedAt FROM chapters GROUP BY chapters.book_id) a ON chapters.book_id = a.chapters_book_id AND chapters.updated_at = a.maxUpdatedAt WHERE books.is_off_shelf = false ORDER BY latest_update DESC LIMIT ? OFFSET ?`, [offsetNextId, nextId])).rows;
            return result;

        } catch (e) {
            throw new Error(e.toString());
        }
    }

    async getNumberOfBook() {
        try {
            let result = await this.knex('books').count('id').where('books.is_off_shelf', 'false');
            return result;

        } catch (e) {
            throw new Error(e.toString());
        }
    }

    async getRankInfo() {
        try {
            let result = (await this.knex.raw(`SELECT rank_num, books.id as book_id, title, author, books.hit_rate as hit_rate, chapter, 
            chapters.updated_at as latest_update, abstract, is_end, 
            rank, cover_image, chapters.id as chapter_id 
            FROM books 
            INNER JOIN chapters ON books.id = chapters.book_id 
            INNER JOIN (SELECT chapters.book_id as chapters_book_id, MAX(updated_at) maxUpdatedAt 
            FROM chapters GROUP BY chapters.book_id) 
            a ON chapters.book_id = a.chapters_book_id AND chapters.updated_at = a.maxUpdatedAt
            INNER JOIN ( SELECT books.id as book_id, books.hit_rate as hit_rate, RANK() OVER 
            (ORDER BY books.hit_rate DESC) rank_num FROM books)
            b ON books.id = b.book_id AND books.hit_rate = b.hit_rate
            WHERE books.is_off_shelf = false LIMIT 10`)).rows;
            return result;

        } catch (e) {
            throw new Error(e.toString());
        }
    }

    //async getChapterContent(chapterId: number) {
    async getChapterContent(chapterId: number) {
        try {
            let result = await this.knex
                .select('book_id', 'id as chapter_id', 'chapter', 'content')
                .from('chapters')
                .where('id', chapterId);
            // let result = (await this.knex.raw(`SELECT id, chapter, content FROM chapters WHERE chapters.id = ? `, [chapterId]));
            //拎某本書(邊本書問state)的第一章，當click
            // let result =  await this.knex
            // .select('chapters.id', 'books.title', 'chapters.chapter', 'chapters.content')
            // .table('chapters')
            // .innerJoin('books', 'chapters.book_id', '=', 'books.id')
            // .orderBy('chapters.id')
            // .limit(1).
            // offset(1)

            return result;

        } catch (e) {
            throw new Error(e.toString());
        }
    }

    async getNextChapterContent(bookId: number, chapterId: number) {
        try {
            let result = await this.knex.select('books.id as book_id', 'chapters.id as chapter_id', 'chapters.chapter', 'chapters.content')
                .from('books')
                .innerJoin('chapters', 'books.id', 'chapters.book_id')
                .where('books.id', bookId).andWhere('chapters.id', '>', chapterId)
                .orderBy('chapters.id')
                .limit(1);

            // SELECT books.id as book_id, chapters.id as chapter_id, chapter, content   
            //     FROM chapters 
            //     INNER JOIN books ON books.id = chapters.book_id WHERE book_id
            //     = 4 AND chapters.id > 22 ORDER BY chapters.id LIMIT 1;
            return result;
        } catch (e) {
            throw new Error(e.toString());
        }
    }

    async getPrevChapterContent(bookId: number, chapterId: number) {
        try {
            let result = await this.knex.select('books.id as book_id', 'chapters.id as chapter_id', 'chapters.chapter', 'chapters.content')
                .from('books')
                .innerJoin('chapters', 'books.id', 'chapters.book_id')
                .where('books.id', bookId).andWhere('chapters.id', '<', chapterId)
                .orderBy('chapters.id', 'desc')
                .limit(1);

            // SELECT books.id as book_id, chapters.id as chapter_id, chapter, content   
            //     FROM chapters 
            //     INNER JOIN books ON books.id = chapters.book_id WHERE book_id
            //     = 4 AND chapters.id > 22 ORDER BY chapters.id LIMIT 1;
            return result;
        } catch (e) {
            throw new Error(e.toString());
        }
    }

    async getChapterGroupContent(bookId: number) {
        try {
            let result = await this.knex.select('id as chapter_id', 'chapter').from('chapters').where('book_id', bookId).orderBy('chapter', 'desc');
            return result;

        } catch (e) {
            throw new Error(e.toString());
        }
    }

    // TK service start -----------------------------------------------------------------------------------------------------

    // TK service end --------------------------------------------------------------------------------------------------------


    // Wilson service start -----------------------------------------------------------------------------------------------------
    async getBookViewRecordByUser(user_id: number, offsetNextId: number, nextId: number) {

        console.log("userId:", user_id, "offset:", offsetNextId, "nextId:", nextId);
        try {

            `SELECT chapter_view.user_id as user_id, books.id as book_id, chapter_view.chapter_id as chapter_id,    
            chapter as latest_read_chapter, rank, title, author, abstract, is_end,
            chapters.updated_at as latest_update, cover_image, chapter_view.created_at as chapter_view_created_at   
            FROM books 
            INNER JOIN chapters ON books.id = chapters.book_id 
            INNER JOIN chapter_view on chapters.id = chapter_view.chapter_id
            INNER JOIN (SELECT chapter_view.chapter_id as chapter_view_chapter_id, MAX(chapter_view.created_at) maxCreatedAt 
            FROM chapter_view GROUP BY chapter_view.chapter_id) 
            a ON chapter_view.chapter_id = a.chapter_view_chapter_id AND chapter_view.created_at = a.maxCreatedAt
            INNER JOIN (SELECT books.id as book_id, MAX(chapters.chapter) maxChapter
            FROM books
            INNER JOIN chapters ON books.id = chapters.book_id 
            INNER JOIN chapter_view on chapters.id = chapter_view.chapter_id GROUP BY books.id)
            b on books.id = b.book_id AND chapters.chapter = b.maxChapter
            WHERE chapter_view.user_id = ? ORDER BY chapter_view.created_at DESC LIMIT ? OFFSET ?`


            let result = (await this.knex.raw(`with user_view as (
                select
                  chapter_view.chapter_id
                , chapter_view.created_at
                , chapters.book_id
                , chapters.chapter as latest_read_chapter
                from chapter_view
                inner join chapters on chapters.id = chapter_view.chapter_id
                where user_id = ?
            )
            
            select
              view.latest_read_chapter
            , view.chapter_id
            , view.created_at
            , books.id as book_id
            , books.*
            from user_view as view
            left join user_view as newer
              on view.created_at < newer.created_at
             and view.book_id = newer.book_id
            inner join books on books.id = view.book_id
            where newer.created_at is null LIMIT ? OFFSET ?`, [user_id, offsetNextId, nextId])).rows;
            return result;

        } catch (e) {
            throw new Error(e.toString());
        }
    }

    async getLatestPublishChapter(bookId: number) {
        try {
            let result = (await this.knex.raw(`SELECT chapter, chapters.updated_at as latest_update  
            FROM books 
            INNER JOIN chapters ON books.id = chapters.book_id 
            INNER JOIN (SELECT chapters.book_id as chapters_book_id, MAX(updated_at) maxUpdatedAt 
            FROM chapters GROUP BY chapters.book_id) 
            a ON chapters.book_id = a.chapters_book_id AND chapters.updated_at = a.maxUpdatedAt 
            WHERE books.id = ?`, bookId)).rows;
            return result;
        } catch (e) {
            throw new Error(e.toString());
        }
    } 

    async getWriterBookRecordByAuthorId(user_id: number) {
        try {
            let result = (await this.knex.raw(`SELECT author_id, books.id as book_id, chapters.id as chapter_id,   
            chapter, title, author, is_end, cover_image, json_agg(category) as category FROM books 
            
            INNER JOIN chapters ON books.id = chapters.book_id 
            INNER JOIN (SELECT chapters.book_id as chapters_book_id, MAX(chapter) maxChapter 
            FROM chapters GROUP BY chapters.book_id) 
            a ON chapters.book_id = a.chapters_book_id AND chapters.chapter = a.maxChapter 
            
            INNER JOIN book_category on books.id = book_category.book_id
            INNER JOIN categories on book_category.category_id = categories.id
            
            WHERE books.author_id = (SELECT id from users WHERE id = ?) GROUP BY books.author_id, books.id, chapters.id,   
            chapter, title, author, is_end,
            chapters.updated_at ORDER BY books.created_at DESC;`, [user_id])).rows;
            return result;

        } catch (e) {
            throw new Error(e.toString());
        }
    }

    async adminGetWriterBookRecordByAuthorId(user_id: number) {
        try {
            let result = (await this.knex.raw(`SELECT author_id, books.id as book_id, chapters.id as chapter_id, books.is_off_shelf as is_off_shelf,   
            chapter, title, author, is_end, cover_image, json_agg(category) as category FROM books 
            
            INNER JOIN chapters ON books.id = chapters.book_id 
            INNER JOIN (SELECT chapters.book_id as chapters_book_id, MAX(chapter) maxChapter 
            FROM chapters GROUP BY chapters.book_id) 
            a ON chapters.book_id = a.chapters_book_id AND chapters.chapter = a.maxChapter 
            
            INNER JOIN book_category on books.id = book_category.book_id
            INNER JOIN categories on book_category.category_id = categories.id
            
            WHERE books.author_id = (SELECT id from users WHERE id = ?) GROUP BY books.author_id, books.id, chapters.id,   
            chapter, title, author, is_end, 
            chapters.updated_at ORDER BY books.created_at DESC;`, [user_id])).rows;
            return result;

        } catch (e) {
            throw new Error(e.toString());
        }
    }


    async getWriterChapterContent(bookId: number, chapter: number) {
        try {
            let result = (await this.knex.raw(`SELECT book_id, id as chapter_id, chapter, content FROM chapters 
            WHERE book_id = ? AND chapter = ?;`, [bookId, chapter])).rows;
            return result;

        } catch (e) {
            throw new Error(e.toString());
        }
    }

    async getSingleBook(bookId: number) {
        try {
            let result = (await this.knex.raw(`SELECT books.id as book_id, chapter, chapters.updated_at as latest_update, title, author, 
            abstract, is_end, rank, cover_image, chapters.id as chapter_id  
            FROM books 
            INNER JOIN chapters ON books.id = chapters.book_id 
            INNER JOIN (SELECT chapters.book_id as chapters_book_id, MAX(updated_at) maxUpdatedAt 
            FROM chapters GROUP BY chapters.book_id) a ON chapters.book_id = a.chapters_book_id 
            AND chapters.updated_at = a.maxUpdatedAt WHERE books.id = ?;`, [bookId])).rows;
            return result;

        } catch (e) {
            throw new Error(e.toString());
        }
    }

    async uploadBookContent(bookInfo: BookForm, coverImage: string) {
        const txn = await this.knex.transaction();
        try{
            console.log("upload book content");
            
            const newBookId = (await txn.raw(`INSERT INTO books (title, cover_image, is_end, author, abstract, rank, hit_rate, is_extra, author_id, is_off_shelf, is_approved, is_adult) VALUES (?, ?, ?, ?, ?, ?, ?, ?, (SELECT id FROM users WHERE name = ?), ?, ?, ?) RETURNING id`, [bookInfo['title'], coverImage, bookInfo['is_end'], bookInfo['author'], bookInfo['abstract'], 0, 0, false, bookInfo['author'], true, false, false])).rows[0]['id'];

            await txn.raw(`INSERT INTO chapters (content, chapter, book_id, is_off_shelf, 
                publish_after) 
                VALUES ('', 0, (SELECT id FROM books WHERE books.id = ?),
                false, NOW()::timestamp)`, [newBookId]);

            console.log("new book id:", newBookId);
            if (bookInfo['alien'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 1]);
            }

            if (bookInfo['magic'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 2]);
            }

            if (bookInfo['campus'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 3]);
            }
            if (bookInfo['love'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 4]);
            }
            if (bookInfo['detective'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 5]);
            }
            if (bookInfo['scific'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 6]);
            }
            if (bookInfo['adventure'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 7]);
            }
            if (bookInfo['martial'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 8]);
            }
            if (bookInfo['fun'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 9]);
            }
            if (bookInfo['mystery'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 10]);
            }
            if (bookInfo['prose'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 11]);
            }
            if (bookInfo['fan'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 12]);
            }
            if (bookInfo['romance'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 13]);
            }
            if (bookInfo['girls'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 14]);
            }
            if (bookInfo['others'] === 'true') {
                await txn.raw(`INSERT INTO book_category (book_id, category_id) VALUES ((SELECT id FROM books WHERE books.id = ?), (SELECT id FROM categories WHERE categories.id = ?))`, [newBookId, 15]);
            }
            await txn.commit();
            return;
        } catch (e) {
            await txn.rollback();
            throw new Error(e.toString());
        }

    }

    async uploadAdvPhoto(bookInfo: BookForm, image: string) {
        const txn = await this.knex.transaction();
        try{
            await txn.raw(`INSERT INTO book_ads (book_id, image, is_active) VALUES ((SELECT id FROM books WHERE title = ?), ?, ?)`, [bookInfo['title'], image, true]);
            await txn.commit();
            return;
        } catch (e) {
            await txn.rollback();
            throw new Error(e.toString());
        }

    }

    async uploadChapterContent(bookId: number, chapter: number, nextChapter: number, chapterContent: string) {
        const txn = await this.knex.transaction();
        try{ 
            await txn.raw(`INSERT INTO chapters (content, chapter, book_id, is_off_shelf, publish_after) VALUES (?, ?, (SELECT id FROM books WHERE id = ?), ?, NOW()::timestamp)`, [chapterContent, nextChapter, bookId, false]);
            if (chapter == 0) {

                await txn.raw(`delete from chapter_view where chapter_id = 
                (select id from chapters where book_id = ? and chapter = ?)`, [bookId, chapter]);

                await txn.raw(`delete from chapters where book_id = ? and chapter = ?`, [bookId, chapter])
            }
            await txn.commit();
            return;
        } catch (e) {
            await txn.rollback();
            throw new Error(e.toString());
        }

    }

    async updateChapterContent(bookId: number, chapter: number, chapterContent: string) {
        const txn = await this.knex.transaction();
        try{ 
            await txn.raw(`UPDATE chapters SET content = ?, updated_at = NOW()::timestamp WHERE chapter = ? AND book_id = (SELECT id from books where id = ?)`, [chapterContent, chapter, bookId]);
            await txn.commit();
            return;
        } catch (e) {
            await txn.rollback();
            throw new Error(e.toString());
        }

    }

    // Wilson service end --------------------------------------------------------------------------------------------------------


    // Enson service start -----------------------------------------------------------------------------------------------------


    // Enson service end --------------------------------------------------------------------------------------------------------
}
