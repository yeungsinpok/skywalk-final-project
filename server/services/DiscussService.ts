import { Knex } from "knex";

export class DiscussService {

    constructor(private knex: Knex) { }

    async getDiscuss(bookId: number) {
        try {
            // commentedBook - Subquery for which book to read comments
            const commentedBook = await this.knex.select('books.id as book_id').from('books').where('id', bookId);
            const commentRows = await this.knex
            .select('books.id as book_id', 'comments.id as comment_id', 'user_id', 'name', 'content', 'comments.created_at as created_at')
            .from('comments')
            .innerJoin('users', 'users.id', '=', 'comments.user_id')
            .innerJoin('books', 'books.id', 'comments.book_id')
            .where('books.id', commentedBook[0]['book_id'])
            .orderBy('comments.created_at', 'desc');
            return commentRows;
        } catch (e) {
            throw new Error(e.toString());
        }
    }

    async createDiscuss(userId: number, content: string, bookId: any ) {
        const txn = await this.knex.transaction();
        try {
            await txn.raw(`INSERT INTO comments (user_id, reply_id, content, book_id) VALUES (?, Null, ? , ?);`, [userId, content, bookId])
            await txn.commit();
            return;
        } catch (e) {
            await txn.rollback();
            throw new Error(e.toString());
        }
    } 
}
