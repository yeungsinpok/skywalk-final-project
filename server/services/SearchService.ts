import { Knex } from "knex";

export class SearchService {

    constructor(private knex: Knex) { }

    async searchByCatId(category: string) {
        try {
            let result = (await this.knex.raw(`SELECT books.id as book_id, category, categories.id as category_id, 
            title, author, chapter, abstract, 
            is_end, rank, cover_image, chapters.id as chapter_id,
            chapters.updated_at as latest_update  FROM categories
            INNER JOIN book_category ON categories.id = book_category.category_id
            INNER JOIN books ON book_category.book_id = books.id
            INNER JOIN chapters ON books.id = chapters.book_id
            INNER JOIN (SELECT chapters.book_id as book_id, MAX(updated_at) as maxUpdatedAt 
            FROM chapters GROUP BY chapters.book_id) a ON chapters.book_id = a.book_id
            AND chapters.updated_at = a.maxUpdatedAt
            WHERE category_id = ? AND books.is_off_shelf = false ORDER BY chapters.updated_at DESC;`, [category])).rows;
            return result;


        } catch (e) {
            throw new Error(e.toString());
        }
    }

    async searchByKeyWord(word: string) {
        try {
            let result = (await this.knex.raw(`SELECT books.id as book_id, json_agg(category) as category, 
            title, author, chapter, abstract, 
            is_end, rank, cover_image, chapters.id as chapter_id,
            chapters.updated_at as latest_update  FROM categories
            INNER JOIN book_category ON categories.id = book_category.category_id
            INNER JOIN books ON book_category.book_id = books.id
            INNER JOIN chapters ON books.id = chapters.book_id
            INNER JOIN (SELECT chapters.book_id as book_id, MAX(updated_at) as maxUpdatedAt 
            FROM chapters GROUP BY chapters.book_id) a ON chapters.book_id = a.book_id
            AND chapters.updated_at = a.maxUpdatedAt
            WHERE (title ilike '%' || ? || '%' OR author ilike '%' || ? || '%') AND books.is_off_shelf = false
            GROUP BY books.id, 
            title, author, chapter, abstract, 
            is_end, rank, cover_image, chapters.id,
            chapters.updated_at 
            ORDER BY chapters.updated_at DESC;`, [word, word])).rows;
            return result;
        } catch (e) {
            throw new Error(e.toString());
        }
    }

    async recordSearchRec(word: string, userId: number) {
        console.log("word:", word, "userId:", userId);
        const txn = await this.knex.transaction();
        try {
            await txn.raw(`INSERT INTO search (user_id, text) VALUES ((SELECT id from users where id = ?), ?)`, [userId, word]);
            await txn.commit()
            return;
        }
        catch (e) {
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

}