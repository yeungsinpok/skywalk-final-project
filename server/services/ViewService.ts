import { Knex } from "knex";
import { logger } from "../logger";

export class ViewService {

    constructor(private knex: Knex) { }

    // async bookView(userId: number) {
    //     try {
    //             const bookViewRecord = await this.knex
    //             .select('title')
    //             .into('book_view')
    //         return addedbook;
    //     } catch (e) {
    //         logger.error(e);
    //         throw new Error(e.toString());            
    //     }
    // }



    async addBookView(userId: number, bookId: number) {
        const txn = await this.knex.transaction();
        try {
            //Subquery to query which book is chosen
            const bookIdSelected = this.knex.select('id').from('books').where('id', bookId)

            const addedbook = await txn.insert({
                user_id: userId,
                book_id: bookIdSelected
            })
                .into('book_view')
                .returning('id');
            await txn.commit();
            return addedbook;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    async addChapterView(userId: number, bookId: number, chapter: number) {
        const txn = await this.knex.transaction();
        try {
            console.log(userId, bookId, chapter);
            const chapterId = (await txn.raw(`SELECT id FROM chapters WHERE book_id = ? AND chapter = ?`, [bookId, chapter])).rows[0]['id'];
           
            const userIdFromDB = (await txn.raw(`SELECT id FROM users WHERE id = ?`, [userId])).rows[0]['id'];
            console.log('chapter Id:', chapterId, 'user id:', userIdFromDB );

            await txn.insert({
                user_id: userIdFromDB,
                chapter_id: chapterId
            })
                .into('chapter_view');
            
            await txn.raw(`UPDATE books SET hit_rate = hit_rate + 1 WHERE id = ?`,[bookId]);

            await txn.commit();
            return;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    // async bookViewCount() {
    //     //return this.knex('book_view').count('book_id')
    //     return this.knex
    //     .select('book_view.book_id', 'books.title', 'books.author')
    //     .from('book_view')
    //     .innerJoin('books', 'book_view.book_id', '=', 'books.id')
    //     .orderBy('book_id', 'desc');
    // }

    // async chapterViewCount() {

    // }

}

