import { Knex } from "knex";
import { logger } from "../logger";

export class CollectService {

    constructor(private knex: Knex) { }

    async getCollectionBook(userId: number) {
        try {
            const collectedBookRows = (await this.knex.raw(`SELECT books.id as book_id, chapters.id as chapter_id,   
            chapter, rank, title, author, abstract, is_end,
            chapters.updated_at as latest_update, cover_image   
            FROM books 
            INNER JOIN collection on books.id = collection.book_id
            INNER JOIN chapters ON books.id = chapters.book_id 
            INNER JOIN (SELECT chapters.book_id as chapters_book_id, MAX(updated_at) maxUpdatedAt 
            FROM chapters GROUP BY chapters.book_id) 
            a ON chapters.book_id = a.chapters_book_id AND chapters.updated_at = a.maxUpdatedAt 
            WHERE collection.user_id = ? ORDER BY collection.created_at DESC;`,[userId])).rows           
            return collectedBookRows;
        } catch (e) {
            logger.error(e);
            throw new Error(e.toString());

        }
    }
 
    async addCollectionBook(userId: number, bookId: number)  {
        const txn = await this.knex.transaction();
        try {
           await txn.insert({
                user_id: userId,
                book_id:  bookId
            })
                .into('collection')
            await txn.commit();
            return;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }
    
    async removeCollectionBook(userId: number, bookId: number) {
        const txn = await this.knex.transaction();
        try {
            await this.knex('collection').where('user_id', userId).where('book_id', bookId).del()
            await txn.commit();
            return;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    async checkCollected(userId: number, bookId: number) {
        try {
           let result =(await this.knex.raw(`SELECT * FROM collection WHERE user_id = ? AND book_id = ?;`, [userId, bookId])).rows;
            return result;
        } catch (e) {
            logger.error(e);
            throw new Error(e.toString());
        }
    }


}

