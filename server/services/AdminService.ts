import { Knex } from "knex";
import { logger } from "../logger";

export class AdminService {

    constructor(private knex: Knex) { }

    async getUserList() {
        const txn = await this.knex.transaction();
        try {
            const userListRows = (await this.knex.raw(`SELECT id, name, email, created_at FROM users WHERE is_admin = FALSE AND is_writer = FALSE ORDER BY id;`)).rows;
            await txn.commit();
            return userListRows;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    async getWriterList() {
        const txn = await this.knex.transaction();
        try {
            const writerListRows = (await this.knex.raw(`SELECT id, name, email, created_at, is_writer FROM users WHERE is_writer = TRUE AND is_admin = FALSE ORDER BY id;`)).rows;
            await txn.commit();
            return writerListRows;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    async getAdvList() {
        const txn = await this.knex.transaction();
        try {
            let result = (await this.knex.raw(`SELECT book_ads.id, book_ads.image, books.title , book_ads.created_at,  book_ads.updated_at, book_ads.is_active FROM book_ads INNER JOIN books ON book_ads.book_id = books.id ORDER BY book_ads.updated_at DESC`)).rows;
            await txn.commit();
            return result;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    async deleteUser(userId: number) {
        const txn = await this.knex.transaction();
        try {
            await this.knex('collection').where('user_id', userId).del();
            await this.knex('search ').where('user_id', userId).del();
            await this.knex('book_view').where('user_id', userId).del();
            await this.knex('chapter_view').where('user_id', userId).del();
            await this.knex('comments').where('user_id', userId).del();
            await this.knex('users').where('id', userId).del();
            await txn.commit();
            return;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    async disableWriter(userId: number) {
        const txn = await this.knex.transaction();
        try {
            await this.knex('users').update({ is_writer: false }).where('id', userId);
            await txn.commit();
            return;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    async enableWriter(userId: number) {
        const txn = await this.knex.transaction();
        try {
            await this.knex('users').update({ is_writer: true }).where('id', userId);
            await txn.commit();
            return;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    async disableAdv(advId: number) {
        const txn = await this.knex.transaction();
        try {
            await this.knex('book_ads').update({ is_active: false }).where('id', advId);
            await this.knex.raw(`UPDATE book_ads set updated_at = NOW()::timestamp WHERE id = ?;`, [advId])
            await txn.commit();
            return;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    async enableAdv(advId: number) {
        const txn = await this.knex.transaction();
        try {
            await this.knex('book_ads').update({ is_active: true }).where('id', advId);
            await this.knex.raw(`UPDATE book_ads set updated_at = NOW()::timestamp WHERE id = ?;`, [advId])
            await txn.commit();
            return;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    async onBookShelf(bookId: number) {
        const txn = await this.knex.transaction();
        try {
            await this.knex('books').update({ is_off_shelf: false }).where('id', bookId);
            await this.knex.raw(`UPDATE books set updated_at = NOW()::timestamp WHERE id = ?;`, [bookId])
            await txn.commit();
            return;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    async offBookShelf(bookId: number) {
        const txn = await this.knex.transaction();
        try {
            await this.knex('books').update({ is_off_shelf: true }).where('id', bookId);
            await this.knex.raw(`UPDATE books set updated_at = NOW()::timestamp WHERE id = ?;`, [bookId])
            await txn.commit();
            return;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    async getRandomAdv() {
        const txn = await this.knex.transaction();
        try {
            // let result = await this.knex
            // Join table拎埋廣告推廣的書名(books.title)，寫埋落去img alt={書名}
            // .select('book_ads.id', 'book_ads.book_id', 'books.title', 'book_ads.image')
            // .innerJoin('books', 'book_ads.id', '=', 'books.id')
            // .where('is_active', true)
            // .orderByRaw('random()')
            // .limit(1);
            let result = (await this.knex.raw(`
            SELECT book_ads.id, book_ads.book_id, books.title, book_ads.image FROM book_ads INNER JOIN books ON book_ads.id = books.id WHERE is_active = TRUE ORDER BY random() LIMIT 1;
            `)).rows;
            await txn.commit();
            return result;
        } catch (e) {
            logger.error(e);
            await txn.rollback();
            throw new Error(e.toString());
        }
    }

    async getOffShelfBookList() {
    const txn = await this.knex.transaction();
    try {
        let result = (await this.knex.raw(`SELECT id, title, author, author_id, created_at, is_off_shelf FROM books WHERE is_off_shelf = TRUE;`)).rows;
        await txn.commit();
        return result;
    } catch (e) {
        logger.error(e);
        await txn.rollback();
        throw new Error(e.toString());
    }
}

async removeComment(commentId: number) {
    const txn = await this.knex.transaction();
    try {
        let result = (await this.knex.raw(`DELETE from comments where id = ?;`,[commentId])).rows;
        await txn.commit();
        return result;
    } catch (e) {
        logger.error(e);
        await txn.rollback();
        throw new Error(e.toString());
    }
}

}