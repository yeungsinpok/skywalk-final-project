
import { Knex } from "knex";
import { checkPassword, hashPassword } from "../hash";
import { JWTPayload } from "../models";
import jwtSimple from 'jwt-simple';
import fetch from 'node-fetch';
import type { ReactFacebookLoginInfo } from 'react-facebook-login';
import { logger } from "../logger";
import crypto from "crypto";


export class UserService {

    constructor(private knex: Knex) { }

    async createTokenWithPassword(email: string, password: string) {
        try {
            let row = await this.knex.select('id', 'name', 'password', 'is_admin', 'is_writer').from('users').where('email', email);

            if (!row) {
                throw new Error('Wrong username or password, no row');
            }

            
            if (!(await checkPassword(password, row[0].password))) {
                throw new Error('Wrong username or password, not match hash');
            }
            let payload: JWTPayload = {
                id: row[0].id,
                email: email,
                name: row[0].name,
                is_admin: row[0].is_admin,
                is_writer: row[0].is_writer
            }
            let JWT_SECRET = process.env.JWT_SECRET;
            if (!JWT_SECRET) {
                throw new Error('missing JWT_SECRET in env');
            }
            let token = jwtSimple.encode(payload, JWT_SECRET);
            return token;
        } catch (e) {
            throw new Error(e.toString());
        }

    }

    async getUserForGuard(id: number) {
        try {
            const row = await this.knex.select('id', 'email', 'name', 'is_admin', 'is_writer').from('users').where('id', id);
            return row[0];
        } catch (e) {
            throw new Error(e.toString());
        }
    }

    async createTokenWithFacebook(accessToken: string): Promise<string> {

        let res = await fetch(`https://graph.facebook.com/me?access_token=${accessToken}&fields=name,email`);
        let userInfo: ReactFacebookLoginInfo = await res.json();
        console.log('facebook login info:', userInfo);

        let payload : JWTPayload = await this.knex.transaction(async (knex): Promise<JWTPayload> => {
            let user = await knex
                .select('id', 'email', 'name', 'is_admin', 'is_writer')
                .from('users')
                .where({ email: userInfo.email })
                .first()
            if (user) {
                return user
            }
            let email: string | undefined = userInfo.email;
            let name: string | undefined = userInfo.name;

            const randomPassword = crypto.randomBytes(20).toString('hex');
            let [id] = await knex
                .insert({
                    name: userInfo.name,
                    email: userInfo.email,
                    password: await hashPassword(randomPassword),
                    is_admin: false,
                    is_writer: false
                })
                .into('users')
                .returning('id')
            return {
                id: id as number,
                email,
                name, 
            
            }
        })
        let JWT_SECRET = process.env.JWT_SECRET;
        if (!JWT_SECRET) {
            throw new Error('missing JWT_SECRET in env');
        }
        let token = jwtSimple.encode(payload, JWT_SECRET);
        return token;
    }

    async createNewUser(name: string, email: string, password: string) {
        try{
            const row = await this.knex.select('id', 'email').from('users').where('email', email);
            if (row[0]) {
                throw new Error('this user has been registered');
            }
        }catch(e) {
            logger.error(e);
            throw new Error(e.toString());
        }

        const txn = await this.knex.transaction();
        
        try{
            const newUserInfo = await txn.insert({
                name: name,
                email: email,
                password: await hashPassword(password),
                is_admin: false,
                is_writer: false
            }).into('users').returning('email');

            await txn.commit();
            return newUserInfo;
        }catch(e) {
            logger.error(e);
            await txn.rollback();
            return
        }
    }

}


