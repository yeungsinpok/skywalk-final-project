import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {


    async function insertSample<T>(table: string, rows: T[]) {
        let row = await knex.count('* as count').from(table).first()
        if (+row!.count > 0) {
            return
        }
        await knex.insert(rows).into(table)
    }


    await insertSample('collection', [
        { user_id: 10, book_id: 1 },
        { user_id: 10, book_id: 2 },
        { user_id: 10, book_id: 5 },
        { user_id: 10, book_id: 7 },
        { user_id: 10, book_id: 8 },
        { user_id: 10, book_id: 9 },
        { user_id: 10, book_id: 11 },
        { user_id: 10, book_id: 10 },
        { user_id: 10, book_id: 14 },
        { user_id: 10, book_id: 17 },
        { user_id: 10, book_id: 19 },
        { user_id: 10, book_id: 20 },
        { user_id: 11, book_id: 1 },
        { user_id: 11, book_id: 4 },
        { user_id: 11, book_id: 9 },
        { user_id: 11, book_id: 5 },
        { user_id: 11, book_id: 8 },
        { user_id: 11, book_id: 10 },
        { user_id: 11, book_id: 16 },
        { user_id: 11, book_id: 19 },
        { user_id: 11, book_id: 21 },
        { user_id: 12, book_id: 6 },
        { user_id: 12, book_id: 7 },
        { user_id: 12, book_id: 8 },
        { user_id: 12, book_id: 9 },
        { user_id: 12, book_id: 10 },
        { user_id: 12, book_id: 12 },
        { user_id: 12, book_id: 11 },
        { user_id: 12, book_id: 14 },
        { user_id: 12, book_id: 17 },
        { user_id: 12, book_id: 19 },
        { user_id: 12, book_id: 20 }
    ])



    await insertSample('search', [
        { user_id: 10, text: '元素之子' },
        { user_id: 10, text: '末日' },
        { user_id: 10, text: '通天潛行' },
        { user_id: 10, text: '戀愛破壞社' },
        { user_id: 10, text: '玲' },
        { user_id: 10, text: '灰姑娘的情事' },
        { user_id: 10, text: '灰姑娘的情事' },
        { user_id: 11, text: '元素之子' },
        { user_id: 11, text: '末日' },
        { user_id: 11, text: '灰姑娘的情事' },
        { user_id: 12, text: '通天潛行' },
        { user_id: 12, text: '戀愛破壞社' },
        { user_id: 12, text: '元素之子' },
        { user_id: 12, text: '花之死' }
    ])



    await insertSample('book_view', [
        { user_id: 11, book_id: 1 },
        { user_id: 10, book_id: 1 },
        { user_id: 12, book_id: 4 },
        { user_id: 11, book_id: 5 },
        { user_id: 10, book_id: 18 },
        { user_id: 12, book_id: 17 },
        { user_id: 12, book_id: 6 },
        { user_id: 10, book_id: 7 },
        { user_id: 12, book_id: 3 },
        { user_id: 10, book_id: 4 },
        { user_id: 11, book_id: 4 },
        { user_id: 11, book_id: 15 },
        { user_id: 12, book_id: 16 },
        { user_id: 12, book_id: 10 },
        { user_id: 10, book_id: 13 },
        { user_id: 10, book_id: 3 },
        { user_id: 12, book_id: 3 },
        { user_id: 11, book_id: 8 },
        { user_id: 11, book_id: 2 },
        { user_id: 12, book_id: 12 },
        { user_id: 11, book_id: 3 },
        { user_id: 10, book_id: 6 },
        { user_id: 10, book_id: 2 },
        { user_id: 11, book_id: 8 },
        { user_id: 12, book_id: 6 }
    ])


    await insertSample('book_ads', [
        { book_id: 1, image: './uploads/image/adv1.jpg', is_active: true },
        { book_id: 2, image: './uploads/image/adv2.jpg', is_active: true },
        { book_id: 3, image: './uploads/image/adv3.jpg', is_active: true },
        { book_id: 4, image: './uploads/image/adv4.jpg', is_active: true },
        { book_id: 5, image: './uploads/image/adv5.jpg', is_active: true },
        { book_id: 6, image: './uploads/image/adv6.jpg', is_active: true },
        { book_id: 7, image: './uploads/image/adv7.jpg', is_active: true },
        { book_id: 8, image: './uploads/image/adv8.jpg', is_active: true },
        { book_id: 9, image: './uploads/image/adv9.jpg', is_active: true },
        { book_id: 10, image: './uploads/image/adv10.jpg', is_active: true },
        { book_id: 11, image: './uploads/image/adv11.jpg', is_active: true },
        { book_id: 12, image: './uploads/image/adv12.jpg', is_active: false },
        { book_id: 13, image: './uploads/image/adv13.jpg', is_active: true },
        { book_id: 14, image: './uploads/image/adv14.jpg', is_active: true },
        { book_id: 15, image: './uploads/image/adv15.jpg', is_active: false },
        { book_id: 16, image: './uploads/image/adv16.jpg', is_active: true },
        { book_id: 17, image: './uploads/image/adv17.jpg', is_active: true },
        { book_id: 18, image: './uploads/image/adv18.jpg', is_active: true },
        { book_id: 19, image: './uploads/image/adv19.jpg', is_active: true },
        { book_id: 20, image: './uploads/image/adv20.jpg', is_active: false },
        { book_id: 21, image: './uploads/image/adv21.jpg', is_active: true },
        { book_id: 22, image: './uploads/image/adv22.jpg', is_active: true },
        { book_id: 23, image: './uploads/image/adv23.jpg', is_active: true },
        { book_id: 24, image: './uploads/image/adv24.jpg', is_active: false }
    ])

    await insertSample('categories', [
        { category: '異界' },
        { category: '魔法' },
        { category: '校園' },
        { category: '愛情' },
        { category: '懸疑' },
        { category: '科幻' },
        { category: '冒險' },
        { category: '武俠' },
        { category: '搞笑' },
        { category: '玄幻' },
        { category: '散文' },
        { category: '同人' },
        { category: '言情' },
        { category: '女生' },
        { category: '其他' }
    ])

    await insertSample('book_category', [
        { book_id: 1, category_id: 2 },
        { book_id: 1, category_id: 10 },
        { book_id: 2, category_id: 7 },
        { book_id: 3, category_id: 9 },
        { book_id: 4, category_id: 4 },
        { book_id: 5, category_id: 4 },
        { book_id: 6, category_id: 6 },
        { book_id: 6, category_id: 8 },
        { book_id: 7, category_id: 4 },
        { book_id: 8, category_id: 4 },
        { book_id: 9, category_id: 4 },
        { book_id: 10, category_id: 4 },
        { book_id: 11, category_id: 4 },
        { book_id: 12, category_id: 4 },
        { book_id: 13, category_id: 4 },
        { book_id: 14, category_id: 4 },
        { book_id: 15, category_id: 4 },
        { book_id: 16, category_id: 4 },
        { book_id: 17, category_id: 4 },
        { book_id: 18, category_id: 4 },
        { book_id: 19, category_id: 5 },
        { book_id: 20, category_id: 5 },
        { book_id: 21, category_id: 5 },
        { book_id: 22, category_id: 3 },
        { book_id: 23, category_id: 6 },
        { book_id: 24, category_id: 13 }
    ])


    await insertSample('chapter_view', [
        { user_id: 10, chapter_id: 1 },
        { user_id: 10, chapter_id: 2 },
        { user_id: 10, chapter_id: 9 },
        { user_id: 10, chapter_id: 22 },
        { user_id: 10, chapter_id: 67 },
        { user_id: 10, chapter_id: 60 },
        { user_id: 10, chapter_id: 50 },
        { user_id: 11, chapter_id: 62 },
        { user_id: 11, chapter_id: 66 },
        { user_id: 11, chapter_id: 49 },
        { user_id: 11, chapter_id: 35 },
        { user_id: 11, chapter_id: 15 },
        { user_id: 11, chapter_id: 6 },
        { user_id: 11, chapter_id: 1 },
        { user_id: 12, chapter_id: 4 },
        { user_id: 12, chapter_id: 1 },
        { user_id: 12, chapter_id: 39 },
        { user_id: 12, chapter_id: 43 },
        { user_id: 12, chapter_id: 50 },
        { user_id: 12, chapter_id: 52 },
        { user_id: 12, chapter_id: 55 },
        { user_id: 12, chapter_id: 67 }
    ])

    
    await insertSample('comments', [
        { user_id: 10, reply_id: null, content: '三龍立時向準備攻擊的風龍發出最強的魔法攻擊', book_id: 1 },
        { user_id: 10, reply_id: null, content: '封龍城的人民正在舉行一年一度的盛大際典..', book_id: 1 },
        { user_id: 10, reply_id: null, content: '一把令人感到骨寒的聲音???', book_id: 1 },
        { user_id: 10, reply_id: null, content: '在上空出現三條全身黑色，身長完全超過二十五米', book_id: 1 },
        { user_id: 10, reply_id: null, content: '十年前。在十年之前，你跟我說是最後一次的召喚', book_id: 1 },
        { user_id: 11, reply_id: null, content: '男魔法師高舉手中的風神之丈', book_id: 1 },
        { user_id: 11, reply_id: null, content: '黑衣人是誰?', book_id: 1 },
        { user_id: 11, reply_id: null, content: '黑衣人群由於太大風，不能把手中的綠劍放出', book_id: 1 },
        { user_id: 11, reply_id: null, content: '十年前與魔族神風一戰以后?', book_id: 1 },
        { user_id: 11, reply_id: null, content: '黑衣人們，似乎真的害怕起來', book_id: 1 },
        { user_id: 11, reply_id: null, content: '第一次感覺到自己與死亡是那麼接近的', book_id: 1 },
        { user_id: 12, reply_id: null, content: '對不起', book_id: 1 },
        { user_id: 12, reply_id: null, content: '黑衣人各手中的鬥氣出現了一把巨大的深綠色重劍', book_id: 1 },
        { user_id: 12, reply_id: null, content: '一邊用水魔法治療著老公,一邊放出水防禦術', book_id: 1 },
        { user_id: 12, reply_id: null, content: '黑衣人領主的身形一閃,己立即在女魔法師身後不遠了', book_id: 1 },
        { user_id: 12, reply_id: null, content: '也吃了不下五個的水彈', book_id: 1 },
        { user_id: 12, reply_id: null, content: '躲開了龍捲風,男魔法師也知道,龍捲風不可以完全消滅他們', book_id: 1 },
        { user_id: 12, reply_id: null, content: '奉龍神之血為引', book_id: 1 },
        { user_id: 12, reply_id: null, content: '白風的意念是....?', book_id: 1 }
    ])



};
