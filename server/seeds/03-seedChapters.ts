import xlsx from 'xlsx';
import { Knex } from "knex";

interface Chapters {
    content: string,
    bookId: number,
    chapter: number,
    isOffShelf: boolean,
    publishAfter: string
}

export async function seed(knex: Knex): Promise<void> {
    let row = await knex.count('* as count').from('chapters').first()
    if (+row!.count > 0) {
        return
    }

    const workbook = xlsx.readFile('./skywalkDB.xlsx');
    const chapterSheet = workbook.Sheets['chapters'];
    const chapters: Chapters[] = xlsx.utils.sheet_to_json(chapterSheet);

    let id = 0;
    for (let chapter of chapters) {
        id ++;
        await knex.insert({
            id, 
            content: chapter.content, 
            book_id: chapter.bookId,
            chapter: chapter.chapter, 
            is_off_shelf: chapter.isOffShelf, 
            publish_after: chapter.publishAfter

        }).into('chapters');



    //     await knex.raw(`
    //     INSERT INTO chapters (id, content, book_id , chapter, is_off_shelf, publish_after) VALUES (?, ?, ?, ?, ?, ?)
    //     `,
    //         [chapters.indexOf(chapter) + 1, chapter.content, chapter.bookId, chapter.chapter, Boolean(chapter.isOffShelf), chapter.publishAfter]);
    };

    await knex.raw(`
        SELECT setval(pg_get_serial_sequence('chapters', 'id'), coalesce(max(id)+1, 1), false) FROM chapters;
    `)

};
