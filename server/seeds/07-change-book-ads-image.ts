import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
   
    const rows = await knex.select('id', 'image').from('book_ads').where('image', 'like', './uploads/image/%')
    for (let row of rows) {
        await knex('book_ads').update({
            'image': row.image.replace('./uploads/image/', '')
        }).where({ id: row.id })
    }

    // const books_ads_list = [
    //     { id: 1, image: 'image-1000000000001.jpg'},
    //     { id: 2, image: 'image-1000000000002.jpg'},
    //     { id: 3, image: 'image-1000000000003.jpg'},
    //     { id: 4, image: 'image-1000000000004.jpg'},
    //     { id: 5, image: 'image-1000000000005.jpg'},
    //     { id: 6, image: 'image-1000000000006.jpg'},
    //     { id: 7, image: 'image-1000000000007.jpg'},
    //     { id: 8, image: 'image-1000000000008.jpg'},
    //     { id: 9, image: 'image-1000000000009.jpg'},
    //     { id: 10, image: 'image-1000000000010.jpg'},
    //     { id: 11, image: 'image-1000000000011.jpg'},
    //     { id: 12, image: 'image-1000000000012.jpg'},
    //     { id: 13, image: 'image-1000000000013.jpg'},
    //     { id: 14, image: 'image-1000000000014.jpg'},
    //     { id: 15, image: 'image-1000000000015.jpg'},
    //     { id: 16, image: 'image-1000000000016.jpg'},
    //     { id: 17, image: 'image-1000000000017.jpg'},
    //     { id: 18, image: 'image-1000000000018.jpg'},
    //     { id: 19, image: 'image-1000000000019.jpg'},
    //     { id: 20, image: 'image-1000000000020.jpg'},
    //     { id: 21, image: 'image-1000000000021.jpg'},
    //     { id: 22, image: 'image-1000000000022.jpg'},
    //     { id: 23, image: 'image-1000000000023.jpg'},
    //     { id: 24, image: 'image-1000000000024.jpg'}
    // ]

    // for (let book of books_ads_list) {
    //         //await knex.select('id', 'image').from('books').where({ id: books.id })
    //         await knex('book_ads').where('id', book.id).update({'image': book.image})
    //         //await knex.update({image: books.image})
    //         continue
    // }
};

