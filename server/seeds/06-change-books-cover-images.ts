import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {


    const rows = await knex.select('id', 'cover_image').from('books').where('cover_image', 'like', './uploads/image/%')
    for (let row of rows) {
        await knex('books').update({
            'cover_image': row.cover_image.replace('./uploads/image/', '')
        }).where({ id: row.id })
    }

    // const books_list = [
    //     { id: 1, cover_image: 'cover_image-1000000000001.jpg' },
    //     { id: 2, cover_image: 'cover_image-1000000000002.jpg' },
    //     { id: 3, cover_image: 'cover_image-1000000000003.jpg' },
    //     { id: 4, cover_image: 'cover_image-1000000000004.jpg' },
    //     { id: 5, cover_image: 'cover_image-1000000000005.jpg' },
    //     { id: 6, cover_image: 'cover_image-1000000000006.jpg' },
    //     { id: 7, cover_image: 'cover_image-1000000000007.jpg' },
    //     { id: 8, cover_image: 'cover_image-1000000000008.jpg' },
    //     { id: 9, cover_image: 'cover_image-1000000000009.jpg' },
    //     { id: 10, cover_image: 'cover_image-1000000000010.jpg' },
    //     { id: 11, cover_image: 'cover_image-1000000000011.jpg' },
    //     { id: 12, cover_image: 'cover_image-1000000000012.jpg' },
    //     { id: 13, cover_image: 'cover_image-1000000000013.jpg' },
    //     { id: 14, cover_image: 'cover_image-1000000000014.jpg' },
    //     { id: 15, cover_image: 'cover_image-1000000000015.jpg' },
    //     { id: 16, cover_image: 'cover_image-1000000000016.jpg' },
    //     { id: 17, cover_image: 'cover_image-1000000000017.jpg' },
    //     { id: 18, cover_image: 'cover_image-1000000000018.jpg' },
    //     { id: 19, cover_image: 'cover_image-1000000000019.jpg' },
    //     { id: 20, cover_image: 'cover_image-1000000000020.jpg' },
    //     { id: 21, cover_image: 'cover_image-1000000000021.jpg' },
    //     { id: 22, cover_image: 'cover_image-1000000000022.jpg' },
    //     { id: 23, cover_image: 'cover_image-1000000000023.jpg' },
    //     { id: 24, cover_image: 'cover_image-1000000000024.jpg' }
    // ]

    // for (let book of books_list) {
    //     //await knex.select('id', 'cover_image').from('books').where({ id: books.id })
    //     await knex('books').where('id', book.id).update({ 'cover_image': book.cover_image })
    //     //await knex.update({cover_image: books.cover_image})
    //     continue
    // }
};
