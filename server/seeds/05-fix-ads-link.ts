import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {



    const book_ads_list = [
        { book_id: 1, image: './uploads/image/adv1.jpg', is_active: true },
        { book_id: 2, image: './uploads/image/adv2.jpg', is_active: true },
        { book_id: 3, image: './uploads/image/adv3.jpg', is_active: true },
        { book_id: 4, image: './uploads/image/adv4.jpg', is_active: true },
        { book_id: 5, image: './uploads/image/adv5.jpg', is_active: true },
        { book_id: 6, image: './uploads/image/adv6.jpg', is_active: true },
        { book_id: 7, image: './uploads/image/adv7.jpg', is_active: true },
        { book_id: 8, image: './uploads/image/adv8.jpg', is_active: true },
        { book_id: 9, image: './uploads/image/adv9.jpg', is_active: true },
        { book_id: 10, image: './uploads/image/adv10.jpg', is_active: true },
        { book_id: 11, image: './uploads/image/adv11.jpg', is_active: true },
        { book_id: 12, image: './uploads/image/adv12.jpg', is_active: false },
        { book_id: 13, image: './uploads/image/adv13.jpg', is_active: true },
        { book_id: 14, image: './uploads/image/adv14.jpg', is_active: true },
        { book_id: 15, image: './uploads/image/adv15.jpg', is_active: false },
        { book_id: 16, image: './uploads/image/adv16.jpg', is_active: true },
        { book_id: 17, image: './uploads/image/adv17.jpg', is_active: true },
        { book_id: 18, image: './uploads/image/adv18.jpg', is_active: true },
        { book_id: 19, image: './uploads/image/adv19.jpg', is_active: true },
        { book_id: 20, image: './uploads/image/adv20.jpg', is_active: false },
        { book_id: 21, image: './uploads/image/adv21.jpg', is_active: true },
        { book_id: 22, image: './uploads/image/adv22.jpg', is_active: true },
        { book_id: 23, image: './uploads/image/adv23.jpg', is_active: true },
        { book_id: 24, image: './uploads/image/adv24.jpg', is_active: false }
    ]

    for (let book_ads of book_ads_list) {
        let rows = await knex.select('id', 'image', 'is_sample').from('book_ads').where({ book_id: book_ads.book_id })
        if (rows.length === 0) {
            await knex.insert({ ...book_ads, is_sample: true }).into('book_ads')
            continue
        }
        for (let row of rows) {
            if (row.is_sample && row.image !== book_ads.image) {
                await knex('book_ads').update({ image: book_ads.image }).where({ id: row.id })
            }
        }
    }

};
