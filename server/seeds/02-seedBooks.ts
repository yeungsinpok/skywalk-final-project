import xlsx from 'xlsx';
import { Knex } from "knex";

interface Books {
    title: string,
    coverImage: string | null,
    isEnd: boolean,
    author: string,
    abstract: string,
    rank: number,
    hitRate: number,
    isExtra: boolean,
    extraTitle: string | null,
    authorId: number,
    isOffShelf: boolean,
    isApproved: boolean,
    isAdult: boolean,
    isSample: boolean
}


export async function seed(knex: Knex): Promise<void> {
    // let row = await knex.count('* as count').from('books').first()
    // if (+row!.count > 0) {
    //     return
    // }

    const workbook = xlsx.readFile('./skywalkDB.xlsx');
    const usersSheet = workbook.Sheets['books'];
    const books: Books[] = xlsx.utils.sheet_to_json(usersSheet);

    let id = 0
    for (let book of books) {
        id++
        let newRow = {
            title: book.title,
            cover_image: book.coverImage,
            is_end: book.isEnd,
            author: book.author,
            abstract: book.abstract,
            rank: book.rank,
            hit_rate: book.hitRate,
            is_extra: book.isExtra,
            extra_title: book.extraTitle,
            author_id: book.authorId,
            is_off_shelf: book.isOffShelf,
            is_approved: book.isApproved,
            is_adult: book.isAdult,
        }
        let row = await knex.select('id').from('books').where({ id })
        if (row) {
            await knex('books').update(newRow).where({ id })
        } else {
            await knex.insert({
                id,
                ...newRow
            }).into('books')
        }

        //     await knex.raw(`
        //     INSERT INTO books (id, title, cover_image , is_end, author, abstract, rank, hit_rate, is_extra, extra_title, author_id, is_off_shelf, is_approved, is_adult)
        //     VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        //     `,
        //         [books.indexOf(book) + 1, book.title, book.coverImage, Boolean(book.isEnd), book.author, book.abstract, book.rank, book.hitRate, Boolean(book.isExtra), book.extraTitle, book.authorId, Boolean(book.isOffShelf), Boolean(book.isApproved), Boolean(book.isAdult)]);
    };

    await knex.raw(`
        SELECT setval(pg_get_serial_sequence('books', 'id'), coalesce(max(id)+1, 1), false) FROM books;   
    `)
};
