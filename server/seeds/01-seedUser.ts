import { Knex } from "knex";
import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {
    let row = await knex.count('* as count').from('users').first()
    if(+row!.count > 0){
        return
    }

    // Inserts seed entries
   await knex("users").insert([
        { name: "rain", email: "rain@skywalk.com", password: await hashPassword('rain'), is_admin: false, is_writer: true },
        { name: "lily", email: "lily@skywalk.com", password: await hashPassword('lily'), is_admin: false, is_writer: true },
        { name: "sky", email: "sky@skywalk.com", password: await hashPassword('sky'), is_admin: false, is_writer: true },
        { name: "soul", email: "soul@skywalk.com", password: await hashPassword('soul'), is_admin: false, is_writer: true },
        { name: "star", email: "star@skywalk.com", password: await hashPassword('star'), is_admin: false, is_writer: true },
        { name: "fire", email: "fire@skywalk.com", password: await hashPassword('fire'), is_admin: false, is_writer: true },
        { name: "sakura", email: "sakura@skywalk.com", password: await hashPassword('sakura'), is_admin: false, is_writer: true },
        { name: "admin", email: "admin@skywalk.com", password: await hashPassword('admin'), is_admin: true, is_writer: false },
        { name: "writer", email: "writer@skywalk.com", password: await hashPassword('writer'), is_admin: false, is_writer: true },
        { name: "user", email: "user@skywalk.com", password: await hashPassword('user'), is_admin: false, is_writer: false },
        { name: "user2", email: "user1@skywalk.com", password: await hashPassword('user1'), is_admin: false, is_writer: false },
        { name: "user3", email: "user2@skywalk.com", password: await hashPassword('user2'), is_admin: false, is_writer: false },
        { name: "alice", email: "alice@tecky.io", password: await hashPassword('first'), is_admin: false, is_writer: false },
        { name: "bob", email: "bob@tecky.io", password: await hashPassword('second'), is_admin: false, is_writer: false},
        { name: "charlie", email: "charlie@tecky.io", password: await hashPassword('third'), is_admin: false, is_writer: false},
        { name: "bigrain", email: "abc@gmail.com", password: await hashPassword('aaa'), is_admin: false, is_writer: false},
        { name: "lily", email: "cde@gmail.com", password: await hashPassword('bbb'), is_admin: false, is_writer: false},
        { name: "skies", email: "efg@gmail.com", password: await hashPassword('ccc'), is_admin: false, is_writer: false},
        { name: "spirit", email: "hij@gmail.com", password: await hashPassword('ddd'), is_admin: false, is_writer: false},
        { name: "starfeather", email: "klm@gmail.com", password: await hashPassword('eee'), is_admin: false, is_writer: false},
        { name: "wilson", email: "wilson@gmail.com", password: await hashPassword('wilson'), is_admin: false, is_writer: false},
        { name: "tk", email: "tk@gmail.com", password: await hashPassword('tk'), is_admin: false, is_writer: false},
        { name: "enson", email: "enson@gmail.com", password: await hashPassword('enson'), is_admin: false, is_writer: false},
        { name: "beeno", email: "beeno@gmail.com", password: await hashPassword('beeno'), is_admin: false, is_writer: false},
        { name: "reader", email: "reader@gmail.com", password: await hashPassword('reader'), is_admin: false, is_writer: false}
    ])

  

};
