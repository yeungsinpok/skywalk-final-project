import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('book_ads', t => {
        t.boolean('is_sample').notNullable().defaultTo(false)
    })
    await knex('book_ads').update({ is_sample: true })

    await knex.schema.alterTable('books', t => {
        t.boolean('is_sample').notNullable().defaultTo(false)
    })
    await knex('books').update({ is_sample: true })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('books', t => {
        t.dropColumn('is_sample')
    })
    await knex.schema.alterTable('book_ads', t => {
        t.dropColumn('is_sample')
    })
}

