import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    const hasUsersTable = await knex.schema.hasTable('users');
    if (!hasUsersTable) {
        await knex.schema.createTable('users', (table) => {
            table.increments();
            table.text("name").nullable();
            table.text("email").notNullable();
            table.text("password").notNullable();
            table.boolean("is_admin").notNullable();
            table.boolean("is_writer").notNullable();
            table.timestamps(false, true);
        });
    }

    const hasBooksTable = await knex.schema.hasTable('books');
    if (!hasBooksTable) {
        await knex.schema.createTable('books', (table) => {
            table.increments();
            table.text("title").notNullable();
            table.text("cover_image").nullable();
            table.boolean("is_end").notNullable();
            table.text("author").notNullable();
            table.text("abstract").notNullable();
            table.integer("rank").unsigned().notNullable();
            table.integer("hit_rate").notNullable();
            table.boolean("is_extra").notNullable();
            table.string("extra_title").nullable();
            table.integer("author_id").unsigned().notNullable();
            table.foreign("author_id").references("users.id");
            table.boolean("is_off_shelf").notNullable();
            table.boolean("is_approved").notNullable();
            table.boolean("is_adult").notNullable();
            table.timestamps(false, true);
        });
    }

    const hasCollectionTable = await knex.schema.hasTable('collection');
    if (!hasCollectionTable) {
        await knex.schema.createTable('collection', (table) => {
            table.increments();
            table.integer("user_id").unsigned().notNullable();
            table.foreign("user_id").references("users.id");
            table.integer("book_id").unsigned().notNullable();
            table.foreign("book_id").references("books.id");
            table.timestamps(false, true);
        });
    }

    const hasSearchTable = await knex.schema.hasTable('search');
    if (!hasSearchTable) {
        await knex.schema.createTable('search', (table) => {
            table.increments();
            table.integer("user_id").unsigned().notNullable();
            table.foreign("user_id").references("users.id");
            table.text("text").notNullable();
            table.timestamps(false, true);
        });
    }

    const hasBookViewTable = await knex.schema.hasTable('book_view');
    if (!hasBookViewTable) {
        await knex.schema.createTable('book_view', (table) => {
            table.increments();
            table.integer("user_id").unsigned().notNullable();
            table.foreign("user_id").references("users.id");
            table.integer("book_id").unsigned().notNullable();
            table.foreign("book_id").references("books.id");
            table.timestamps(false, true);
        });
    }

    const hasBookAdsTable = await knex.schema.hasTable('book_ads');
    if (!hasBookAdsTable) {
        await knex.schema.createTable('book_ads', (table) => {
            table.increments();
            table.integer("book_id").unsigned().notNullable();
            table.foreign("book_id").references("books.id");
            table.text("image").notNullable();
            table.boolean("is_active").notNullable();
            table.timestamps(false, true);
        });
    }

    const hasChapterTable = await knex.schema.hasTable('chapters');
    if (!hasChapterTable) {
        await knex.schema.createTable('chapters', (table) => {
            table.increments();
            table.text("content").notNullable();
            table.integer('chapter').notNullable();
            table.integer("book_id").notNullable();
            table.foreign("book_id").references("books.id");
            table.boolean("is_off_shelf").notNullable();
            table.timestamp("publish_after").notNullable();
            table.timestamps(false, true);
        });
    }

    const hasCategoriesTable = await knex.schema.hasTable('categories');
    if (!hasCategoriesTable) {
        await knex.schema.createTable('categories', (table) => {
            table.increments();
            table.text("category").notNullable();
            table.timestamps(false, true);
        });
    }

    const hasBookCategoryTable = await knex.schema.hasTable('book_category');
    if (!hasBookCategoryTable) {
        await knex.schema.createTable('book_category', (table) => {
            table.increments();
            table.integer("book_id").unsigned().notNullable();
            table.foreign("book_id").references("books.id");
            table.integer("category_id").unsigned().notNullable();
            table.foreign("category_id").references("categories.id");
            table.timestamps(false, true);
        });
    }

    const hasChapterViewTable = await knex.schema.hasTable('chapter_view');
    if (!hasChapterViewTable) {
        await knex.schema.createTable('chapter_view', (table) => {
            table.increments();
            table.integer("user_id").notNullable();
            table.foreign("user_id").references("users.id");
            table.integer("chapter_id").notNullable();
            table.foreign("chapter_id").references("chapters.id");
            table.timestamps(false, true);
        });
    }

    const hasCommentsTable = await knex.schema.hasTable('comments');
    if (!hasCommentsTable) {
        await knex.schema.createTable('comments', (table) => {
            table.increments();
            table.integer("user_id").unsigned().notNullable();
            table.foreign("user_id").references("users.id");
            table.integer("reply_id").unsigned().nullable();
            table.foreign("reply_id").references("comments.id");
            table.text("content").notNullable();
            table.integer("book_id").unsigned().notNullable();
            table.foreign("book_id").references("books.id");
            table.timestamps(false, true);
        });
    }
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("comments");
    await knex.schema.dropTableIfExists("chapter_view");
    await knex.schema.dropTableIfExists("book_ads");
    await knex.schema.dropTableIfExists("book_view");
    await knex.schema.dropTableIfExists("search");
    await knex.schema.dropTableIfExists("collection");
    await knex.schema.dropTableIfExists("book_category");
    await knex.schema.dropTableIfExists("categories");
    await knex.schema.dropTableIfExists("chapters");
    await knex.schema.dropTableIfExists("books");
    await knex.schema.dropTableIfExists("users");
}