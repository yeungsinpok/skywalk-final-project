#!/usr/bin/bash
set -e
set -o pipefail
set -x

docker stop skywalk-web-server || true
docker rm skywalk-web-server || true
docker run -d -it -p 8080:8080 --name=skywalk-web-server skywalk-web-image
sleep 2
docker ps | grep skywalk-web-server 