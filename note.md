https://myapp.com

https://myapp.com/admin

https://myapp.com/compose

App.env
REACT_APP_API_SERVER=http://localhost:8080 

Server.env
PORT=8080
JWT_SECRET=skywalkNovelRock
NODE_ENV=development



<!-- .envfile -->
POSTGRES_USER=skywalk
POSTGRES_PASSWORD=MidTownLunch
POSTGRES_DB=skywalk
JWT_SECRET=skywalkNovelRock

<!-- TK Dockerfile -->
FROM node:slim
RUN node -v
WORKDIR /usr/src/express-app
COPY . .
RUN npm install
EXPOSE 8080
CMD npm install && \ 
    npx knex migrate:latest &&\
    # Should be done in knex seed file by if not production
    # npx knex seed:run &&\
    # npx ts-node seedBooks.ts &&\
    # npx ts-node seedChapters.ts &&\
    node index.js 

<!-- Update books - cover_image path in Postgres -->
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000001.jpg' WHERE id = 1;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000002.jpg' WHERE id = 2;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000003.jpg' WHERE id = 3;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000004.jpg' WHERE id = 4;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000005.jpg' WHERE id = 5;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000006.jpg' WHERE id = 6;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000007.jpg' WHERE id = 7;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000008.jpg' WHERE id = 8;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000009.jpg' WHERE id = 9;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000010.jpg' WHERE id = 10;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000011.jpg' WHERE id = 11;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000012.jpg' WHERE id = 12;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000013.jpg' WHERE id = 13;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000014.jpg' WHERE id = 14;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000015.jpg' WHERE id = 15;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000016.jpg' WHERE id = 16;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000017.jpg' WHERE id = 17;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000018.jpg' WHERE id = 18;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000019.jpg' WHERE id = 19;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000020.jpg' WHERE id = 20;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000021.jpg' WHERE id = 21;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000022.jpg' WHERE id = 22;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000023.jpg' WHERE id = 23;
UPDATE books SET cover_image = './uploads/image/cover_image-1000000000024.jpg' WHERE id = 24;

<!-- Update book_ads - image path in Postgres -->
UPDATE book_ads SET image = './uploads/image/adv.jpg' WHERE book_id = 1;
UPDATE book_ads SET image = './uploads/image/adv1.jpg' WHERE book_id = 3;
UPDATE book_ads SET image = './uploads/image/adv2.jpg' WHERE book_id = 5;
UPDATE book_ads SET image = './uploads/image/adv3.jpg' WHERE book_id = 8;
UPDATE book_ads SET image = './uploads/image/adv4.jpg' WHERE book_id = 11;
UPDATE book_ads SET image = './uploads/image/adv5.jpg' WHERE book_id = 16;
UPDATE book_ads SET image = './uploads/image/adv6.jpg' WHERE book_id = 2;
UPDATE book_ads SET image = './uploads/image/adv7.jpg' WHERE book_id = 4;
UPDATE book_ads SET image = './uploads/image/adv9.jpg' WHERE book_id = 10;
UPDATE book_ads SET image = './uploads/image/adv10.jpg' WHERE book_id = 18;
UPDATE book_ads SET image = './uploads/image/adv12.jpg' WHERE book_id = 20;
UPDATE book_ads SET image = './uploads/image/adv13.jpg' WHERE book_id = 21;