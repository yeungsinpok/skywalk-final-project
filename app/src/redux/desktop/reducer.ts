import { DesktopState, initialState } from "./state";
import { DesktopSideAction } from "./action";


export const desktopReducer = (
    state: DesktopState = initialState,
    action: DesktopSideAction,
): DesktopState => {
    switch (action.type) {
        case 'goDesktopSide':
            return {
                goDesktop: true,
            }

        case 'quitDesktopSide':
            return {
                goDesktop: false,
            }
        default:
            return state
    }
}
