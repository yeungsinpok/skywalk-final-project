export function goDesktopSide (goDesktop: Boolean) {
    return {
        type: 'goDesktopSide' as const,
        goDesktop
    }
}

export function quitDesktopSide (goDesktop: Boolean) {
    return {
        type: 'quitDesktopSide' as const,
        goDesktop
    }
}


export type DesktopSideAction = 
| ReturnType <typeof goDesktopSide>
| ReturnType <typeof quitDesktopSide>
