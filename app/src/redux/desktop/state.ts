
export type DesktopState = {
    goDesktop: boolean;
}

export const initialState: DesktopState = {
    goDesktop: false,
}
