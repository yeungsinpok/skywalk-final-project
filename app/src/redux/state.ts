
import { AuthState } from './auth/state'
import { RouterState } from 'connected-react-router'
import { BookContentState } from './book-content/state'
import { SearchState } from './search/state'
import { DesktopState} from './desktop/state'
import { ViewState } from './view/state'
import { ThemeState } from './theme/state'
// import { CollectionState } from './collection/state'


export type RootState = {

  auth: AuthState;
  router: RouterState;
  bookContent: BookContentState;
  search: SearchState;
  desktop: DesktopState;
  view: ViewState;
  theme: ThemeState;
  // collection: CollectionState
}