import { RootDispatch } from "../dispatch";
import { loadingLogin, loginFailed, loginSuccess, logout, registerFailed, registerSuccess } from './action';
import jwtDecode from 'jwt-decode';
import { JWTPayload } from "./state";


export function loginWithPasswordThunk(email: string, password: string) {
    return async(dispatch: RootDispatch) => {
        let origin;
        try{
            let { REACT_APP_API_SERVER } = process.env
            if(!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        }catch(error) {
            dispatch(loginFailed((error as Error).toString()));
            return;
        }

        dispatch(loadingLogin())
        let json: any;
        try{
            let res = await fetch(`${origin}/login/password`, {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({email, password})
            })
            json = await res.json();
        }catch(error) {
            console.error('failed to call login with password API:', error);
            dispatch(loginFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('login with password response error:', json.error);
            dispatch(loginFailed(json.error));
            return
        }

        let token: string = json.token; 
        localStorage.setItem('access_token', token);

        try{
            let payload = jwtDecode<JWTPayload>(token);
            console.log("login success with password");
            dispatch(loginSuccess(payload));
            return
        }catch(error) {
            console.error('failed to decode JWT token', error);
            dispatch(loginFailed((error as Error).toString()))
        }
    }
}

export function loginWithFaceBookThunk(accessToken: string) {
    return async (dispatch: RootDispatch)=>{
        let origin
        try{
            let { REACT_APP_API_SERVER } = process.env
            if(!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        }catch(error) {
            dispatch(loginFailed((error as Error).toString()));
            return;
        }

        dispatch(loadingLogin());
        let json: any;
        try{
            console.log('i am in login in with facebook thunk');
            let res = await fetch(`${origin}/login/facebook`, {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({accessToken})
            })
            json = await res.json();
        }catch(error) {
            console.error('failed to call login with facebook API:', error);
            dispatch(loginFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('login with facebook response error:', json.error);
            dispatch(loginFailed(json.error));
            return
        }

        let token: string = json.token;
        console.log("set facebook localStorage token");
        localStorage.setItem('access_token', token);

        try{
            let payload = jwtDecode<JWTPayload>(token);
            console.log("facebook login payload:", payload);
            console.log("login success with facebook");
            dispatch(loginSuccess(payload));
            return
        }catch(error) {
            console.error('failed to decode JWT token', error);
            dispatch(loginFailed((error as Error).toString()))
        }
        
    }
}

export function checkTokenThunk() {
    return (dispatch:RootDispatch) => {
        let token = localStorage.getItem('access_token');
        if (!token){
            console.log('no token found in check token');
            dispatch(logout());
            return;
        }

        try{
            let payload = jwtDecode<JWTPayload>(token);
            console.log("login success in check token");
            dispatch(loginSuccess(payload));
            return
        }catch(error) {
            console.error('failed to decode JWT token', error);
            dispatch(loginFailed((error as Error).toString()))
        }
    }
}

export function registerThunk(name: string, email: string, password: string) {
    return async(dispatch: RootDispatch)=> {
        let origin;
        try{
            let { REACT_APP_API_SERVER } = process.env
            if(!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        }catch(error) {
            dispatch(registerFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try{
            let res = await fetch(`${origin}/register`, {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({name, email, password})
            })
            json = await res.json();
        }catch(error) {
            console.error('failed to call register API:', error);
            dispatch(registerFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('register response error:', json.error);
            dispatch(registerFailed(json.error));
            return
        }

        let token: string = json.token; 
        console.log("set token in localStorage after register");
        localStorage.setItem('access_token', token);

        try{
            let payload = jwtDecode<JWTPayload>(token);
            dispatch(registerSuccess(payload));
            return
        }catch(error) {
            console.error('failed to decode JWT token', error);
            dispatch(registerFailed((error as Error).toString()))
        }
    }
}
