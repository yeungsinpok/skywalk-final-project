import { AuthAction } from './action'
import { initialState, AuthState } from './state'

export const authReducer = (
  state: AuthState = initialState,
  action: AuthAction,
): AuthState => {
  
  switch (action.type) {
    case 'loadingLogin':
      return { loading: true}
    case 'loginFailed':
      return { loading: false, error: action.reason }
    case 'loginSuccess':
      return { loading:false, user: action.payload }
    case 'logout':
      return { loading: false, user: undefined }
    case 'registerSuccess':
      return { loading:false, user: action.payload }
    case 'registerFailed':
        return { loading: false, error: action.reason }  
    default:
      return state
  }
}
