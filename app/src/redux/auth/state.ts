export type AuthState = {
  user?: JWTPayload;
  loading: boolean;
  error?: string;
}


export type JWTPayload = {
  id: number;
  email?: string;
  name?: string;
  is_admin?: boolean;
  is_writer?: boolean
}

export const initialState: AuthState = {
  user: undefined,
  loading: false,
  error: undefined
}

