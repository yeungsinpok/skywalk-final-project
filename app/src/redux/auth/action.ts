import { JWTPayload } from "./state"

export function loadingLogin() {
  return{
    type: 'loadingLogin' as const,
  }
}

export function loginSuccess(payload: JWTPayload) {
  return {
    type: 'loginSuccess' as const,
    payload
  }
}

export function loginFailed(reason: string) {
  return{
    type: 'loginFailed' as const,
    reason
  }
}

export function logout() {
  return {
    type: 'logout' as const,
  }
}

export function registerSuccess(payload: JWTPayload) {
  return {
    type: 'registerSuccess' as const,
    payload
  }
}

export function registerFailed(reason: string) {
  return{
    type: 'registerFailed' as const,
    reason
  }
}



export type AuthAction = 
| ReturnType<typeof loadingLogin> 
| ReturnType<typeof loginSuccess>
| ReturnType<typeof loginFailed>
| ReturnType<typeof logout>
| ReturnType<typeof registerSuccess>
| ReturnType<typeof registerFailed>
