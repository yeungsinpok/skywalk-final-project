
export type SearchState = {
    goSearch: boolean;
    onReminder: boolean;
}

export const initialState: SearchState = {
    goSearch: true,
    onReminder: true
}