export function directToSearch(goSearch: boolean, onReminder: boolean) {
    return {
        type: 'directToSearch' as const,
        goSearch,
        onReminder
    }
}


export function turnOnReminder(onReminder: boolean) {
    return {
        type: 'turnOnReminder' as const,
        onReminder
    }
}



export type SearchAction = 
| ReturnType <typeof directToSearch>
| ReturnType <typeof turnOnReminder>

