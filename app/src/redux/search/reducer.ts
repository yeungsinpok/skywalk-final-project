import { SearchState, initialState } from "./state";
import { SearchAction } from './action';

export const searchReducer = (
    state: SearchState = initialState,
    action: SearchAction,
): SearchState => {
    switch (action.type) {
        case 'directToSearch':
            return {
                goSearch: action.goSearch,
                onReminder: action.onReminder
            }
        case 'turnOnReminder':
            return {
                goSearch: false,
                onReminder: action.onReminder
            }
        default:
            return state
    }
}