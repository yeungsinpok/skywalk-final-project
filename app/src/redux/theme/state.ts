export type ThemeState = {
  sizeName: string;
  fontSize: string;
  // lineHeigh: string;
}

export const themeList = {
  "large": {
    sizeName: 'large',
    fontSize: '1.3em',
  },
  "middle": {
    sizeName: 'middle',
    fontSize: '1em'
  },
  "small": {
    sizeName: 'small',
    fontSize: '0.8em'
  },

  // "small": {
  //   sizeName: 'small',
  //   fontSize: '0.8em'
  // },


}

export type ThemeName = keyof (typeof themeList)

export type ThemeStyleKey  = keyof (typeof themeList[ThemeName])

export const initialState: ThemeState = themeList.middle

