

import { ThemeState, initialState, themeList } from "./state";
import { ChangeFontSizeAction } from "./action";


export const themeReducer = (
  state: ThemeState = initialState,
  action: ChangeFontSizeAction,
): ThemeState => {
  switch (action.type) {
    case 'ChangeFontSize':
      return {
        sizeName: action.theme,
        fontSize: themeList[action.theme].fontSize
      }

    
    default:
      return state
  }
}
