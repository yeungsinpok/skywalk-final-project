

import { ThemeName } from "./state"


export function changeFontSize(theme: ThemeName) {
  return {
    type: 'ChangeFontSize' as const,
    theme
  }
}



export type ChangeFontSizeAction =
  | ReturnType<typeof changeFontSize>