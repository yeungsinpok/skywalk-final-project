import { authReducer } from './auth/reducer'
import { RootState } from './state'
import { RootAction } from './action'
import { combineReducers } from 'redux'

import { connectRouter } from 'connected-react-router'
import history from './history'
import { bookContentReducer } from './book-content/reducer'
import { searchReducer } from './search/reducer'
import { desktopReducer } from './desktop/reducer'
import { viewReducer } from './view/reducer'
import { themeReducer } from './theme/reducer'
// import { collectionReducer } from './collection/reducer'

export const rootReducer: (
  state: RootState | undefined,
  action: RootAction,
) => RootState = combineReducers({

  auth: authReducer,
  bookContent: bookContentReducer,
  search: searchReducer,
  router: connectRouter(history),
  desktop: desktopReducer,
  view: viewReducer, 
  theme: themeReducer, 
  // collection: collectionReducer,
})
