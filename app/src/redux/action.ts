import { AuthAction } from './auth/action'
import { BookContentAction } from './book-content/action'
import { SearchAction } from './search/action';
import { DesktopSideAction } from './desktop/action'
import { ViewAction } from './view/action';
// import { CollectionAction } from './collection/action';



export type RootAction =  AuthAction | BookContentAction | SearchAction | DesktopSideAction | ViewAction;
// | CollectionAction;


