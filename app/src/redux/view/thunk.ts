import { RootDispatch } from "../dispatch";

export function addChapterViewThunk(userId: number, chapterId: number) {
    return async (dispatch: RootDispatch) => {
        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            throw new Error('Server unreachable');
            //dispatch(loginFailed((error as Error).toString()));
            //return;
        }

        //dispatch(loadingLogin())
        let json: any;
        try {
            let res = await fetch(`${origin}/view/add_chapter`, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ userId, chapterId })
            })
            json = await res.json();
        } catch (error) {
            console.error('Failed to add record in chapter_view table:', error);
            //dispatch(loginFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('Add chapter view response error:', json.error);
            //dispatch(loginFailed(json.error));
            return
        }

        else if (json.status == 200) {
            return
        }
    }
}

export function addBookViewThunk(userId: number, bookId: number) {
    return async (dispatch: RootDispatch) => {
        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            throw new Error('Server unreachable');
            //dispatch(loginFailed((error as Error).toString()));
            //return;
        }

        //dispatch(loadingLogin())
        let json: any;
        try {
            let res = await fetch(`${origin}/view/add_book`, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ userId, bookId })
            })
            json = await res.json();
        } catch (error) {
            console.error('Failed to add record in book_view table:', error);
            //dispatch(loginFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('Add book view response error:', json.error);
            //dispatch(loginFailed(json.error));
            return
        }

        else if (json.status == 200) {
            return
        }
    }
}

// export function getChapterContentThunk(userId: number, bookId: number) {
//     return async(dispatch: RootDispatch) => {
//         let origin;
//         try{
//             let { REACT_APP_API_SERVER } = process.env
//             if(!REACT_APP_API_SERVER) {
//                 console.error('missing REACT_APP_API_SERVER in env');
//                 throw new Error('missing REACT_APP_API_SERVER in env');
//             }
//             origin = REACT_APP_API_SERVER;
//         }catch(error) {
//             throw new Error('Server unreachable');
//             //dispatch(loginFailed((error as Error).toString()));
//             //return;
//         }

//         //dispatch(loadingLogin())
//         let json: any;
//         try{
//             let res = await fetch(`${origin}/view/add_book`, {
//                 method: 'POST',
//                 headers: {'Content-Type': 'application/json'},
//                 body: JSON.stringify({userId, bookId})
//             })
//             json = await res.json();
//         }catch(error) {
//             console.error('Failed to add record in book_view table:', error);
//             //dispatch(loginFailed((error as Error).toString()));
//             return
//         }

//         if (json.error) {
//             console.error('Add book view response error:', json.error);
//             //dispatch(loginFailed(json.error));
//             return
//         }

//         else if (json.status(200)) {
//             return
//         }
//     }
// }