export function addChapterView (chapterView: boolean) {
    return {
        type: 'addChapterView' as const,
        chapterView
    }
}

export function addBookView (bookView: boolean) {
    return {
        type: 'addBookView' as const,
        bookView
    }
}

export type ViewAction = 
| ReturnType <typeof addChapterView>
| ReturnType <typeof addBookView>