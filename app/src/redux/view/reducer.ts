import { ViewState, initialState } from "./state";
import { ViewAction } from "./action";


export const viewReducer = (
    state: ViewState = initialState,
    action: ViewAction,
): ViewState => {
    switch (action.type) {
        case 'addChapterView':
            return {           
                chapterView: true
            }

        case 'addBookView':
            return {
                bookView: true,     
            }
        default:
            return state
    }
}
