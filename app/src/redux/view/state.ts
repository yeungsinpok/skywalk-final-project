
export type ViewState = {
    bookView?: boolean;
    chapterView?: boolean;
}

export const initialState: ViewState = {
    bookView: false,
    chapterView: false
}
