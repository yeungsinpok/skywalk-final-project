import { createStore, applyMiddleware, compose, combineReducers} from 'redux'
import { rootReducer } from './reducer'
import {logger} from 'redux-logger'
import history from './history'
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
export type { RootState } from './state'
declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: any
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

// const rootEnhancer =
//   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

const rootEnhancer = composeEnhancers(
  applyMiddleware(thunk),
  applyMiddleware(routerMiddleware(history)),
  applyMiddleware(logger)
)


const store = createStore(rootReducer, rootEnhancer)

export default store


