
export type BookContentState = {
    isReading: boolean;
    bookId?: number;
    chapter?: number;
    chapterId?: number;
    latestUpdateList?: BookSummaryInfo[];
    nextId: number;
    offsetNextId: number;
    rankList?:BookSummaryInfo[];
    totalNumBook: number;
    error?: string;
    chapterContent?: ChapterContentList[]; 
    individualBookInfo?: BookSummaryInfo;
    chapterGroup?: ChapterGroupArray[];
    bookViewRec?: BookSummaryInfo[];
    collectionList?: BookSummaryInfo[];
    writerBookList?: WriterBookInfo[];
    adminGetBookList?: AdminGetBookInfo
    writerChapter?: ChapterContentList;
    checkCollected?: BookSummaryInfo[];
    isCollected?: boolean;
    isChangeToUser?: boolean;
    isChangeToWriter?: boolean;
    isSearching: boolean;
    searchingResult?: searchInfo[];
    totalChapter?: number;
    isEditing?: number; // 0:undefined, 1:upload new chapter, 2:update old chapter, 3:create new book
    adminIsManaging:boolean;
    isBookRec: boolean;
}

export type BookSummaryInfo = {
    book_id: string;
    chapter: number;
    latest_update: string;
    title: string;
    author: string;
    abstract: string;
    is_end: boolean;
    rank: number;
    cover_image: string;
    chapter_id: number;
}

export type ChapterContentList = {
    book_id: number;
    chapter_id: number;
    chapter: number;
    content: string;
}

export type ChapterGroupArray = {
    chapter_id: number;
    chapter: number;
}

export type WriterBookInfo = {
    author_id: number
    book_id: number;
    chapter_id: number;
    chapter: number;
    title: string;
    author: string;
    is_end: boolean;
    cover_image: string;
    category: string[];
}

export type AdminGetBookInfo = {
    author_id: number
    book_id: number;
    chapter_id: number;
    chapter: number;
    title: string;
    author: string;
    is_end: boolean;
    cover_image: string;
    is_off_shelf:boolean;
    category: string[];
}

export type searchInfo ={
    book_id: number;
    category: string;
    category_id: number;
    title: string;
    author: string;
    chapter: number;
    abstract: string;
    is_end: boolean;
    rank: number;
    cover_image: string | null;
    chapter_id: number;
    latest_update: string;
}

export const initialState: BookContentState = {
    isReading: false,
    bookId: undefined,
    chapter: undefined,
    chapterId: undefined,
    latestUpdateList: undefined,
    nextId: 0,
    offsetNextId: 8,
    rankList: undefined,
    totalNumBook: 0,
    error: undefined,
    chapterContent: undefined,
    individualBookInfo: undefined,
    chapterGroup: undefined,
    collectionList: undefined,
    writerBookList: undefined,
    adminGetBookList: undefined,
    writerChapter: undefined,
    isCollected: false,
    isChangeToUser: false,
    isChangeToWriter: false,
    isSearching: false,
    searchingResult: undefined,
    totalChapter: undefined,
    isEditing: undefined,
    adminIsManaging: false,
    isBookRec: false
}
