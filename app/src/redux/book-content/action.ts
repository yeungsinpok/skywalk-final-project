import { BookSummaryInfo, ChapterContentList, ChapterGroupArray, searchInfo, WriterBookInfo } from "./state"

export function readBookContent (isReading: boolean) {
    return {
        type: 'readBookContent' as const,
        isReading
    }
}

export function quitReadBookContent (isReading: boolean) {
    return {
        type: 'quitReadBookContent' as const,
        isReading
    }
}

export function loadingFailed (reason: string) {
    return {
        type: 'loadingFailed' as const,
        reason
    }
}


export function loadLatestUpdateListSuccess (latestUpdateList: BookSummaryInfo[], nextId: number) {
    return {
        type: 'loadLatestUpdateListSuccess' as const,
        latestUpdateList,
        nextId
    }
}


export function loadRankSuccess (rankList: BookSummaryInfo[]) {
    return {
        type: 'loadRankSuccess' as const,
        rankList
    }
}


export function setNoOfBooks (totalNumBook: number) {
    return {
        type: 'setNoOfBooks' as const,
        totalNumBook
    }
}

export function readChapterContent (chapterContent: ChapterContentList[]) {
    return {
        type: 'readChapterContent' as const,
        chapterContent
    }
}

export function readNextChapterContent (chapterContent: ChapterContentList[]) {
    return {
        type: 'readNextChapterContent' as const,
        chapterContent
    }
}

export function readPrevChapterContent (chapterContent: ChapterContentList[]) {
    return {
        type: 'readPrevChapterContent' as const,
        chapterContent
    }
}

export function clearChapterContent () {
    return {
        type: 'clearChapterContent' as const,
    }
}

export function setIndividualBookInfo (individualBookInfo: BookSummaryInfo) {
    return {
        type: 'setIndividualBookInfo' as const,
        individualBookInfo
    }
}

export function loadChapterGroupSuccess (chapterGroup: ChapterGroupArray[]) {
    return {
        type: 'loadChapterGroupSuccess' as const,
        chapterGroup
    }
}

export function loadBookViewRecSuccess (bookViewRec: BookSummaryInfo[]) {
    return {
        type: 'loadBookViewRecSuccess' as const,
        bookViewRec
    }
}

// collection related
export function loadCollectionListSuccess (collectionList: BookSummaryInfo[]) {
    return {
        type: 'loadCollectionListSuccess' as const,
        collectionList
    }
}

export function isCollected ( isCollected: boolean) {
    return {
        type: 'isCollected' as const,
        isCollected
    }
}

export function isChangeToWriter ( isChangeToWriter: boolean) {
    return {
        type: 'isChangeToWriter' as const,
        isChangeToWriter
    }
}

export function leaveIsChangeToWriter ( isChangeToWriter: boolean) {
    return {
        type: 'leaveIsChangeToWriter' as const,
        isChangeToWriter
    }
}


export function isChangeToUser ( isChangeToUser: boolean) {
    return {
        type: 'isChangeToUser' as const,
        isChangeToUser
    }
}

export function leaveIsChangeToUser ( isChangeToUser: boolean) {
    return {
        type: 'leaveIsChangeToUser' as const,
        isChangeToUser
    }
}

export function isSearching ( isSearching: boolean) {
    return {
        type: 'isSearching' as const,
        isSearching
    }
}

export function loadWriterBookListSuccess (writerBookList: WriterBookInfo[]) {
    return {
        type: 'loadWriterBookListSuccess' as const,
        writerBookList
    }
}

export function setChapterId (chapterId: number) {
    return {
        type: 'setChapterId' as const,
        chapterId
    }
}

export function loadWriterChapterSuccess (writerChapter: ChapterContentList) {
    return {
        type: 'loadWriterChapterSuccess' as const,
        writerChapter
    }
}

export function loadSearchResultSuccess (searchingResult: searchInfo[]) {
    return {
        type: 'loadSearchResultSuccess' as const,
        searchingResult
    }
}

export function setTotalChapter (totalChapter: number) {
    return {
        type: 'setTotalChapter' as const,
        totalChapter
    }
}

export function setEdit (isEditing: number) {
    return {
        type: 'setEdit' as const,
        isEditing
    }
}

export function setWriterBookIdChapter(bookId: number, chapter: number){
    return {
        type: 'setWriterBookIdChapter' as const,
        bookId, 
        chapter
    }
}

export function adminIsManaging (adminIsManaging: boolean) {
    return {
        type: 'adminIsManaging'as const,
        adminIsManaging
    }
}

export function adminQuitManaging (adminIsManaging: boolean) {
    return {
        type: 'adminQuitManaging'as const,
        adminIsManaging
    }
}

export function loadBookRec () {
    return {
        type: 'loadBookRec' as const,
    }
}




export type BookContentAction = 
| ReturnType <typeof readBookContent>
| ReturnType <typeof quitReadBookContent>
| ReturnType <typeof loadingFailed>
| ReturnType <typeof loadLatestUpdateListSuccess>
| ReturnType <typeof loadRankSuccess>
| ReturnType <typeof setNoOfBooks>
| ReturnType <typeof readChapterContent>
| ReturnType <typeof readNextChapterContent>
| ReturnType <typeof readPrevChapterContent>
| ReturnType <typeof clearChapterContent>
| ReturnType <typeof setIndividualBookInfo>
| ReturnType <typeof loadChapterGroupSuccess>
| ReturnType <typeof loadBookViewRecSuccess>
| ReturnType <typeof loadCollectionListSuccess>
| ReturnType <typeof loadWriterBookListSuccess>
| ReturnType <typeof isCollected>
| ReturnType <typeof isChangeToWriter>
| ReturnType <typeof leaveIsChangeToWriter>
| ReturnType <typeof isChangeToUser>
| ReturnType <typeof leaveIsChangeToUser>
| ReturnType <typeof isSearching>
| ReturnType <typeof setChapterId>
| ReturnType <typeof loadWriterChapterSuccess>
| ReturnType <typeof loadSearchResultSuccess>
| ReturnType <typeof setTotalChapter>
| ReturnType <typeof setEdit>
| ReturnType <typeof setWriterBookIdChapter>
| ReturnType <typeof adminIsManaging>
| ReturnType <typeof adminQuitManaging>
| ReturnType <typeof loadBookRec>




