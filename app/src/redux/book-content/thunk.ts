import { trendingUp } from "ionicons/icons";
import { StringDecoder } from "string_decoder";
import { RootDispatch } from "../dispatch";
import { loadingFailed, setNoOfBooks, loadRankSuccess, readChapterContent, readNextChapterContent, readPrevChapterContent, loadChapterGroupSuccess, loadBookViewRecSuccess, loadCollectionListSuccess, loadLatestUpdateListSuccess, loadWriterBookListSuccess, loadWriterChapterSuccess, isCollected } from "./action";
import { ChapterContentList, BookSummaryInfo, ChapterGroupArray, WriterBookInfo } from "./state";




export function loadLatestUpdateListThunk(offsetNextId: number, nextId: number) {
    return async (dispatch: RootDispatch) => {
        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/book_content/latest_update_list/offsetNextId/${offsetNextId}/nextId/${nextId}`, {
                method: 'GET'
            })
            json = await res.json();
        } catch (error) {
            console.error('failed to call load latest update list API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('load latest update list response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }

        let latestUpdateList: BookSummaryInfo[] = json.bookSummary;

        try {
            console.log("load latest update list successfully");
            console.log(latestUpdateList);
            if (latestUpdateList.length > 0) {
                dispatch(loadLatestUpdateListSuccess(latestUpdateList, offsetNextId));
            }
            return
        } catch (error) {
            console.error('failed to dispatch load latest update list action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }

    }
}

export function checkNoOfBooksThunk() {
    return async (dispatch: RootDispatch) => {
        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/book_content/number_book`, {
                method: 'GET'
            })
            json = await res.json();
        } catch (error) {
            console.error('failed to call number of book API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('load number of book response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }

        let numberOfBook = json.count;
        console.log('No of book:', numberOfBook, typeof numberOfBook);

        try {
            console.log("load number of book successfully");
            dispatch(setNoOfBooks(parseInt(numberOfBook)));
            return
        } catch (error) {
            console.error('failed to dispatch set number of book action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }
    }
}

export function loadRankThunk() {
    return async (dispatch: RootDispatch) => {
        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/book_content/rank`, {
                method: 'GET'
            })
            json = await res.json();
        } catch (error) {
            console.error('failed to call load book rank API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('load book rank response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }

        let rankList: BookSummaryInfo[] = json.rankList;

        try {
            console.log("load book rank successfully");
            console.log(rankList);
            dispatch(loadRankSuccess(rankList));
                
            return
        } catch (error) {
            console.error('failed to dispatch load rank action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }

    }
}

export function loadChapterThunk(chapterId: number){
	return async (dispatch: RootDispatch) => {
        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/book_content/${chapterId}`, {
                method: 'GET'
            })
            json = await res.json();
            

        } catch (error) {
            console.error('Failed to call chapter content summary API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('load chapter content summary response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }

        let enterChapter: ChapterContentList[] = json.chapterRows;

        try {
            console.log("get chapter content successfully");
            dispatch(readChapterContent(enterChapter));;
            return
        } catch (error) {
            console.error('failed to dispatch chapter group action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }
    }
}

export function loadNextChapterThunk(chapterId: number, bookId: number){
	return async (dispatch: RootDispatch) => {
        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/book_content_next/bookId/${bookId}/chapterId/${chapterId}`, {
                method: 'GET'
            })
            json = await res.json();
            
        } catch (error) {
            console.error('Failed to call chapter content summary API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('load chapter content summary response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }

        let nextChapter: ChapterContentList[] = json.chapterRows;
        
        try {
            console.log("get next chapter successfully");
            dispatch(readNextChapterContent(nextChapter));
                
            return
        } catch (error) {
            console.error('failed to dispatch next chapter action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }
    }
}

export function loadPrevChapterThunk(chapterId: number, bookId: number){
	return async (dispatch: RootDispatch) => {
        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/book_content_prev/bookId/${bookId}/chapterId/${chapterId}`, {
                method: 'GET'
            })
            json = await res.json();          

        } catch (error) {
            console.error('Failed to call chapter content summary API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('load chapter content summary response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }

        let prevChapter: ChapterContentList[] = json.chapterRows;

        try {
            console.log("get previous chapter successfully");
            dispatch(readPrevChapterContent(prevChapter));
                
            return
        } catch (error) {
            console.error('failed to dispatch previous chapter action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }
    }
}

export function loadChapterGroupThunk(bookId: number|string) {
    return async (dispatch:RootDispatch) => {

        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/book_content/chapter_group/${bookId}`, {
                method: 'GET'
            })
            json = await res.json();
        } catch (error) {
            console.error('failed to call get chapter group API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('get chapter group response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }

        let chapterGroup: ChapterGroupArray[] = json.chapterGroup;

        try {
            console.log("get chapter group successfully");
            console.log(chapterGroup);
            dispatch(loadChapterGroupSuccess(chapterGroup));
                
            return
        } catch (error) {
            console.error('failed to dispatch chapter group action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }

    }
}

// TK thunk start -----------------------------------------------------------------------------------------------------


// TK thunk end --------------------------------------------------------------------------------------------------------


// Wilson thunk start -----------------------------------------------------------------------------------------------------
export function getBookViewRecThunk(userId: number) {
    return async (dispatch:RootDispatch) => {

        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/book_content/book_view/${userId}`, {
                method: 'GET'
            })
            json = await res.json();
        } catch (error) {
            console.error('failed to call get book view record API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('get book view record response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }

        let bookViewRec: BookSummaryInfo[] = json.bookViewRec;

        try {
            console.log("get book view record successfully");
            console.log(bookViewRec);
            dispatch(loadBookViewRecSuccess(bookViewRec));
                
            return
        } catch (error) {
            console.error('failed to dispatch book view record action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }
    }

}


export function loadWriterBookThunk(userId: number) {
    return async (dispatch:RootDispatch) => {

        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/book_content/writer_book_record/${userId}`, {
                method: 'GET'
            })
            json = await res.json();
        } catch (error) {
            console.error('failed to call load writer book record API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('load writer book record response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }

        let writerBookList: WriterBookInfo[] = json.writerBookRec;

        try {
            console.log("load writer book record successfully");
            console.log(writerBookList);
            dispatch(loadWriterBookListSuccess(writerBookList));
                
            return
        } catch (error) {
            console.error('failed to dispatch load writer book record action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }
    }

}

export function loadWriterChapterThunk(bookId: number, chapter: number) {
    return async (dispatch:RootDispatch) => {

        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/book_content/bookId/${bookId}/chapter/${chapter}`, {
                method: 'GET'
            })
            json = await res.json();
        } catch (error) {
            console.error('failed to call load writer chapter record API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('load writer chapter record response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }

        let writerChapter: ChapterContentList = json.writerChapterRec;

        try {
            console.log("load writer chapter record successfully");
            console.log(writerChapter);
            dispatch(loadWriterChapterSuccess(writerChapter));
                
            return
        } catch (error) {
            console.error('failed to dispatch load writer chapter record action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }
    }
    
}



// Wilson thunk end --------------------------------------------------------------------------------------------------------


// Enson thunk start -----------------------------------------------------------------------------------------------------
export function getCollectionListThunk(userId: number) {
    return async (dispatch:RootDispatch) => {

        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/collection/list/${userId}`, {
                method: 'GET'
            })
            json = await res.json();
        } catch (error) {
            console.error('failed to call get collection API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('get collection response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }

        let collectionListRec: BookSummaryInfo[] = json.collectedBookList;

        try {
            console.log("get collection successfully");
            console.log(collectionListRec);
            dispatch(loadCollectionListSuccess(collectionListRec));
                
            return
        } catch (error) {
            console.error('failed to dispatch get collection action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }
    }

}




export function checkCollectionThunk(userId: number, bookId: number) {
    return async (dispatch:RootDispatch) => {

        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/collection/check/${userId}/${bookId}`, {
                method: 'GET'
            })
            json = await res.json();
        } catch (error) {
            console.error('failed to call check collection API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('check collection response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }

        let checkCollectedRecord: boolean = json.checkCollectedRecord;

        try {
            console.log("check collection successfully");
            console.log(checkCollectedRecord);
            dispatch(isCollected(checkCollectedRecord));
                
            return
        } catch (error) {
            console.error('failed to dispatch check collection action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }
    }
}


export function addCollectionThunk(userId: number, bookId: string) {
    return async (dispatch:RootDispatch) => {

        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/collection/add/${userId}/${bookId}`, {
                method: 'POST'
            })
            json = await res.json();
        } catch (error) {
            console.error('failed to call add collection API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('add collection response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }

        let checkCollectedRecord: boolean = json.checkCollectedRecord;

        try {
            if (json.status === 200){       
                console.log("add collection successfully");
                dispatch(isCollected(checkCollectedRecord));
                return
            }
        } catch (error) {
            console.error('failed to dispatch add collection action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }
    }
}



export function removeCollectionThunk(userId: number, bookId: number) {
    return async (dispatch:RootDispatch) => {

        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
            dispatch(loadingFailed((error as Error).toString()));
            return;
        }

        let json: any;
        try {
            let res = await fetch(`${origin}/collection/remove/${userId}/${bookId}`, {
                method: 'DELETE'
            })
            json = await res.json();
        } catch (error) {
            console.error('failed to call remove collection API:', error);
            dispatch(loadingFailed((error as Error).toString()));
            return
        }

        if (json.error) {
            console.error('remove collection response error:', json.error);
            dispatch(loadingFailed(json.error));
            return
        }
        
        try {
            if (json.status === 200){       
                console.log("remove collection successfully");
                return
            }
        } catch (error) {
            console.error('failed to dispatch remove collection action', error);
            dispatch(loadingFailed((error as Error).toString()))
        }
    }
}


// Enson thunk end --------------------------------------------------------------------------------------------------------
