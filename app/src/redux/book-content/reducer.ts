import { BookContentState, initialState } from "./state";
import { BookContentAction } from "./action";


export const bookContentReducer = (
    state: BookContentState = initialState,
    action: BookContentAction,
): BookContentState => {
    switch (action.type) {
        case 'readBookContent':
            return {
                ...state,
                isReading: true,
                chapterId: state.chapterId,
                nextId: 0,
                offsetNextId: state.offsetNextId
            }

        case 'quitReadBookContent':
            return {
                ...state,
                isReading: action.isReading,
                chapterId: state.chapterId,
                nextId: state.nextId,
                offsetNextId: state.offsetNextId
            }
            

        case 'adminIsManaging':
            return {
                ...state,
                adminIsManaging: true
            }

        case 'adminQuitManaging':
            return {
                ...state,
                adminIsManaging: false
            }
            
        case 'loadingFailed':
            return {
                ...state,
                isReading: false,
                chapterId: undefined,
                error: action.reason,
                nextId: 0,
                offsetNextId: state.offsetNextId
            }

        case 'loadLatestUpdateListSuccess':
            return {
                ...state,
                isReading: false,
                chapterId: undefined,
                latestUpdateList: state.latestUpdateList ? state.latestUpdateList.concat(action.latestUpdateList) : [...action.latestUpdateList],
                nextId: state.nextId + action.nextId,
                offsetNextId: state.offsetNextId
            }

        case 'loadRankSuccess':
            return {
                ...state,
                isReading: false,
                chapterId: state.chapterId,
                rankList: action.rankList
            }

        case 'setNoOfBooks':
            return {
                ...state,
                isReading: false,
                chapterId: undefined,
                nextId: state.nextId,
                offsetNextId: state.offsetNextId,
                totalNumBook: action.totalNumBook
            }

        case 'readChapterContent':
            return {
                ...state,
                isReading: true,
                chapterContent: action.chapterContent
            }

        case 'readNextChapterContent':
            return {
                ...state,
                isReading: true,
                chapterContent: action.chapterContent
            }

        case 'readPrevChapterContent':
            return {
                ...state,
                isReading: true,
                chapterContent: action.chapterContent
            }
        case 'clearChapterContent':
            return {
                ...state,
                chapterContent: undefined
            }
        case 'setIndividualBookInfo':
            return {
                ...state,
                individualBookInfo: action.individualBookInfo
            }

        case 'loadChapterGroupSuccess':
            return {
                ...state,
                chapterGroup: action.chapterGroup
            }

        case 'loadBookViewRecSuccess':
            return {
                ...state,
                bookViewRec: action.bookViewRec
            }

        // collection related
        case 'loadCollectionListSuccess':
            return {
                ...state,
                collectionList: action.collectionList
            }

        case 'isCollected':
            return {
                ...state,
                isCollected: action.isCollected
            }

        case 'isChangeToWriter':
            return {
                ...state,
                isChangeToWriter: true
            }

        case 'leaveIsChangeToWriter':
            return {
                ...state,
                isChangeToWriter: false
            }

        case 'isChangeToUser':
            return {
                ...state,
                isChangeToUser: true
            }

        case 'leaveIsChangeToUser':
            return {
                ...state,
                isChangeToUser: false
            }

        case 'isSearching':
            return {
                ...state,
                isSearching: action.isSearching
            }

        case 'loadWriterBookListSuccess':
            return {
                ...state,
                writerBookList: action.writerBookList
            }

        case 'setChapterId':
            return {
                ...state,
                chapterId: action.chapterId
            }

        case 'loadWriterChapterSuccess':
            return {
                ...state,
                writerChapter: action.writerChapter
            }


        case 'loadSearchResultSuccess':
            return {
                ...state,
                searchingResult: action.searchingResult
            }

        case 'setTotalChapter':
            return {
                ...state,
                totalChapter: action.totalChapter
            }

        case 'setEdit':
            return {
                ...state,
                isEditing: action.isEditing,
            }
        case 'setWriterBookIdChapter':
            return {
                ...state,
                bookId: action.bookId,
                chapter: action.chapter
            }
        case 'loadBookRec':
            return {
                ...state,
                isBookRec: !state.isBookRec
            }

        default:
            return state
    }
}
