export function getAPIOrigin() {
    if (window.location.origin.startsWith('http://localhost')) {
        return 'http://localhost:8080'
    }
    return window.location.origin
}

export function get(url: string) {
    return fetch(getAPIOrigin() + url, {
        headers: {
            "Authorization": `Bearer ${localStorage.getItem('access_token')}`,
        }
    })
        .then(res => res.json())
        .catch(error => ({ error: error.toString() }))
}

export function post(url: string, body = {}) {
    return fetch(getAPIOrigin() + url, {
        method: 'POST',
        headers: {
            "Authorization": `Bearer ${localStorage.getItem('access_token')}`,
            "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
    })
        .then(res => res.json())
        .catch(error => ({ error: error.toString() }))
}



export function toImageUrl(url: string) {
    if (url.startsWith('./')) {
        return url
    }
    let server = getAPIOrigin()
    // let server = 'https://skywalknovel.com'
    // url = 'adv13.jpg'
    return `${server}/uploads/image/${url}`
}