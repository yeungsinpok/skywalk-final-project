import commentDataStyles from "../components/scss/BookCommentData.module.scss";
import { format } from "date-fns";
import { CommentList, UserList } from "../model/book-content";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { IonButton } from "@ionic/react";
import { adminIsManaging, isCollected } from "../redux/book-content/action";

type Props = {
    commentList: CommentList;
}



function BookComment(props: Props) {
  const dispatch = useDispatch();
  const is_admin = useSelector(
    (state: RootState) => state.auth.user?.is_admin
  );

  async function removeComment(commentId: number) {
    let origin;

    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      const commentObj = {
        commentId: commentId,
      };
      let res = await fetch(`${origin}/comment/remove`, {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(commentObj),
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call remove collection API:", error);
      return;
    }
    if (json.error) {
      console.error("remove collection response error:", json.error);
      return;
    }
    console.log("status:", json.status);
    try {
      if (json.status === 200) {
        console.log("remove collection successfully");
        return;
      }
    } catch (error) {
      console.error("failed to dispatch remove collection action", error);
      return;
    }
    dispatch(adminIsManaging(true))
  }
      


  
    return (
        <>
        {is_admin ? 
         <div>
         <div className={commentDataStyles.commentBox}>
             <div className={commentDataStyles.commentUser}>{props.commentList.name} <span  className={commentDataStyles.commentDate}>
                 {format(new Date(props.commentList.created_at), "yyyy-MM-dd")} <span className={commentDataStyles.time}>{format(new Date(props.commentList.created_at), "p")}</span> 
             </span></div>
            
             <div className={commentDataStyles.commentDetail}>{props.commentList.content}</div>
             <IonButton color="danger" size="small" onClick={() => removeComment(props.commentList.comment_id)}>刪除評論</IonButton>
          
         </div>

     </div>:  <div>
            <div className={commentDataStyles.commentBox}>
                <div className={commentDataStyles.commentUser}>{props.commentList.name} <span  className={commentDataStyles.commentDate}>
                    {format(new Date(props.commentList.created_at), "yyyy-MM-dd")} <span className={commentDataStyles.time}>{format(new Date(props.commentList.created_at), "p")}</span> 
                </span></div>
               
                <div className={commentDataStyles.commentDetail}>{props.commentList.content}</div>
            </div>

        </div>
    
    
    
    }
        </>
    )

}

export default BookComment;