import styles from "./scss/AdminAuthorManage.module.scss";
import AdminEditBar from "../components/AdminEditBar";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import barStyles from "../components/scss/DesktopBar.module.scss";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { goDesktopSide, quitDesktopSide } from "../redux/desktop/action";
import { useEffect, useState } from "react";
import tableStyles from "../components/scss/AdminOffShelfBookListPageTable.module.scss";
import { OffShelfBookList } from "../model/book-content";
import { logout } from "../redux/auth/action";
import AdminAuthorManageElement from "../components/AdminAuthorManageElement";
import { isChangeToUser, isChangeToWriter } from "../redux/book-content/action";
import AdminOffShelfBookListPageElement from "../components/AdminOffShelfBookListPageElement";
import Space from "../components/Space";

const AdminOffShelfBookList = () => {
   // work later
  const changeToUser = useSelector((state: RootState) => state.bookContent.isChangeToUser);

  const name = useSelector((state: RootState) => state.auth.user?.name);
  const history = useHistory();
  const dispatch = useDispatch();
  const [offShelfBookLists, setOffShelfBookList] = useState<OffShelfBookList[] | undefined>(undefined);
  const adminManaging = useSelector((state:RootState)=>state.bookContent.adminIsManaging);
  const isEditing = useSelector((state:RootState)=>state.bookContent.isEditing);

  function logoutNow() {
    dispatch(quitDesktopSide(false));
    dispatch(logout());
    localStorage.removeItem('access_token');
    history.push("/");
  }

  // GetList// GetList// GetList// GetList// GetList// GetList// GetList// GetList// GetList// GetList

  async function getOffShelfBookList() {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      return;
    }
    let json: any;
    try {
      let res = await fetch(`${origin}/admin/off_shelf_book_list`, {
        method: "GET",
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call off shelf bookList API:", error);

      return;
    }

    if (json.error) {
      console.error("load off shelf bookList response error:", json.error);

      return;
    }

    let offShelfBookList: OffShelfBookList[] = json.offShelfBookList;
    setOffShelfBookList(offShelfBookList);
    try {
      console.log("load off shelf bookList successfully");
      console.log(offShelfBookList);
      return;
    } catch (error) {
      console.error("failed to dispatch load off shelf bookList action", error);
    }
  }

  useEffect(() => {
    getOffShelfBookList();
    dispatch(goDesktopSide(true));
  }, []);

  useEffect(() => {
    getOffShelfBookList();
  }, [changeToUser]);

  useEffect(() => {
    getOffShelfBookList();
  }, [adminManaging]);

  useEffect(() => {
    getOffShelfBookList();
  }, [isEditing]);


  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>
            <div className={barStyles.accountBox}>
              <div className={barStyles.accountName}>
                您好, <span>{name}</span>
              </div>
              <div className={barStyles.block}> | </div>
              <div
                className={barStyles.logoutButton}
                onClick={() => logoutNow()}
              >
                登出
              </div>
            </div>
          </IonTitle>
        </IonToolbar>
        <AdminEditBar />
      </IonHeader>
      <IonContent>
        {/* <AdminAuthorManageTable /> */}
        <main>
          <div className={tableStyles.accountManageTable}>
          <div className={tableStyles.accountManageBtn}>作者 ID</div>
            <div className={tableStyles.accountManageBtn}>作者 </div>
            <div className={tableStyles.accountManageBtn}>小說 ID</div>
            <div className={tableStyles.accountManageBtn}>小說名稱</div>
            <div className={tableStyles.accountManageBtn}>上載時間</div>
          </div>
           
          {offShelfBookLists ? (
            offShelfBookLists.map((result) => (
              <AdminOffShelfBookListPageElement key={result.id} resultInfo={result} />
            ))
          ) : (
            <div className={barStyles.updatedBox} color="secondary">
              沒有找到搜尋結果....
            </div>
          )}
             <Space />
        </main>
      </IonContent>
    </IonPage>
  );
};

export default AdminOffShelfBookList;
