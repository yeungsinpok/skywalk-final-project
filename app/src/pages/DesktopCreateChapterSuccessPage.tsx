import DesktopEditBar from "../components/DesktopEditBar";
import styles from "./scss/DesktopCreateChapterSuccess.module.scss";
import {
  IonButton,
  IonCheckbox,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonPage,
  IonRadio,
  IonRadioGroup,
  IonTitle,
  IonToolbar,
  useIonRouter,
} from "@ionic/react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import barStyles from "../components/scss/DesktopBar.module.scss";
import { goDesktopSide, quitDesktopSide } from "../redux/desktop/action";
import { RootState } from "../redux/state";
import { SetStateAction, useEffect, useState } from "react";
import { logout } from "../redux/auth/action";
import { isSearching, setEdit } from "../redux/book-content/action";

const DesktopCreateNewSuccess = () => {
  const name = useSelector((state: RootState) => state.auth.user?.name);
  const history = useHistory();
  const dispatch = useDispatch();

  function logoutNow() {
    dispatch(quitDesktopSide(false));
    dispatch(logout());
    localStorage.removeItem("access_token");
    history.push("/");
  }
  useEffect(() => {
    dispatch(goDesktopSide(true));
  }, []);



const router = useIonRouter();

function backBtn(){
  // dispatch(isSearching(false));
  dispatch(setEdit(0));
  router.push('/desktop_view_book','none')
}


  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>
            <div className={barStyles.accountBox}>
              <div className={barStyles.accountName}>
                您好, <span>{name}</span>
              </div>
              <div className={barStyles.block}> | </div>
              <div
                className={barStyles.logoutButton}
                onClick={() => logoutNow()}
              >
                登出
              </div>
            </div>
          </IonTitle>
        </IonToolbar>
        <DesktopEditBar />
      </IonHeader>
      <IonContent>
        <main>
          <div className={styles.messageArea}>
            <div className={styles.messageBox}>
            <div className={styles.messageWordBox}>
              <p>文章成功上傳</p>
              <p>請等待管理員批核並發佈。</p>
              <div className={styles.buttonBar}>
              <IonButton color="primary" size="large" onClick={backBtn}> 確定</IonButton>
              </div>
              </div>
            </div>
          </div>
        </main>
      </IonContent>
    </IonPage>
  );
};
export default DesktopCreateNewSuccess;
