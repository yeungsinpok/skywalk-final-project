import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from "@ionic/react"
import styles from "./scss/AboutMe.module.scss";
import { useSelector } from "react-redux";
import { RootState } from "../redux/state";
import Login from "../components/Login";
import Setting from "../components/Setting";
import { Redirect } from 'react-router-dom';

const AboutMe = () => {
  const user = useSelector((state:RootState)=>state.auth.user);
  console.log('User in about me: ', user);
  return (
    <IonPage>
    <IonHeader>
      <IonToolbar color="primary"  >
        <IonTitle className={styles.appTitle}>我的配置</IonTitle>
      </IonToolbar>
    </IonHeader>
   
      
      <IonContent color="secondary">
        {
          user ?
            <Setting />
            :
            <Redirect to="/login" />
        }
      </IonContent>
      </IonPage>
  )
}

export default AboutMe
