import { IonContent} from "@ionic/react"
import styles from "./scss/FirstPage.module.scss";
import firstPageLogo from "../components/img/book.jpg";


const FirstPage = () => {
    return (
      <IonContent>
      <main>
          <div className={styles.firstPageBackground}>
          <img src={firstPageLogo} className={styles.firstPageIcon}></img>
          </div>
      </main>
      </IonContent>
    )
  }
  
  export default FirstPage
  