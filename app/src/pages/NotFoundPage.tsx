import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react'
import { useLocation } from 'react-router'
import styles from "./scss/NotFoundPage.module.scss";
import NotFound from '../components/NotFound';

const NotFoundPage = () => {
  const location = useLocation()
  const pathname = location.pathname
  return (
    <IonPage>
    <IonHeader>
      <IonToolbar color="primary"  >
     
      </IonToolbar>
    </IonHeader>
      <IonContent color="secondary">
        <NotFound />
      </IonContent>
      </IonPage>
  )
}

export default NotFoundPage
