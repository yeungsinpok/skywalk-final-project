import UpdatedBook from "../components/UpdatedBook";
import Space from "../components/Space";
import styles from "./scss/Update.module.scss";
import React, { useEffect, useState } from 'react';
import { checkNoOfBooksThunk, loadLatestUpdateListThunk } from "../redux/book-content/thunk";
import { RootState } from "../redux/state";
import InfiniteScroll from "react-infinite-scroll-component";
import { useDispatch, useSelector } from 'react-redux';
import { IonButton, IonButtons, IonContent, IonHeader, IonIcon, IonPage, IonTitle, IonToolbar } from "@ionic/react";
import logo from "../components/img/SkywalkSmall.jpg";
import NavBarStyles from "../components/scss/NavBar.module.scss";
import { searchOutline } from "ionicons/icons";
import { BookSummaryInfo } from "../model/book-content";
import { getAPIOrigin } from "../helpers/api";

function Update() {

  // const latestUpdateList = useSelector((state: RootState) => state.bookContent.latestUpdateList);
  // const nextId = useSelector((state: RootState) => state.bookContent.nextId);
  // const offsetNextId = useSelector((state: RootState) => state.bookContent.offsetNextId);
  // const totalNumBook = useSelector((state: RootState) => state.bookContent.totalNumBook);

  // const dispatch = useDispatch();
  // useEffect(() => {
  //   if (latestUpdateList === undefined) {
  //     dispatch(checkNoOfBooksThunk());
  //     dispatch(loadLatestUpdateListThunk(offsetNextId, nextId));
  //   }
  // }, []);

  const [latestUpdateList, setLatestUpdateList] = useState<BookSummaryInfo[] | undefined>(undefined);
  const [nextId, setNextId] = useState<number>(0);
  const offsetNextId = 8;
  const [totalNumBook, setTotalNumBook] = useState<number>(0);

  async function checkNoOfBooks() {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/book_content/number_book`, {
        method: 'GET'
      })
      json = await res.json();
    } catch (error) {
      console.error('failed to call number of book API:', error);
      return
    }

    if (json.error) {
      console.error('load number of book response error:', json.error);
      return
    }

    let numberOfBook = json.count;
    console.log('No of book:', numberOfBook, typeof numberOfBook);
    console.log("load number of book successfully");
    setTotalNumBook(numberOfBook);
    return

  }

  async function loadLatestUpdateList(offsetNextId: number, nextId: number) {
    // let origin = getAPIOrigin()
// beeno

    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/book_content/latest_update_list/offsetNextId/${offsetNextId}/nextId/${nextId}`, {
        method: 'GET'
      })
      json = await res.json();
    } catch (error) {
      console.error('failed to call load latest update list API:', error);
      return
    }

    if (json.error) {
      console.error('load latest update list response error:', json.error);
      return
    }

    let updateList: BookSummaryInfo[] = json.bookSummary;


    console.log("load latest update list successfully");
    if (updateList.length > 0) {
      setLatestUpdateList(latestUpdateList ? latestUpdateList.concat(updateList) : [...updateList]);
      setNextId(nextId + offsetNextId);
    }
    return

  }

  useEffect(() => {
    console.log("In latest update useEffect");
    checkNoOfBooks();
    loadLatestUpdateList(offsetNextId, nextId);
  }, []);


  function fetchMoreBooks() {
    console.log('book summary info list in fetch more books:', latestUpdateList)
    console.log("i am in fetch more books function");
    loadLatestUpdateList(offsetNextId, nextId);
  };


  const adminManaging = useSelector((state:RootState)=>state.bookContent.adminIsManaging);
  const desktop = useSelector((state:RootState)=>state.desktop.goDesktop);

  useEffect(() => {
    console.log("In latest update useEffect");
    checkNoOfBooks();
    loadLatestUpdateList(offsetNextId, nextId);
  }, [adminManaging]);

  useEffect(() => {
    console.log("In latest update useEffect");
    checkNoOfBooks();
    loadLatestUpdateList(offsetNextId, nextId);
  }, [desktop]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary"  >
          <IonTitle className={styles.appTitle}>最近更新</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
      <div color="secondary" className={styles.area}>
        <InfiniteScroll
          dataLength={latestUpdateList?.length as number || offsetNextId}
          next={fetchMoreBooks}
          hasMore={(latestUpdateList?.length || offsetNextId) < totalNumBook}
          loader={<h4>Loading...</h4>}
          height={'91vh'}
        >
          {
            latestUpdateList ?
              latestUpdateList.map(latestUpdate => (
                <UpdatedBook
                  key={latestUpdate.book_id}
                  latestUpdateContent={latestUpdate} />
              ))
              :
              <><div>empty list</div></>
          }
          <Space />
        </InfiniteScroll>
      </div >
      </IonContent>
    </IonPage>
  );
}


export default Update;
