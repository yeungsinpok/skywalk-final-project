import {
  IonAvatar,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonPage,
  IonSearchbar,
  IonThumbnail,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import styles from "./scss/Ranking.module.scss";
import RankingBook from "../components/RankingBook";
import OtherRankingList from "../components/OtherRankingList";
import Space from "../components/Space";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { useEffect, useState } from "react";
import { loadRankThunk } from "../redux/book-content/thunk";
import AppHeader from "../components/AppHeader";
import {
  bookmarkSharp,
  bookmarksOutline,
  bookmarksSharp,
  searchOutline,
} from "ionicons/icons";
import logo from "../components/img/SkywalkSmall.jpg";
import NavBarStyles from "../components/scss/NavBar.module.scss";
// import { BookSummaryInfo } from "../redux/book-content/state";
import { BookSummaryInfo } from "../model/book-content";

const RankingPage = () => {
  // const rankList = useSelector(
  //   (state: RootState) => state.bookContent.rankList
  // );

  // const dispatch = useDispatch();

  // useEffect(() => {
  //   if (rankList === undefined) {
  //     dispatch(loadRankThunk());
  //   }
  // }, []);

  const [rankList, setRankList] = useState<BookSummaryInfo[] | undefined>(undefined);

  async function loadRank() {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/book_content/rank`, {
        method: 'GET'
      })
      json = await res.json();
    } catch (error) {
      console.error('failed to call load book rank API:', error);
      return
    }

    if (json.error) {
      console.error('load book rank response error:', json.error);
      return
    }

    let rankList: BookSummaryInfo[] = json.rankList;

    try {
      console.log("load book rank successfully");
      setRankList(rankList);
      return

    } catch (error) {
      console.error('failed to dispatch load rank action', error);
    }
  }

  useEffect(() => {
    console.log("In rank page useEffect");
    loadRank()
  }, []);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary"  >
          <IonTitle className={styles.appTitle}>人氣排行榜</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent color="secondary">

        <div className={styles.rankingListBox}>
          {rankList ? (
            rankList.map((rank, index) =>
              index < 3 ? (
                <RankingBook key={rank.book_id} rankListInfo={rank} rank={index} />
              ) : (
                <OtherRankingList key={rank.book_id} rankListInfo={rank} rank={index} />
              )
            )
          ) : (
            <>
              <div>empty list</div>
            </>
          )}

          <Space />
        </div>
      </IonContent>
    </IonPage>
  );
};

export default RankingPage;
