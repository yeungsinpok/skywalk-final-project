import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonPage,
  IonTitle,
  IonToolbar,
  useIonRouter,
} from "@ionic/react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import styles from "../components/scss/Search.module.scss";
import {
  isSearching,
  loadSearchResultSuccess,
} from "../redux/book-content/action";
import { SearchByCatList } from "../model/book-content";
import { useEffect, useState } from "react";
import {
  book,
  searchCircle,
  searchCircleOutline,
  searchOutline,
} from "ionicons/icons";
import { Link } from "react-router-dom";

const SearchPage = () => {
  const router = useIonRouter();
  const dispatch = useDispatch();
  const onReminder = useSelector((state: RootState) => state.search.onReminder);
  const userId = useSelector((state:RootState)=>state.auth.user?.id);
  const [word, setWord] = useState("");

  function search(id: number) {
    console.log("i am in search page");
    router.push(`/searching/by_category_id/${id}`, "none");
    dispatch(isSearching(true));
  }

  async function searchKeyWord(word: string, userId?: number) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }
    let json: any;
    try {
      const keyWord = {
        word: word,
        userId
      };
      console.log("word:", word);
      let res = await fetch(`${origin}/search/by_key_word`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(keyWord),
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call search key word rank API:", error);
      return;
    }
    if (json.error) {
      console.error("search key word response error:", json.error);
      return;
    }
    let searchKeyWordResultList: SearchByCatList[] = json.searchByKeyWordResult;
    try {
      console.log("search key word successfully");
      console.log(searchKeyWordResultList);

      dispatch(loadSearchResultSuccess(searchKeyWordResultList));
      dispatch(isSearching(true));

      if (searchKeyWordResultList.length > 0) {
        return;
      }
      return;
    } catch (error) {
      console.error("failed to dispatch search key word action", error);
    }
  }


  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonButton onClick={() => {
                searchKeyWord(word, userId);
                setWord('');
                {
                  router.push("/searching/by_keyword", "none");
                }
              }}>
            <IonIcon
              icon={searchCircle}
              className={styles.iconSize}
              
              color="secondary"
              size="large"
            ></IonIcon>
            </IonButton>
          </IonButtons>
          <IonButtons >
            <input
              className={styles.searchBar}
              type="text"
              value={word}
              onChange={(e) => setWord(e.target.value!)}
            ></input>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <div>
          {onReminder ? (
            <>
              <div className={styles.searchHints}>請嘗試搜尋書名，作者</div>
              <div className={styles.typeName}>按分類搜尋</div>
              <div className={styles.typeBox}>
                <div className={styles.type} onClick={() => search(1)}>
                  異界
                </div>
                <div className={styles.type} onClick={() => search(2)}>
                  魔法
                </div>
                <div className={styles.type} onClick={() => search(3)}>
                  校園
                </div>
                <div className={styles.type} onClick={() => search(4)}>
                  愛情
                </div>
                <div className={styles.type} onClick={() => search(5)}>
                  懸疑
                </div>
                <div className={styles.type} onClick={() => search(6)}>
                  科幻
                </div>
                <div className={styles.type} onClick={() => search(7)}>
                  冒險
                </div>
                <div className={styles.type} onClick={() => search(8)}>
                  武俠
                </div>
                <div className={styles.type} onClick={() => search(9)}>
                  搞笑
                </div>
                <div className={styles.type} onClick={() => search(10)}>
                  玄幻
                </div>
                <div className={styles.type} onClick={() => search(11)}>
                  散文
                </div>
                <div className={styles.type} onClick={() => search(12)}>
                  同人
                </div>
                <div className={styles.type} onClick={() => search(13)}>
                  言情
                </div>
                <div className={styles.type} onClick={() => search(14)}>
                  女生
                </div>
                <div className={styles.type} onClick={() => search(15)}>
                  其他
                </div>
              </div>
            </>
          ) : null}
        </div>
      </IonContent>
    </IonPage>
  );
};

export default SearchPage;
