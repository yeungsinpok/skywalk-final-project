import DesktopEditBar from "../components/DesktopEditBar";
import DesktopViewBookElement from "../components/DesktopViewBookElement";
import { RootState } from "../redux/state";
import styles from "./scss/DesktopViewBook.module.scss";
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from "@ionic/react";
import { Redirect, useHistory, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import barStyles from "../components/scss/DesktopBar.module.scss";
import { useEffect, useState } from "react";
import { AdminGetBookInfo, WriterBookInfo } from "../model/book-content"
import { goDesktopSide, quitDesktopSide } from "../redux/desktop/action";
import { logout } from "../redux/auth/action";
import AdminManageBookElement from "../components/AdminManageBookElement";
import { adminQuitManaging } from "../redux/book-content/action";
import AdminEditBar from "../components/AdminEditBar";

const AdminManageBook = () => {
  let writerId = ~~useParams<{ writerId: string }>().writerId;
  const name = useSelector((state: RootState) => state.auth.user?.name);
  const [adminGetBookList, setAdminGetBookList] = useState<AdminGetBookInfo[] | undefined>(undefined);

  const user = useSelector((state: RootState) => state.auth.user);
  const isEditing = useSelector((state:RootState)=>state.bookContent.isEditing);
  const history = useHistory();
  const dispatch = useDispatch();
  const adminManaging = useSelector((state:RootState)=>state.bookContent.adminIsManaging);
  

  function logoutNow() {
    dispatch(quitDesktopSide(false));
    dispatch(logout());
    localStorage.removeItem('access_token');
    history.push("/");
  }

  async function loadWriterBook(userId: number) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/book_content/admin_book_record/${userId}`, {
        method: 'GET'
      })
      json = await res.json();
    } catch (error) {
      console.error('failed to call load writer book record API:', error);
      return
    }

    if (json.error) {
      console.error('load writer book record response error:', json.error);
      return
    }

    let writerBookRec: AdminGetBookInfo[] = json.writerBookRec;
    console.log("load writer book record successfully");
    console.log(json.writerBookRec)
    console.log(writerBookRec)
    
    setAdminGetBookList(writerBookRec);
    return
  }

  useEffect(() => {
    dispatch(goDesktopSide(true));
    if (user?.is_admin === true) {
      loadWriterBook(writerId);
    }
  }, [isEditing]);


  useEffect(()=>{
     if (user?.is_admin === true) {
      loadWriterBook(writerId);
    }
    dispatch(adminQuitManaging(false))
  },[adminManaging])

  useEffect(()=>{
    if (user?.is_admin === true) {
      loadWriterBook(writerId);
    }
    dispatch(adminQuitManaging(false))
  },[])


  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>
            <div className={barStyles.accountBox}>
              <div className={barStyles.accountName}>
                您好, <span>{name}</span>
              </div>
              <div className={barStyles.block}> | </div>
              <div
                className={barStyles.logoutButton}
                onClick={() => logoutNow()}
              >
                登出
              </div>
            </div>
          </IonTitle>
        </IonToolbar>
        <AdminEditBar />
      </IonHeader>
      <IonContent>
        <main>
          <div className={styles.spaceBar}></div>
          {user?.is_writer ? (
            <div>
              {adminGetBookList ? (
                adminGetBookList.map((book) => (
                  <AdminManageBookElement key={book.book_id} bookInfo={book} />
                ))
              ) : (
                <div>沒有上載紀錄</div>
              )}
            </div>
          ) : (
            <div>
              {adminGetBookList ? (
                adminGetBookList.map((book) => (
                  <AdminManageBookElement key={book.book_id} bookInfo={book} />
                ))
              ) : (
                <div>沒有上載紀錄</div>
              )}
            </div>
          )}
        </main>
      </IonContent>
    </IonPage>
  );
};
export default AdminManageBook;
