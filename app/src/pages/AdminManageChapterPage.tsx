import DesktopEditBar from "../components/DesktopEditBar";
import DesktopCreateChapterElement from "../components/DesktopCreateChapterElement";
import DesktopBookElement from "../components/DesktopBookElement";
import styles from "./scss/AdminManageChapterPage.module.scss";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { useHistory, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import barStyles from "../components/scss/DesktopBar.module.scss";
import { goDesktopSide, quitDesktopSide } from "../redux/desktop/action";
import { RootState } from "../redux/state";
import { useEffect } from "react";
import { logout } from "../redux/auth/action";
import AdminManageChapterElement from "../components/AdminManageChapterElement";
import AdminEditBar from "../components/AdminEditBar";
import { adminQuitManaging } from "../redux/book-content/action";

const AdminManageChapter = () => {
  const name = useSelector((state: RootState) => state.auth.user?.name);
  const history = useHistory();
  const dispatch = useDispatch();
  
  function logoutNow() {
    dispatch(quitDesktopSide(false));
    dispatch(logout());
    localStorage.removeItem('access_token');
    history.push("/");
  }
  useEffect(() => {
    dispatch(goDesktopSide(true));
  }, []);

 

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>
            <div className={barStyles.accountBox}>
              <div className={barStyles.accountName}>
                您好, <span>{name}</span>
              </div>
              <div className={barStyles.block}> | </div>
              <div
                className={barStyles.logoutButton}
                onClick={() => logoutNow()}
              >
                登出
              </div>
            </div>
          </IonTitle>
        </IonToolbar>
        <AdminEditBar />
      </IonHeader>
      <IonContent>
        <div className={styles.background}>
          <AdminManageChapterElement />
        </div>
      </IonContent>
    </IonPage>
  );
};
export default AdminManageChapter;
