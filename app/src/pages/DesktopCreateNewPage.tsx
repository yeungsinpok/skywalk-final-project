import DesktopEditBar from "../components/DesktopEditBar";
import elementStyles from "../components/scss/CreateBookElement.module.scss";
import sampleBook from "../components/img/book.jpg";
import {
  IonButton,
  IonCheckbox,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonPage,
  IonRadio,
  IonRadioGroup,
  IonTitle,
  IonToolbar,
  useIonRouter,
} from "@ionic/react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import barStyles from "../components/scss/DesktopBar.module.scss";
import { goDesktopSide, quitDesktopSide } from "../redux/desktop/action";
import { RootState } from "../redux/state";
import { SetStateAction, useEffect, useRef, useState } from "react";
import { logout } from "../redux/auth/action";

const DesktopCreateNew = () => {
  const name = useSelector((state: RootState) => state.auth.user?.name);
  const history = useHistory();
  const dispatch = useDispatch();

  function logoutNow() {
    dispatch(quitDesktopSide(false));
    dispatch(logout());
    localStorage.removeItem("access_token");
    history.push("/");
  }
  useEffect(() => {
    dispatch(goDesktopSide(true));
    setPageData({
      ...initialPageData,
      author: name
    });
  }, []);

  useEffect(() => {
    setPageData({
      ...initialPageData,
      author: name
    });

    try {
      const titleInput: any = document.querySelector('.titleInput')
      const abstractInput: any = document.querySelector('.abstractInput')
      const checkboxes: any = document.querySelectorAll('ion-checkbox')
      const radios: any = document.querySelectorAll('ion-radio')

      for (let checkbox of checkboxes) {
        checkbox.checked = false
      }
      titleInput.value = ""
      abstractInput.value = ""

      for (let radio of radios) {
        radio.classList.remove('radio-checked')
      }

    } catch (error) {
      console.log((error as Error).toString())
    }

  }, [history.location])

  function onCategoryCheckboxClick(
    selectedCategory: BookCategories,
    checked: boolean
  ) {
    let categories;

    if (checked) {
      categories = [...pageData.categories, selectedCategory];
    } else {
      categories = pageData.categories.filter(
        (current) => current !== selectedCategory
      );
    }

    setPageData({
      ...pageData,
      categories,
    });
  }

  const [pageData, setPageData] = useState<CreateNewPageInfo>(initialPageData);

  const onSubmitClick = () => {
    const dataIsValid = checkSubmittedData(pageData);

    if (!dataIsValid) {
      console.log('Data test failed')
      return;
    }
    let formData = new FormData();
    formData.append("cover_image", pageData.imageFileObject);
    formData.append("title", pageData.title as string);
    formData.append("author", pageData.author as string);
    formData.append("abstract", pageData.abstract as string);
    formData.append("is_end", pageData.isEnd ? "true" : "false");

    // insert category data to form data
    for (let key in BookCategories) {
      // if the key is a word not a number string
      if (isNaN(Number(key))) {
        const isSelected = pageData.categories.some(
          (selectedCat) => BookCategories[selectedCat] === key
        );
        formData.append(key, `${isSelected}`);
      }
    }

    submitFunction(formData)
      .then((res) => {
        if (res.success) {
          console.log("upload book successfully");
          setPageData(initialPageData)
          router.push('/desktop_create_chapter_success', 'none');
        } else {
          throw new Error("upload book response error:" + res.error);
        }
      })
      .catch((e) => {
        // deal with the error
        console.log((e as Error).toString())
      });
  };

  async function submitFunction(formData: FormData): Promise<{
    success: boolean,
    error?: string
  }> {
    let origin;
    let { REACT_APP_API_SERVER } = process.env;
    if (!REACT_APP_API_SERVER) throw new Error("missing REACT_APP_API_SERVER in env");
    origin = REACT_APP_API_SERVER;

    let json: any;
    try {
      const res = await fetch(`${origin}/book_content/upload_book`, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
        body: formData,
      });
      json = await res.json();
    } catch (error) {
      throw new Error("failed to upload book API:" + (error as Error).toString())
    }

    return json
  }

  const fileHandler = (event: any) => {
    const imageNameArray: Array<any> = event.target.files;
    let image: string | undefined;

    if (imageNameArray.length === 0) image = undefined;
    image = imageNameArray[0].name

    setPageData({
      ...pageData,
      imageFileObject: event.target.files[0],
      image
    });
  };

  const router = useIonRouter();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>
            <div className={barStyles.accountBox}>
              <div className={barStyles.accountName}>
                您好, <span>{name}</span>
              </div>
              <div className={barStyles.block}> | </div>
              <div
                className={barStyles.logoutButton}
                onClick={() => logoutNow()}
              >
                登出
              </div>
            </div>
          </IonTitle>
        </IonToolbar>
        <DesktopEditBar />
      </IonHeader>
      <IonContent>
        <main>
          <div className={elementStyles.createBookArea}>
            <div className={elementStyles.coverArea}>
              <img
                src={
                  pageData.imageFileObject
                    ? URL.createObjectURL(pageData.imageFileObject)
                    : undefined
                }
                className={elementStyles.bookCover + " previewImage"}
              />
              <input
                id="book-cover"
                type="file"
                className={elementStyles.uploadInput}
                onChange={fileHandler}
              />
              <label htmlFor="book-cover" className={elementStyles.uploadLabel}>
                上載小說封面
              </label>
              {imageIsOk(pageData.image) ? null : (
                <div className={elementStyles.reminder}>
                  沒有上傳小說封面圖片
                </div>
              )}
              <div className={elementStyles.uploadBox}></div>
            </div>
            <div className={elementStyles.createBookFormArea}>
              <div className={elementStyles.textArea}>
                <div className={elementStyles.inputBox}>
                  <label
                    htmlFor="bookTitle"
                    className={elementStyles.inputLabel}
                  >
                    小說書名
                    <span className={elementStyles.inputPS}> (15字以內)</span>
                  </label>
                  <br />
                  <input
                    type="text"
                    className={elementStyles.input + " titleInput"}
                    onChange={(e) =>
                      setPageData({
                        ...pageData,
                        title: e.target.value,
                      })
                    }
                  />

                  <div className={elementStyles.reminder}>
                    小說書名字數最多15字，不能留空
                  </div>
                </div>

                <div className={elementStyles.inputBox}>
                  <label htmlFor="penName" className={elementStyles.inputLabel}>
                    筆名
                    <span className={elementStyles.inputPS}> (10字以內)</span>
                  </label>
                  <input
                    type="text"
                    className={elementStyles.input}
                    value={name}
                    // onChange={(e) => setAuthor(e.target.value)}
                    disabled
                  />
                </div>

                <div className={elementStyles.inputBox}>
                  <label
                    htmlFor="bookSummary"
                    className={elementStyles.inputLabel}
                  >
                    內容簡介
                    <span className={elementStyles.inputPS}> (80字以內)</span>
                  </label>
                  <textarea
                    className={elementStyles.inputSummary  + " abstractInput"}
                    onChange={(e) =>
                      setPageData({
                        ...pageData,
                        abstract: e.target.value,
                      })
                    }
                  />

                  <div className={elementStyles.reminder}>
                    內容簡介字數要最多80字，不能留空
                  </div>
                </div>

                <div className={elementStyles.checkboxArea}>
                  <IonList>
                    <IonRadioGroup>
                      <IonListHeader className={elementStyles.checkboxTitle}>
                        小說類型
                      </IonListHeader>
                      <IonLabel className={elementStyles.reminder}>
                        請選擇小說類別(最少一個)
                      </IonLabel>
                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          異界
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.alien,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          魔法
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.magic,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          校園
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.campus,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          愛情
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.love,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          懸疑
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.detective,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          科幻
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.scific,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          冒險
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.adventure,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          武俠
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.martial,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          搞笑
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.fun,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          玄幻
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.mystery,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          散文
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.prose,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          同人
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.fan,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          言情
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.romance,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          女生
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.girls,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          其他
                        </IonLabel>
                        <IonCheckbox
                          color="primary"
                          slot="start"
                          className={elementStyles.checkbox}
                          onIonChange={(e) =>
                            onCategoryCheckboxClick(
                              BookCategories.others,
                              e.detail.checked
                            )
                          }
                        ></IonCheckbox>
                      </IonItem>
                    </IonRadioGroup>
                  </IonList>

                  <IonList>
                    <IonRadioGroup
                      onIonChange={(e) => {
                        setPageData({
                          ...pageData,
                          isEnd: e.detail.value,
                        });
                      }}
                    >
                      <IonListHeader className={elementStyles.checkboxTitle}>
                        連載狀態
                      </IonListHeader>
                      <IonLabel className={elementStyles.reminder}>
                        請選擇連載狀態
                      </IonLabel>
                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          連載中
                        </IonLabel>
                        <IonRadio
                          color="warning"
                          slot="start"
                          className={elementStyles.checkbox}
                          value={false}
                        ></IonRadio>
                      </IonItem>

                      <IonItem>
                        <IonLabel className={elementStyles.checkboxLabel}>
                          已完結
                        </IonLabel>
                        <IonRadio
                          color="success"
                          slot="start"
                          className={elementStyles.checkbox}
                          value={true}
                        ></IonRadio>
                      </IonItem>
                    </IonRadioGroup>
                  </IonList>
                </div>
                <div className={elementStyles.submitBox}>
                  <IonButton
                    color="danger"
                    size="small"
                    className={elementStyles.submitBtn}
                    onClick={onSubmitClick}
                  >
                    確定
                  </IonButton>
                </div>
              </div>
            </div>
          </div>
        </main>
      </IonContent>
    </IonPage>
  );
};
export default DesktopCreateNew;

function imageIsOk(image: string | undefined) {
  if (!image) return false;

  if (image.length === 0) {
    return false;
  } else {
    return true;
  }
}

type CreateNewPageInfo = {
  image?: string;
  imageFileObject?: any;
  title?: string;
  author?: string;
  abstract?: string;
  isEnd?: boolean;
  categories: BookCategories[];
};

const initialPageData: CreateNewPageInfo = {
  image: undefined,
  imageFileObject: undefined,
  title: undefined,
  author: undefined,
  abstract: undefined,
  isEnd: undefined,
  categories: [],
};

enum BookCategories {
  alien = 0,
  magic,
  campus,
  love,
  detective,
  scific,
  adventure,
  martial,
  fun,
  mystery,
  prose,
  fan,
  romance,
  girls,
  others,
}

function checkSubmittedData(pageData: CreateNewPageInfo): boolean {
  if (!pageData.author) return false;
  if (!pageData.title) return false;
  if (!pageData.abstract) return false;
  if (!pageData.image) return false;
  if (pageData.isEnd === undefined) return false;

  const titleRegex = /^(\w|\W){1,15}$/;
  const authorRegex = /^(\w|\W){1,10}$/;
  const abstractRegex = /^(\w|\W){1,80}$/;

  let testPassed: boolean = true;
  testPassed =
    testPassed &&
    titleRegex.test(pageData.title) &&
    authorRegex.test(pageData.author) &&
    abstractRegex.test(pageData.abstract) &&
    imageIsOk(pageData.image) &&
    pageData.categories.length === 0
      ? false
      : true;

  return testPassed;
}
