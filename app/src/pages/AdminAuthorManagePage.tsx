
import AdminEditBar from "../components/AdminEditBar";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import barStyles from "../components/scss/DesktopBar.module.scss";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { goDesktopSide, quitDesktopSide } from "../redux/desktop/action";
import { useEffect, useState } from "react";
import tableStyles from "../components/scss/AdminAuthorManageTable.module.scss";
import { UserList, WriterList } from "../model/book-content";
import { logout } from "../redux/auth/action";
import AdminAuthorManageElement from "../components/AdminAuthorManageElement";
import { isChangeToUser, isChangeToWriter, leaveIsChangeToUser, leaveIsChangeToWriter } from "../redux/book-content/action";
import Space from "../components/Space";

const AdminAuthorManage = () => {
   // work later
  const changeToUser = useSelector((state: RootState) => state.bookContent.isChangeToUser);
  const changeToWriter = useSelector((state: RootState) => state.bookContent.isChangeToWriter);
  const name = useSelector((state: RootState) => state.auth.user?.name);
  const history = useHistory();
  const dispatch = useDispatch();
  const [writerList, setWriterList] = useState<WriterList[] | undefined>(undefined);
  const [userList, setUserList] = useState<UserList[] | undefined>(undefined);

  function logoutNow() {
    dispatch(quitDesktopSide(false));
    dispatch(logout());
    localStorage.removeItem('access_token');
    history.push("/");
  }

  // GetList// GetList// GetList// GetList// GetList// GetList// GetList// GetList// GetList// GetList

  async function getWriterList() {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      return;
    }
    let json: any;
    try {
      let res = await fetch(`${origin}/admin/writer_list`, {
        method: "GET",
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call load latest update list API:", error);

      return;
    }

    if (json.error) {
      console.error("load latest update list response error:", json.error);

      return;
    }

    let writerListResult: WriterList[] = json.writerList;
    setWriterList(writerListResult);
    try {
      console.log("load latest update list successfully");
      console.log(writerListResult);
      return;
    } catch (error) {
      console.error("failed to dispatch load writer chapter record action", error);
    }
  }

  async function getUserList() {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      return;
    }
    let json: any;
    try {
      let res = await fetch(`${origin}/admin/user_list`, {
        method: "GET",
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call load latest update list API:", error);
      return;
    }
    if (json.error) {
      console.error("load latest update list response error:", json.error);
      return;
    }
    let userListResult: UserList[] = json.userList;
    setUserList(userListResult);
    try {
      console.log("load latest update list successfully");
      console.log(userListResult);
      return;
    } catch (error) {
      console.error("failed to dispatch load update list action", error);
    }
  }

  useEffect(() => {
    getWriterList();
    dispatch(goDesktopSide(true));
  }, []);


  useEffect(() => {
    getWriterList();
    getUserList();
    dispatch(leaveIsChangeToWriter(false));
  }, [changeToWriter]);

  useEffect(() => {
    getWriterList();
    getUserList();
    dispatch(leaveIsChangeToUser(false));
  }, [changeToUser]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>
            <div className={barStyles.accountBox}>
              <div className={barStyles.accountName}>
                您好, <span>{name}</span>
              </div>
              <div className={barStyles.block}> | </div>
              <div
                className={barStyles.logoutButton}
                onClick={() => logoutNow()}
              >
                登出
              </div>
            </div>
          </IonTitle>
        </IonToolbar>
        <AdminEditBar />
      </IonHeader>
      <IonContent>
        {/* <AdminAuthorManageTable /> */}
        <main>
          <div className={tableStyles.accountManageTable}>
            <div className={tableStyles.accountManageBtn}>用戶 ID</div>
            <div className={tableStyles.accountManageBtn}>用戶名稱</div>
            <div className={tableStyles.accountManageBtn}>用戶電郵</div>
            <div className={tableStyles.accountManageBtn}>註冊日期</div>
            <div className={tableStyles.accountManageBtn}>解除作者權限</div>
            <div className={tableStyles.accountManageBtn}>上架新小說</div>
          </div>

          {writerList ? (
            writerList.map((result) => (
              <AdminAuthorManageElement key={result.id} resultInfo={result} />
            ))
          ) : (
            <div className={barStyles.updatedBox} color="secondary">
              沒有找到搜尋結果....
            </div>
          )}
          <Space />
        </main>
      </IonContent>
    </IonPage>
  );
};

export default AdminAuthorManage;
