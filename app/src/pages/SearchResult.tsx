import { IonBackButton, IonButtons, IonContent, IonHeader,IonPage,  IonTitle,  IonToolbar, useIonRouter} from "@ionic/react"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { SearchByCatList } from "../model/book-content";
import { isSearching } from "../redux/book-content/action";
import { RootState } from "../redux/state";
import styles from "./scss/SearchResult.module.scss";
import sampleBook from "../components/img/book.jpg";
import { format } from "date-fns";
import Space from "../components/Space";
import SearchResultElement from "../components/SearchResultElement";

const SearchResult = ()=> {
 
  let catId = ~~useParams<{ categoryId: string }>().categoryId;
  console.log('cat id:', catId);
  const dispatch = useDispatch();
  const searched = useSelector((state:RootState)=>state.bookContent.isSearching);
  const [searchResultRec, setSearchResultRec] = useState<SearchByCatList[] | undefined>(undefined);

  async function getSearchResultByCatId(catId: number) {
  
        let origin;
        try {
            let { REACT_APP_API_SERVER } = process.env
            if (!REACT_APP_API_SERVER) {
                console.error('missing REACT_APP_API_SERVER in env');
                throw new Error('missing REACT_APP_API_SERVER in env');
            }
            origin = REACT_APP_API_SERVER;
        } catch (error) {
          console.log(error);
            return;
        }
  
        let json: any;
        try {
            let res = await fetch(`${origin}/search/by_category_id/${catId}`, {
                method: 'GET'
            })
            json = await res.json();
        } catch (error) {
            console.error('failed to call load latest update list API:', error);
  
            return
        }
  
        if (json.error) {
            console.error('load latest update list response error:', json.error);
      
            return
        }
  
        let searchCatResultList: SearchByCatList[] = json.searchByCatResult;
        setSearchResultRec(searchCatResultList)
  
        try {
            console.log("load latest update list successfully");
            console.log(searchCatResultList);
            if (searchCatResultList.length > 0) {
              return 
            }
            return
        } catch (error) {
            console.error('failed to dispatch load latest update list action', error);
  
        }
        dispatch(isSearching(true));
        getSearchResultByCatId(catId);  
    }
    

    useEffect(() => {
      getSearchResultByCatId(catId);    
      dispatch(isSearching(false));
    }, [searched]);



  const router = useIonRouter()
    return (
    <IonPage>
    <IonHeader>
    <IonToolbar color="primary">
    <IonButtons slot="start">
      <IonBackButton mode="md"></IonBackButton>
      </IonButtons>
      <IonTitle className={styles.appTitle}>搜尋結果</IonTitle>

        </IonToolbar>
      </IonHeader>
      <IonContent>
          <div className={styles.area}>
                  

          {searchResultRec ? 
            searchResultRec.map(result => (
              <SearchResultElement 
                key={result.book_id}
                resultInfo={result} />
              ))
              : 
              <><div className={styles.area} color="secondary">沒有找到搜尋結果....</div></>
         
          }
  </div>
  <Space />
  </IonContent>
          </IonPage>
      )
}

export default SearchResult;