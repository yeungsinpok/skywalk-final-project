import DesktopEditBar from "../components/DesktopEditBar";
import DesktopEditChapterElement from "../components/DesktopEditChapterElement";
import DesktopBookElement from "../components/DesktopBookElement";
import styles from "./scss/DesktopEditChapter.module.scss";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import barStyles from "../components/scss/DesktopBar.module.scss";
import { goDesktopSide, quitDesktopSide } from "../redux/desktop/action";
import { RootState } from "../redux/state";
import { useEffect } from "react";
import { logout } from "../redux/auth/action";

const DesktopEditChapter = () => {
  const name = useSelector((state: RootState) => state.auth.user?.name);
  const history = useHistory();
  const dispatch = useDispatch();

  function logoutNow() {
    dispatch(quitDesktopSide(false));
    dispatch(logout());
    localStorage.removeItem('access_token');
    history.push("/");
  }

  useEffect(() => {
    dispatch(goDesktopSide(true));
  }, []);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>
            <div className={barStyles.accountBox}>
              <div className={barStyles.accountName}>
                您好, <span>{name}</span>
              </div>
              <div className={barStyles.block}> | </div>
              <div
                className={barStyles.logoutButton}
                onClick={() => logoutNow()}
              >
                登出
              </div>
            </div>
          </IonTitle>
        </IonToolbar>
        <DesktopEditBar />
      </IonHeader>
      <IonContent>
        <main>
          <div className={styles.background}>
            <DesktopBookElement />
            <DesktopEditChapterElement />
          </div>
        </main>
      </IonContent>
    </IonPage>
  );
};
export default DesktopEditChapter;
