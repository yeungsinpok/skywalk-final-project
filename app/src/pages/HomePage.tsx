
import { IonButton, IonButtons, IonContent, IonHeader, IonIcon, IonPage, IonToolbar, useIonRouter } from '@ionic/react';
import { useEffect, useState } from 'react';
import styles from "./scss/HomePage.module.scss";
import btnStyles from "../components/scss/HomeButtonBar.module.scss";
import advStyles from "../components/scss/HomeAdv.module.scss";
import rankStyles from "../components/scss/HomeRanking.module.scss";


import {
  library,
  searchCircle,
  searchCircleOutline,
  searchOutline
} from "ionicons/icons";
import NavBarStyles from "../components/scss/NavBar.module.scss";
import advImg from "../components/img/adv.jpg";
import RankingBook from '../components/RankingBook';
import { BookSummaryInfo } from '../model/book-content';
import { isSearching } from '../redux/book-content/action';
import { useDispatch, useSelector } from 'react-redux';
import { BookAd } from '../model/ad';
import { RootState } from '../redux/state';
import { toImageUrl } from '../helpers/api';


const HomePage = () => {
  const router = useIonRouter();
  const dispatch = useDispatch();
  // const rankList = useSelector((state:RootState)=>state.bookContent.rankList);
  // const dispatch = useDispatch();
  
  // useEffect(()=>{
  //   if (rankList === undefined) {
  //     dispatch(loadRankThunk());
  //   }
  // }, []);


// rankinglist logic
const [rankList, setRankList] = useState<BookSummaryInfo[] | undefined>(undefined);
const [ad, setAd] = useState<BookAd[] | undefined>(undefined);
const isReading = useSelector((state: RootState) => state.bookContent.isReading);
const searched = useSelector((state:RootState)=>state.bookContent.isSearching);
const collected = useSelector((state: RootState) => state.bookContent.isCollected);

function search(id: number) {
  router.push(`/searching/by_category_id/${id}`, "none");
  dispatch(isSearching(true));
}


async function loadRank() {
  let origin;
  try {
    let { REACT_APP_API_SERVER } = process.env
    if (!REACT_APP_API_SERVER) {
      console.error('missing REACT_APP_API_SERVER in env');
      throw new Error('missing REACT_APP_API_SERVER in env');
    }
    origin = REACT_APP_API_SERVER;
  } catch (error) {
    console.log(error);
    return;
  }

  let json: any;
  try {
    let res = await fetch(`${origin}/book_content/rank`, {
      method: 'GET'
    })
    json = await res.json();
  } catch (error) {
    console.error('failed to call load book rank API:', error);
    return
  }

  if (json.error) {
    console.error('load book rank response error:', json.error);
    return
  }

  let rankList: BookSummaryInfo[] = json.rankList;

  try {
    console.log("load book rank successfully");
    setRankList(rankList);
    return

  } catch (error) {
    console.error('failed to dispatch load rank action', error);
  }
}

async function loadRandomAd() {
  let origin;
  try {
    let { REACT_APP_API_SERVER } = process.env
    if (!REACT_APP_API_SERVER) {
      console.error('missing REACT_APP_API_SERVER in env');
      throw new Error('missing REACT_APP_API_SERVER in env');
    }
    origin = REACT_APP_API_SERVER;
  } catch (error) {
    console.log(error);
    return;
  }

  let json: any;
  try {
    let res = await fetch(`${origin}/admin/adv_list/random`, {
      method: 'GET'
    })
    json = await res.json();
  } catch (error) {
    console.error('failed to call load book rank API:', error);
    return
  }

  if (json.error) {
    console.error('load book rank response error:', json.error);
    return
  }

  let randomAd: BookAd[] = json.randomAd;
  console.log(randomAd);

  try {
    console.log("load book rank successfully");
    setAd(randomAd);
    return

  } catch (error) {
    console.error('failed to dispatch load rank action', error);
  }
} 

useEffect(() => {
  loadRank();
  loadRandomAd();
}, []);
// rankinglist logic

useEffect(() => {
  loadRandomAd();
}, [null || isReading || searched || collected]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonButton routerLink="/search">
              <IonIcon icon={searchCircle} className={styles.iconSizeSearch}></IonIcon>
            </IonButton>
            </IonButtons>
            <IonButtons slot="end">
            <IonButton routerLink="/book_shelf">
            <IonIcon icon={library}  className={styles.iconSize} size="large" ></IonIcon>
            </IonButton>
            </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
    <main>
      {/* <HomeAdv /> */}
      <div className={advStyles.homeAdv}>
        <div className={advStyles.advBar}>
        {/* <img alt={(ad)?ad[0].title:''} src={advImg} className={advStyles.advImg}></img> */}
        {
          ad?
          <img alt={(ad) ? '《' + ad[0].title + '》的廣告': ''} src={toImageUrl(ad[0].image)} className={advStyles.advImg}></img>
          :
          <div></div>
        }
        </div>
      </div>
      {/* <HomeButtonBar /> */}
      <div className={btnStyles.homeMiddle}>
        <h2 className={btnStyles.homeRanking}>人氣排行榜</h2>
        <div className={btnStyles.homeButtonBar}>
          <div className={btnStyles.homeButton}onClick={() => search(6)}>科幻</div>
          <div className={btnStyles.homeButton} onClick={() => search(4)}>愛情</div>
          <div className={btnStyles.homeButton} onClick={() => search(5)}>懸疑</div>
          <div className={btnStyles.homeButton} onClick={() => search(9)}>搞笑</div>
          <div className={btnStyles.homeButton} onClick={() => search(12)}>同人</div>
        </div>
      </div>
      {/* <HomeRanking /> */}
      <div className={rankStyles.homeRankingBooks} color="secondary">
      {
        rankList ?
          rankList.map((rank, index) => (
            <RankingBook
              key={rank.book_id}
              rankListInfo={rank}
              rank={index} />
          ))
          :
          <><div>沒有找到搜尋結果....</div></>
      }
    </div>

    </main>
    </IonContent>
    </IonPage>
  )
}
export default HomePage
