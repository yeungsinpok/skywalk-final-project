import { IonContent, IonItem, IonTitle } from "@ionic/react"
import styles from "./scss/ConfirmPage.module.scss";

const ConfirmPage = () => {
    return (
      <main>
        <IonContent color="secondary">
          <div className={styles.yourBookshelf}>您的收藏</div>
        </IonContent>
      </main>
    )
  }
  
  export default ConfirmPage
  