
import AdminEditBar from "../components/AdminEditBar";

import barStyles from "../components/scss/DesktopBar.module.scss";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { IonButton, IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from "@ionic/react";
import { goDesktopSide, quitDesktopSide } from "../redux/desktop/action";
import { useState, useEffect } from "react";
import tableStyles from "../components/scss/AdminAdvManageTable.module.scss";
import advImg from "../components/img/adv.jpg"
import elementStyles from "../components/scss/AdminAdvManageElement.module.scss";
import { AdvList } from '../model/book-content';
import { format } from 'date-fns';
import { logout } from "../redux/auth/action";
import Space from "../components/Space";
import { toImageUrl } from "../helpers/api";

const AdminAdvManage = () => {
  const name = useSelector((state: RootState) => state.auth.user?.name);
  const history = useHistory();
  const dispatch = useDispatch();
  const [advList, setAdvList] = useState<AdvList[] | undefined>(undefined);

  function logoutNow() {
    dispatch(quitDesktopSide(false));
    dispatch(logout());
    localStorage.removeItem('access_token');
    history.push("/");
  }
  useEffect(() => {
    dispatch(goDesktopSide(true));
  }, []);

  async function getAdvList() {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {

      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/admin/adv_list`, {
        method: 'GET'
      })
      json = await res.json();
    } catch (error) {
      console.error('failed to call load latest update list API:', error);

      return
    }

    if (json.error) {
      console.error('load latest update list response error:', json.error);

      return
    }

    let advListResult: AdvList[] = json.advList;
    setAdvList(advListResult);

    console.log("load latest update list successfully");
    console.log(advListResult);

  }

  useEffect(() => {
    getAdvList();
  }, []);

  async function publishAdv(advId: number) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {

      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/admin/adv_list/up/${advId}`, {
        method: 'POST'
      })
      json = await res.json();
    } catch (error) {
      console.error('failed to call load latest update list API:', error);

      return
    }

    if (json.error) {
      console.error('load latest update list response error:', json.error);

      return
    }

    let advListResult: AdvList[] = json.advList;
    setAdvList(advListResult);

    console.log("load latest update list successfully");
    console.log(advListResult);
    await getAdvList();
  }

  async function takeDownAdv(advId: number) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {

      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/admin/adv_list/takedown/${advId}`, {
        method: 'POST'
      })
      json = await res.json();
    } catch (error) {
      console.error('failed to call load latest update list API:', error);

      return
    }

    if (json.error) {
      console.error('load latest update list response error:', json.error);

      return
    }

    let advListResult: AdvList[] = json.advList;
    setAdvList(advListResult);

    console.log("load latest update list successfully");
    console.log(advListResult);
    await getAdvList();
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>
            <div className={barStyles.accountBox}>
              <div className={barStyles.accountName}>
                您好, <span>{name}</span>
              </div>
              <div className={barStyles.block}> | </div>
              <div
                className={barStyles.logoutButton}
                onClick={() => logoutNow()}
              >
                登出
              </div>
            </div>
          </IonTitle>
        </IonToolbar>
        <AdminEditBar />
      </IonHeader>
      <IonContent>
        {/* <AdminAdvManageTable /> */}

        <main>

          <div className={tableStyles.advManageTable}>
            <div className={tableStyles.advManageBtn}>廣告圖片
            </div>
            {/* <div className={tableStyles.advManageBtn}>小說書名
            </div> */}
            <div className={tableStyles.advManageBtn}>上載時間
            </div>
            <div className={tableStyles.advManageBtn}>編輯時間
            </div>
            <div className={tableStyles.advManageBtn}>廣告狀態
            </div>
            <div className={tableStyles.advManageBtn}>管理發佈
            </div>
          
          </div>
          {/* < AdminAdvManageElement /> */}
          {advList?.map((adv, i) => (
            <div className={elementStyles.adminAdvManageBox} key={adv.id}>
              <div className={elementStyles.advBox}>
                <img alt={`《${adv.title}》的廣告`} src={toImageUrl(adv.image)} className={elementStyles.advImg}></img>
              </div>
              {/* <div className={elementStyles.bookName}>{adv.title}</div> */}
              <div className={elementStyles.date}>{format(new Date(adv.created_at), 'yyyy-MM-dd')}<br/><span>{format(new Date(adv.created_at), 'p')}</span></div>
              <div className={elementStyles.date}>{format(new Date(adv.updated_at), 'yyyy-MM-dd ')}<br/><span>{format(new Date(adv.updated_at), 'p')}</span></div>
      
           
             {adv.is_active ? 
              <div className={elementStyles.authority}>已發佈</div> 
              : <div className={elementStyles.authority}>未發佈</div>}
              <div className={elementStyles.turnAuthority}>
                {adv.is_active === false?
                <IonButton color="success" className={elementStyles.blockBtn} size="small" onClick={() => publishAdv(adv.id)}>發佈</IonButton>
                :<IonButton color="danger" className={elementStyles.unBlockBtn} size="small" onClick={() => takeDownAdv(adv.id)}>暫停</IonButton>
                  }
              </div>
             


            </div>
          ))}
          <Space />
        </main>
      </IonContent>
    </IonPage>
  );
};

export default AdminAdvManage;
