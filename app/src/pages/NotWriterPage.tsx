
import { IonButton, IonContent } from "@ionic/react";
import { Link } from "react-router-dom";
import styles from "./scss/NotWriter.module.scss";



const NotWriter = () => {
  return (
    <main>
      <IonContent>
      <div className={styles.blueBar}></div>
      
      Sorry, u are not writer
      <Link to="/desktop_index">
      <IonButton color='primary'>back to front page</IonButton>
      </Link>
      </IonContent>
    </main>
  );
};
export default NotWriter;
