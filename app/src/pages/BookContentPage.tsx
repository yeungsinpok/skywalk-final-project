import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonList,
  IonPage,
  IonPopover,
  IonTitle,
  IonToolbar,
  useIonRouter,
} from "@ionic/react";
import styles from "./scss/BookContent.module.scss";
// import ReadingProcessBar from "../components/ReadingProcessBar";
import BookChapterContent from "../components/BookChapterContent";
import Space from "../components/Space";
import { useHistory, useParams } from "react-router";
import { ChapterContentList } from "../model/book-content";
import { useEffect, useState } from "react";
import { goDesktopSide } from "../redux/desktop/action";
import { RootState } from "../redux/state";
import { loadWriterChapterThunk } from "../redux/book-content/thunk";
import { useDispatch, useSelector } from "react-redux";
import { home, menu } from "ionicons/icons";
import { clearChapterContent, loadBookRec, quitReadBookContent, readBookContent, setTotalChapter } from "../redux/book-content/action";
import { changeFontSize } from "../redux/theme/action";
import { ThemeName } from "../redux/theme/state";
import logo from "../components/img/buttonLogo.jpg"
import { ChapterGroupArray } from "../model/book-content";

const BookContent = () => {
  let bookId = ~~useParams<{ bookId: string }>().bookId;
  let chapter = ~~useParams<{ chapter: string }>().chapter;
  
  // const switchBar = useSelector((state:RootState)=>state.bookContent.isReading);
  const chapterList = useSelector((state: RootState) => state.bookContent.chapterContent);
  const userId = useSelector((state: RootState) => state.auth.user?.id);
  const dispatch = useDispatch();

  const [ChapterIdList, setChapterIdList] = useState<ChapterGroupArray[] | undefined>(undefined);
 
  async function loadChapterGroup(bookId: number | string) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/book_content/chapter_group/${bookId}`, {
        method: "GET",
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call get chapter group API:", error);
      return;
    }

    if (json.error) {
      console.error("get chapter group response error:", json.error);
      return;
    }

    let chapterGroup: ChapterGroupArray[] = json.chapterGroup;
    console.log("get chapter group successfully");
    setChapterIdList(chapterGroup);
  }
  
  const router = useIonRouter();
  const history = useHistory();

  const isReading = useSelector((state: RootState) => state.bookContent.isReading);

  function quitReadBook() {
    console.log("i am in quit reading");
    dispatch(clearChapterContent());
    dispatch(quitReadBookContent(false));
    history.push("/");
  }

  const [popoverState, setShowPopover] = useState({
    showPopover: false,
    event: undefined,
  });

  const setFontSize = (font: ThemeName) => {
    const action = changeFontSize(font);
    dispatch(action);
  };

  useEffect(()=>{
    loadChapterGroup(bookId);
  },[bookId]);


  useEffect(() => {
    if (ChapterIdList) {
      dispatch(setTotalChapter(ChapterIdList.length));
    }
  }, [ChapterIdList]);
  

  useEffect(() => {
    dispatch(readBookContent(true));
    // if (userId){
    //   addChapterView(userId, bookId, chapterBookId)
    // }
    if (chapterList) {
      dispatch(
        loadWriterChapterThunk(chapterList[0].book_id, chapterList[0].chapter)
      );
    }
  }, []);


  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonButton routerLink="/" >
              <img src={logo} className={styles.iconSizeSearch} onClick={quitReadBook}></img>
            </IonButton>
          </IonButtons>

          <IonButtons slot="end">
            <div className={styles.menuBtn}>
              <IonPopover
                cssClass="my-custom-class"
                event={popoverState.event}
                isOpen={popoverState.showPopover}
                animated={false}
                onDidDismiss={() =>
                  setShowPopover({ showPopover: false, event: undefined })
                }
              >
                <IonList>
                  <IonItem
                    button
                    className={styles.themeOption}
                    onClick={() => setFontSize("large")}
                  >
                    字體 大
                  </IonItem>
                  <IonItem
                    button
                    className={styles.themeOption}
                    onClick={() => setFontSize("middle")}
                  >
                    字體 中
                  </IonItem>

                  <IonItem
                    button
                    className={styles.themeOption}
                    onClick={() => setFontSize("small")}
                  >
                    字體 小
                  </IonItem>
                </IonList>
              </IonPopover>
              <IonButton
                onClick={(e: any) => {
                  e.persist();
                  setShowPopover({ showPopover: true, event: e });
                }}
              >
                <IonIcon icon={menu} className={styles.iconSize}></IonIcon>
              </IonButton>
            </div>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

            <BookChapterContent
              content={chapterList ? chapterList[0].content : null}
            />

    </IonPage>
  );
};

export default BookContent;
