import { IonContent, IonPage } from "@ionic/react"
import Login from "../components/Login";
import Space from "../components/Space";

const LoginPage = () => {

  return (
    <IonPage>
      <IonContent color="secondary">
        <Login />
        <Space />
      </IonContent>
  
      </IonPage>
  )
}

export default LoginPage;
