import {
  IonContent,
  IonHeader,
  IonItem,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import styles from "./scss/Bookshelf.module.scss";
import Space from "../components/Space";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { useEffect, useState } from "react";
import { CollectionList } from "../model/book-content";
import { Redirect } from "react-router";
import sampleBook from "../components/img/book.jpg";
import { format } from "date-fns";
import continueStyles from "../components/scss/BookUpdateDate.module.scss";
import elementStyles from "../components/scss/CollectedBook.module.scss";
import { isCollected } from "../redux/book-content/action";
import CollectionElement from "../components/CollectionElement";

const Bookshelf = () => {
  const user = useSelector((state: RootState) => state.auth.user);
  const userId = useSelector((state: RootState) => state.auth.user?.id);
  const [collectionList, setCollectionList] = useState<
    CollectionList[] | undefined
  >(undefined);
  const collected = useSelector((state: RootState) => state.bookContent.isCollected);

  const dispatch = useDispatch();

  async function getCollectionList(userId: number) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/collection/list/${userId}`, {
        method: "GET",
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call load collection list API:", error);
      return;
    }

    if (json.error) {
      console.error("load collection list response error:", json.error);
      return;
    }

    let collectionListResult: CollectionList[] = json.collectedBookList;
    setCollectionList(collectionListResult);

    console.log("load collection list successfully");
    console.log(collectionListResult);
  }

  

  useEffect(() => {
    if (userId) {
      console.log("i am in useEffect get collection function");
      getCollectionList(userId);
      dispatch(isCollected(false));
    }
  }, []);

  useEffect(() => {
    if (userId) {
      console.log("i am in useEffect get collection function");
      getCollectionList(userId);
      dispatch(isCollected(false));
    }
  }, [collected]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle className={styles.appTitle}>您的收藏</IonTitle>
        </IonToolbar>
      </IonHeader>

      {user?.id ? (
        <IonContent>
          <div className={styles.area}>
            {collectionList ? (
              collectionList.map((result) => (
                <CollectionElement key={result.book_id} resultInfo={result} />
              ))
            ) : (
              <div className={styles.updatedBox} color="secondary">
                沒有找到搜尋結果....
              </div>
            )}

            <Space />
          </div>
        </IonContent>
      ) : (
        <Redirect to="/about_me" />
      )}
    </IonPage>
  );
};

export default Bookshelf;
