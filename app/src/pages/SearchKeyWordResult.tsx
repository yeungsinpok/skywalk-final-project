import { IonBackButton, IonButtons, IonContent, IonHeader, IonPage, IonTitle, IonToolbar, useIonRouter } from "@ionic/react"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { SearchByCatList } from "../model/book-content";
import { isSearching, loadSearchResultSuccess } from "../redux/book-content/action";
import { RootState } from "../redux/state";
import styles from "./scss/SearchResult.module.scss";
import sampleBook from "../components/img/book.jpg";
import { format } from "date-fns";
import Space from "../components/Space";
import SearchResultElement from "../components/SearchResultElement";
import SearchKeyWordElement from "../components/SearchKeyWordElement";

const SearchKeyWordResult = () => {

  // let catId = ~~useParams<{ categoryId: string }>().categoryId;
  // const dispatch = useDispatch();
  // const searched = useSelector((state:RootState)=>state.bookContent.isSearching);
  // const [searchResultRec, setSearchResultRec] = useState<SearchByCatList[] | undefined>(undefined);
  const searchResult = useSelector((state: RootState) => state.bookContent.searchingResult);
  

  // useEffect(() => {

  //   dispatch(isSearching(false));
  // }, []);


  // useEffect(() => {

  //   dispatch(isSearching(false));
  // }, [searched]);



  const router = useIonRouter()
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton mode="md"></IonBackButton>
          </IonButtons>
          <IonTitle className={styles.appTitle}>搜尋結果</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <div className={styles.area}>

          {searchResult ?
            searchResult.map(result => (
              <SearchKeyWordElement
                key={result.book_id}
                resultInfo={result} />
            ))
            :
            <div className={styles.updatedBox} color="secondary">沒有找到搜尋結果....</div>

          }
        </div>
        <Space />
      </IonContent>
    </IonPage>
  )

}

export default SearchKeyWordResult;