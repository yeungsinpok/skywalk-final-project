import AdminEditBar from "../components/AdminEditBar";

import { goDesktopSide, quitDesktopSide } from "../redux/desktop/action";
import barStyles from "../components/scss/DesktopBar.module.scss";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import {
  IonButton,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { useEffect, useState } from "react";
import sampleAdv from "../components/img/adv.jpg";
import elementStyles from "../components/scss/CreateAdvElement.module.scss";
import { logout } from "../redux/auth/action";

const AdminCreateAdv = () => {
  const name = useSelector((state: RootState) => state.auth.user?.name);
  const history = useHistory();
  const dispatch = useDispatch();

  function logoutNow() {
    dispatch(goDesktopSide(false));
    dispatch(quitDesktopSide(false));
    dispatch(logout());
    localStorage.removeItem('access_token');
    history.push("/");
    window.location.reload();
  }

  useEffect(() => {
    dispatch(goDesktopSide(true));
  }, []);

  const [image, setImage] = useState('');
  const [title, setTitle] = useState('');
  const [titleOk, setTitleOk] = useState(true);
  const [imageOk, setImageOk] = useState(true);
  const titleRegex = /^(\w|\W){1,15}$/;

  const checkInput = () => {
    if (!titleRegex.test(title)) {
      setTitleOk(false);
    } else {
      setTitleOk(true);
    }

    if (!image) {
      setImageOk(false);
    } else {
      setImageOk(true);
    }

    if(titleOk === true && imageOk === true) {
      let formData = new FormData();
      formData.append('title', title);
      formData.append('image', image);
      submitFunction(formData);
    }
  }

  async function submitFunction(formData: FormData) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      const res = await fetch(`${origin}/book_content/upload_adv_photo`, {
        method: "POST",
        headers: {
          "Authorization": `Bearer ${localStorage.getItem('access_token')}`
        },
        body: formData
      });
      json = await res.json();
    } catch (error) {
      console.error('failed to upload adv photo API:', error);
      return
    }

    if (json.error) {
      console.error('upload adv photo response error:', json.error);
      return
    }

    if (json.status == 200) {
      console.log(json.json);
      return;
    }
  }

  const fileHandler = (event: any) => {
    setImage(event.target.files[0])
  };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>
            <div className={barStyles.accountBox}>
              <div className={barStyles.accountName}>
                您好, <span>{name}</span>
              </div>
              <div className={barStyles.block}> | </div>
              <div
                className={barStyles.logoutButton}
                onClick={() => logoutNow()}
              >
                登出
              </div>
            </div>
          </IonTitle>
        </IonToolbar>
        <AdminEditBar />
      </IonHeader>
      <IonContent>
      <main>
      <div className={elementStyles.createBookArea}>
        <div className={elementStyles.coverArea}>
          <img src={image ? URL.createObjectURL(image) : undefined} className={elementStyles.advImg}></img>
          <input type="file" className={elementStyles.upload} onChange={fileHandler} />
          {
                imageOk ?
                  null :
                  <div className={elementStyles.reminder}>沒有上傳廣告圖片</div>
              }
          <div className={elementStyles.uploadBox}>
            <IonButton color="light" className={elementStyles.uploadBtn}>
              上傳廣告圖片
            </IonButton>
            <div className={elementStyles.inputBox}>
              <label htmlFor="bookTitle" className={elementStyles.inputLabel}>
                小說書名:
              </label>
              <input type="text" className={elementStyles.input} onChange={e => setTitle(e.target.value)}/>
              {
                    titleOk ?
                      null :
                      <div className={elementStyles.reminder}>必須是已上載小說的名字，不能留空或多於15字</div>
                  }
            </div>
          </div>
          <div className={elementStyles.submitBox}>
            <IonButton color="success" size="small" className={elementStyles.submitBtn} onClick={checkInput}>
              確定
            </IonButton>
          </div>
        </div>
      </div>
    </main>
      </IonContent>
    </IonPage>
  );
};
export default AdminCreateAdv;
