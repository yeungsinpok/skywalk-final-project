import { IonContent, IonPage } from "@ionic/react"
import Register from "../components/Register";

const RegisterPage = () => {

  return (
    <IonPage>
      <IonContent color="secondary">
        <Register />
      </IonContent>
      </IonPage>
  )
}

export default RegisterPage;