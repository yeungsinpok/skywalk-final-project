import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonInput,
  IonLoading,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import styles from "./scss/Book.module.scss";
import chapterStyles from "../components/scss/BookChapter.module.scss";
import Space from "../components/Space";
import { useParams } from "react-router";
import { useEffect, useState } from "react";
import { BookSummaryInfo } from "../redux/book-content/state";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { searchOutline } from "ionicons/icons";
import bookInfoStyles from "../components/scss/BookInfo.module.scss";
import { format } from "date-fns";
import sampleBook from "../components/img/book.jpg";
import bookIntroStyles from "../components/scss/BookIntro.module.scss";
import BookChapterLatestUpdateElement from "../components/BookChapterLatestUpdateElement";
import BookChapterElement from "../components/BookChapterElement";
import bookCommentStyles from "../components/scss/BookComment.module.scss";
import commentDataStyles from "../components/scss/BookCommentData.module.scss";

import {
  ChapterGroupArray,
  CollectionList,
  CommentList,
} from "../model/book-content";
import BookComment from "./BookComment";
import { adminQuitManaging, isCollected, isSearching, setTotalChapter } from "../redux/book-content/action";
import { addBookViewThunk } from "../redux/view/thunk";
import { toImageUrl } from "../helpers/api";

const BookPage = () => {
  let bookId = ~~useParams<{ id: string }>().id;
  const [text, setText] = useState<string>();
  const userId = useSelector((state: RootState) => state.auth.user?.id);
  const [singleBook, setSingleBook] = useState<BookSummaryInfo | undefined>(
    undefined
  );
  const [chapterList, setChapterList] = useState<
    ChapterGroupArray[] | undefined
  >(undefined);
  const [isCollect, setIsCollect] = useState<boolean | undefined>(undefined);
  const [commentList, setCommentList] = useState<CommentList[] | undefined>(
    undefined
  );
  const searched = useSelector((state:RootState)=>state.bookContent.isSearching);
  const dispatch = useDispatch();
  const adminManaging = useSelector((state:RootState)=>state.bookContent.adminIsManaging);

  // let [book, setBook] = useState<BookContentState>();

  // useEffect(() => {
  //   // TODO fetch
  //   setTimeout(() => {
  //     setBook({} as any);
  //   }, 1000);
  // }, [bookId]);

  // bookinfo logic
  // const isCollected = useSelector(
  //   (state: RootState) => state.bookContent.isCollected
  // );
  // const individualBookInfo = useSelector(
  //   (state: RootState) => state.bookContent.individualBookInfo
  // );
  // const book_Id = useSelector(
  //   (state: RootState) => state.bookContent.individualBookInfo?.book_id
  // );
  // const dispatch = useDispatch();

  function addCollection() {
    console.log("i am in adding collection");
    if (userId && bookId) {
      addCollectionRec(userId, bookId);
      setIsCollect(true);
      dispatch(isCollected(true));
    }
  }

  // function checkCollection() {
  //   console.log("i am in adding collection");
  //   if (userId && book_Id) {
  //     dispatch(checkCollectionThunk(userId, parseInt(book_Id)));
  //   }
  // }

  async function loadSingleBookInfo(bookId: number) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/book_content/bookId/${bookId}`, {
        method: "GET",
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call single book record API:", error);
      return;
    }

    if (json.error) {
      console.error("load single book record response error:", json.error);
      return;
    }

    let bookInfo: BookSummaryInfo = json.singleBook[0];
    console.log("load single book record successfully");
    setSingleBook(bookInfo);
    return;
  }

  async function loadChapterGroup(bookId: number | string) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/book_content/chapter_group/${bookId}`, {
        method: "GET",
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call get chapter group API:", error);
      return;
    }

    if (json.error) {
      console.error("get chapter group response error:", json.error);
      return;
    }

    let chapterGroup: ChapterGroupArray[] = json.chapterGroup;
    console.log("get chapter group successfully");
    setChapterList(chapterGroup);
  }

  async function checkCollection(userId: number, bookId: number) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/collection/check/${userId}/${bookId}`, {
        method: "GET",
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call check collection API:", error);
      return;
    }

    if (json.error) {
      console.error("check collection response error:", json.error);
      return;
    }

    let checkCollectedRecord: boolean = json.checkCollectedRecord;
    console.log("check collection successfully");
    setIsCollect(checkCollectedRecord);
    return;
  }

  async function addCollectionRec(userId: number, bookId: number) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/collection/add/${userId}/${bookId}`, {
        method: "POST",
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call add collection API:", error);
      return;
    }

    if (json.error) {
      console.error("add collection response error:", json.error);
      return;
    }

    try {
      if (json.status === 200) {
        console.log("add collection successfully");
        return;
      }
    } catch (error) {
      console.error("failed to dispatch add collection action", error);
    }
  }

  async function loadComment(bookId: number) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/comment/book/${bookId}`, {
        method: "GET",
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call comment API:", error);
      return;
    }

    if (json.error) {
      console.error("get comment response error:", json.error);
      return;
    }

    let commentResult: CommentList[] = json.commentRows;
    console.log("get comment successfully");
    setCommentList(commentResult);
  }

async function newComment(userId: number, comment: string, bookId: number) {
  let origin;
  try {
    let { REACT_APP_API_SERVER } = process.env;
    if (!REACT_APP_API_SERVER) {
      console.error("missing REACT_APP_API_SERVER in env");
      throw new Error("missing REACT_APP_API_SERVER in env");
    }
    origin = REACT_APP_API_SERVER;
  } catch (error) {
    console.log(error);
    return;
  }
  let json: any;
  try {
    const keyWord = {
      userId: userId,
      comment: comment
    };
    let res = await fetch(`${origin}/comment/add/${bookId}`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(keyWord),
    });
    json = await res.json();
  } catch (error) {
    console.error("failed to add comment API:", error);
    return;
  }
  if (json.error) {
    console.error(" add comment response error:", json.error);
    return;
  }
  try {
    console.log("add comment successfully");
    dispatch(isSearching(true))
    return;
  } catch (error) {
    console.error("failed to add comment action", error);
  }
}


  useEffect(() => {
    loadSingleBookInfo(bookId);
    loadChapterGroup(bookId);
    if (userId) {
      checkCollection(userId, bookId);
      dispatch(addBookViewThunk(userId, bookId));
    }
    loadComment(bookId);
  }, [bookId]);

  

  useEffect(() => {
    loadComment(bookId);
    dispatch(adminQuitManaging(false))
  }, [adminManaging]);

  useEffect(() => {
    dispatch(isSearching(false))
    loadSingleBookInfo(bookId);
    loadChapterGroup(bookId);
    if (userId) {
      checkCollection(userId, bookId);
      dispatch(addBookViewThunk(userId, bookId));
    }
    loadComment(bookId);
  }, [searched]);

  useEffect(() => {
    if (chapterList) {
      dispatch(setTotalChapter(chapterList.length));
    }
  }, [chapterList]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton mode="md"></IonBackButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent color="secondary" className={styles.bookPage + "de"}>
        {/* <IonLoading isOpen={!book} /> */}

        {/* <BookInfo /> */}
        <div className={bookInfoStyles.bookInfoBox} color="secondary">
          {
            singleBook?
            <img alt={`《${singleBook?.title}》的封面`} src={toImageUrl(singleBook?.cover_image)} className={bookInfoStyles.bookInfoBook}></img>
            :
            <div></div>
          }
          <div className={bookInfoStyles.bookInfoContentBox}>
            {singleBook ? (
              <div>
                <div className={bookInfoStyles.bookInfoBookInfoBox}>
                  <div className={bookInfoStyles.bookInfoBookTitle}>
                    <span className={bookInfoStyles.bookInfoBookTitleUnderline}>
                      {singleBook.title}
                    </span>
                    {singleBook.is_end ? (
                      <span
                        className={bookInfoStyles.bookInfoBookStateFinished}
                      >
                        已完結
                      </span>
                    ) : (
                      <span className={bookInfoStyles.bookInfoBookState}>
                        連載中
                      </span>
                    )}
                  </div>
                  <div className={bookInfoStyles.bookInfoBookAuthor}>
                    {singleBook?.author}
                  </div>
                </div>

                <div className={bookInfoStyles.bookInfoUpdateInfoBox}>
                  <div className={bookInfoStyles.bookInfoBookLastUpdate}>
                    更新至第{singleBook.chapter}章
                  </div>

                  <div className={bookInfoStyles.bookInfoUpdateDate}>
                    最近更新:{" "}
                    {format(new Date(singleBook.latest_update), "yyyy-MM-dd")}
                  </div>
                </div>
              </div>
            ) : null}

            {userId ? (
              <div className={bookInfoStyles.addCollectionBox}>
                {isCollect ? (
                  <IonButton
                    size="small"
                    color="danger"
                    className={bookInfoStyles.addCollection}
                  >
                    {" "}
                    已收藏{" "}
                  </IonButton>
                ) : (
                  <IonButton
                    size="small"
                    color="success"
                    className={bookInfoStyles.addCollection}
                    onClick={addCollection}
                  >
                    {" "}
                    加入收藏{" "}
                  </IonButton>
                )}
              </div>
            ) : null}
          </div>
        </div>

        {/* <BookIntro /> */}
        <div className={bookIntroStyles.bookIntroBox}>
          <div className={bookIntroStyles.bookIntroWord}>內容簡介:</div>
          <div className={bookIntroStyles.bookIntoContent}>
            {singleBook?.abstract}
          </div>
        </div>
        {/* <BookChapter /> */}
        <div className={chapterStyles.bookChapterArea}>
          <div className={chapterStyles.bookChapterBox}>
            {chapterList ? (
              chapterList.map((chapter, index) =>
                index < 1 ? (
                  <BookChapterLatestUpdateElement
                    key={chapter.chapter_id}
                    //bookId={bookId}
                    chapterInfo={chapter}
                  />
                ) : (
                  <BookChapterElement
                    key={chapter.chapter_id}
                    //bookId={bookId}
                    chapterInfo={chapter}
                  />
                )
              )
            ) : (
              <></>
            )}
          </div>
        </div>
        {/* <BookComment /> */}

        <div className={bookCommentStyles.commentArea}>
          <div className={bookCommentStyles.commentWord}>評論區</div>
          {/* <BookCommentData /> */}
          {commentList ? (
            commentList.map((comment) => (
              <BookComment key={comment.comment_id} commentList={comment} />
            ))
          ) : (
            <></>
          )}
          <div className={commentDataStyles.commentBox}>
             <div>
             <IonInput value={text} placeholder="輸入評論" onIonChange={e => setText(e.detail.value!)} color='medium'></IonInput>
             <div className={commentDataStyles.btnBox}>
              {userId && text? 
              <IonButton color="primary" size="small" onClick={()=>{newComment(userId, text, bookId)}}> 新增</IonButton>
              : <div className={bookCommentStyles.commentWord}>請登錄/輸入留言</div>
              }
             
             </div> 
     
         
             </div>
            </div>
        </div>

        <Space />
      </IonContent>
    </IonPage>
  );
};

export default BookPage;
