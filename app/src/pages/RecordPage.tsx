import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from "@ionic/react";
import { useDispatch, useSelector } from "react-redux";
import BookshelfBook from "../components/BookshelfBook";
import Space from "../components/Space";
import { RootState } from "../redux/state";
import styles from "./scss/Record.module.scss";
import { Redirect } from 'react-router-dom';
import { useEffect, useState } from "react";
import { getBookViewRecThunk } from "../redux/book-content/thunk";
import { BookSummaryInfo } from "../model/book-content";
import InfiniteScroll from "react-infinite-scroll-component";


const Record = () => {

  // const bookViewRec = useSelector((state: RootState) => state.bookContent.bookViewRec);
  // const dispatch = useDispatch();
  const isReading = useSelector((state: RootState) => state.bookContent.isReading);
  const userId = useSelector((state: RootState) => state.auth.user?.id);
  const isBookRec = useSelector((state:RootState)=>state.bookContent.isBookRec);
  const [bookViewRec, setBookViewRec] = useState<BookSummaryInfo[] | undefined>(undefined);
  const [nextId, setNextId] = useState<number>(0);
  const offsetNextId = 8;
  


  async function getBookViewRec(userId: number, offsetNextId: number, nextId: number) {

    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/book_content/book_view/${userId}/${offsetNextId}/${nextId}`, {
        method: 'GET'
      })
      json = await res.json();
    } catch (error) {
      console.error('failed to call get book view record API:', error);
      return
    }

    if (json.error) {
      console.error('get book view record response error:', json.error);
      return
    }

    let bookView: BookSummaryInfo[] = json.bookViewRec;
    console.log("get book view record successfully");
    if (bookView.length > 0) {
      setBookViewRec(bookViewRec ? bookViewRec.concat(bookView) : [...bookView]);
      setNextId(nextId + offsetNextId);
    }
    return

  }

  useEffect(()=>{
    setNextId(0);
    setBookViewRec(undefined);
  },[userId]);
  
  
  
  useEffect(()=>{
    setNextId(0);
    setBookViewRec(undefined);
  }, [isReading]);


  useEffect(() => {
    if (userId) {
      getBookViewRec(userId, offsetNextId, nextId);
    }
  }, [isBookRec]);





  function fetchMoreBooks() {
    console.log("i am in fetch more books function");
    if (userId) {
      getBookViewRec(userId, offsetNextId, nextId);
    }
  };


  
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary"  >
          <IonTitle className={styles.appTitle}>閱讀紀錄</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent color="secondary" className={styles.recordPageContent} >
        <InfiniteScroll
          dataLength={bookViewRec?.length as number || offsetNextId}
          next={fetchMoreBooks}
          hasMore={true}
          loader={<h4>Loading...</h4>}
          height={'91vh'}
        >
          <div className={styles.area}>
            {
              userId ?
                <div>
                  {
                    bookViewRec ?
                      bookViewRec.map(bookView => (
                        <BookshelfBook key={bookView.book_id} bookSummaryInfo={bookView} />
                      ))
                      :
                      <div>沒有閱讀紀錄</div>
                  }
                </div>
                :
                <Redirect to='/login' />
            }
          </div>
          <Space />
        </InfiniteScroll>
      </IonContent>
    </IonPage>
  );
};

export default Record;
