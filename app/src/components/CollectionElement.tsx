import sampleBook from "../components/img/book.jpg";
import { format } from "date-fns";
import continueStyles from "../components/scss/BookUpdateDate.module.scss";
import elementStyles from "../components/scss/CollectedBook.module.scss";
import { CollectionList, SearchByCatList } from "../model/book-content";
import { useIonRouter } from "@ionic/react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { useEffect, useState } from "react";
import { isCollected } from "../redux/book-content/action";
import { toImageUrl } from "../helpers/api";

type Props = {
  resultInfo: CollectionList;
};

const CollectionElement = (props: Props) => {
  const dispatch = useDispatch();
  const userId = useSelector((state: RootState) => state.auth.user?.id);
  const router = useIonRouter();
  const collected = useSelector(
    (state: RootState) => state.bookContent.isCollected
  );
  const [collectionList, setCollectionList] = useState<
    CollectionList[] | undefined
  >(undefined);

  async function getCollectionList(userId: number) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      return;
    }
    let json: any;
    try {
      let res = await fetch(`${origin}/collection/list/${userId}`, {
        method: "GET",
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call load collection list API:", error);
      return;
    }
    if (json.error) {
      console.error("load collection list response error:", json.error);
      return;
    }
    let collectionListResult: CollectionList[] = json.collectedBookList;
    setCollectionList(collectionListResult);
    console.log("load collection list successfully");
    console.log(collectionListResult);
  }

  async function removeCollection(userId: number, bookId: number) {
    let origin;

    try {
      let { REACT_APP_API_SERVER } = process.env;
      if (!REACT_APP_API_SERVER) {
        console.error("missing REACT_APP_API_SERVER in env");
        throw new Error("missing REACT_APP_API_SERVER in env");
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      getCollectionList(userId);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/collection/remove/${userId}/${bookId}`, {
        method: "DELETE",
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call remove collection API:", error);
      return;
    }
    if (json.error) {
      console.error("remove collection response error:", json.error);
      return;
    }
    console.log("status:", json.status);
    try {
      if (json.status === 200) {
        console.log("remove collection successfully");
        return;
      }
    } catch (error) {
      console.error("failed to dispatch remove collection action", error);
      return;
    }
    await getCollectionList(userId);
    dispatch(isCollected(true));
  }

  useEffect(() => {
    if (userId) {
      console.log("i am in useEffect get collection function");
      getCollectionList(userId);
      dispatch(isCollected(false));
    }
  }, [collected]);

  return (
    <>
      {userId ? (
        <div className={elementStyles.bookshelfBox} color="secondary">
          <img alt={`《${props.resultInfo.title}》的封面`} src={toImageUrl(props.resultInfo.cover_image)}  className={elementStyles.bookshelfBook}></img>
          <div className={elementStyles.bookshelfContentBox}>
            <div className={elementStyles.bookshelfBookInfoBox}>
              <div className={elementStyles.bookshelfBookTitle}>
                <span className={elementStyles.bookshelfBookTitleUnderline}>
                  {props.resultInfo.title}
                </span>

                {props.resultInfo.is_end ? (
                  <span className={elementStyles.bookshelfBookStateFinished}>
                    已完結{" "}
                  </span>
                ) : (
                  <span className={elementStyles.bookshelfBookState}>
                    連載中{" "}
                  </span>
                )}
              </div>
              <div className={elementStyles.bookshelfBookAuthor}>
                {props.resultInfo.author}
              </div>
            </div>
            <div className={elementStyles.updateInfoBox}>
              <div className={elementStyles.bookshelfBookLastUpdate}>
                更新至第{props.resultInfo.chapter}章
              </div>
              <div className={elementStyles.updateDate}>
                最近更新:{" "}
                {format(new Date(props.resultInfo.latest_update), "yyyy-MM-dd")}
              </div>
            </div>
            <div className={elementStyles.buttonBar}>
              <div
                color="medium"
                className={elementStyles.removeCollection}
                onClick={() =>
                  removeCollection(userId, props.resultInfo.book_id)
                }
              >
                {" "}
                移除收藏{" "}
              </div>
              <div className={continueStyles.userBookRecordBox}>
                <div className={continueStyles.userBookRecord} onClick={() => router.push("/book/" + props.resultInfo.book_id)}>
                  繼續閱讀
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <></>
      )}
    </>
  );
};

export default CollectionElement;
