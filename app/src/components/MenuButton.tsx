import React, { useState } from "react";
import {
  IonButton,
  IonContent,
  IonIcon,
  IonItem,
  IonList,
  IonListHeader,
  IonPage,
  IonPopover,
  useIonPopover,
} from "@ionic/react";
import { menu } from "ionicons/icons";
import styles from "./scss/NavBar.module.scss";

import { useDispatch, useSelector } from "react-redux";

import { RootState } from "../redux/state";
import { changeFontSize } from "../redux/theme/action";

import { ThemeName } from "../redux/theme/state";


const MenuButton: React.FC = () => {

  const [popoverState, setShowPopover] = useState({
    showPopover: false,
    event: undefined,
  });

  const dispatch = useDispatch();

  const setFontSize = (font: ThemeName) => {
    const action = changeFontSize(font);
    dispatch(action);
  };

  return (
    <div className={styles.menuBtn}>
      <IonPopover
        cssClass="my-custom-class"
        event={popoverState.event}
        isOpen={popoverState.showPopover}
        animated={false}
        onDidDismiss={() =>
          setShowPopover({ showPopover: false, event: undefined })
        }
      >
        <IonList>
          <IonItem
            button
            className={styles.themeOption}
            onClick={() => setFontSize("large")}
          >
            字體 大
          </IonItem>
          <IonItem
            button
            className={styles.themeOption}
            onClick={() => setFontSize("middle")}
          >
            字體 中
          </IonItem>
          
          <IonItem
            button
            className={styles.themeOption}
            onClick={() => setFontSize("small")}
          >
            字體 小
          </IonItem>
        </IonList>
      </IonPopover>
      <IonButton
        onClick={(e: any) => {
          e.persist();
          setShowPopover({ showPopover: true, event: e });
        }}
      >
        <IonIcon
          icon={menu}
          className={styles.iconSize}
        ></IonIcon>
      </IonButton>
    </div>
  );
};

export default MenuButton;
