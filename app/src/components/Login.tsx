
import loginPageLogo from "./img/skywalk_login.jpg";
import facebookLogo from "./img/facebook-logo.png";
import googleLogo from "./img/google-logo.png";
import styles from './scss/Login.module.scss'
import { Link, useHistory } from "react-router-dom";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginWithPasswordThunk, loginWithFaceBookThunk } from "../redux/auth/thunk";
import { RootState } from "../redux/state";
import { push } from "connected-react-router";
import ReactFacebookLogin, {ReactFacebookLoginInfo, ReactFacebookFailureResponse} from "react-facebook-login";
import { loginFailed } from "../redux/auth/action";
import { IonHeader, IonPage, IonTitle, IonToolbar } from "@ionic/react";

const Login = () => {

    const user = useSelector((state: RootState) => state.auth.user);
    const error = useSelector((state: RootState)=> state.auth.error);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const dispatch = useDispatch();
    async function loginWithPassword() {
        console.log("dispatch function:",email, password);
        dispatch(loginWithPasswordThunk(email, password));
        setEmail('');
        setPassword('');
    }
    const history = useHistory();
    useEffect(() => {
        if (user) {
            console.log("redirect to home page");
            // dispatch(push('/'));
            history.push("/");
              
        }
    }, [user]);


    const facebookCallback = (info:ReactFacebookLoginInfo | ReactFacebookFailureResponse ) => {
        console.log('Facebook user info');
        if('accessToken' in info) {
            console.log('facebook user info:', info);
            dispatch(loginWithFaceBookThunk(info.accessToken));
        }else {
            console.log('failed to login with facebook:', info);
            dispatch(loginFailed('Failed to login with Facebook'));
        }
    }

    let { REACT_APP_FACEBOOK_APP_ID } = process.env;
    if (!REACT_APP_FACEBOOK_APP_ID) {
        return <code>
            missing REACT_APP_FACEBOOK_APP_ID in env!
        </code>
    }


    return (
        <>
        <IonHeader>
          <IonToolbar color="primary"  >
              <IonTitle className={styles.appTitle}>登入</IonTitle>
          </IonToolbar>
        </IonHeader>
        <div >
            <div className={styles.loginArea}>
                <img src={loginPageLogo} className={styles.loginPageIcon}></img>
                <div className={styles.emailInput}>
                    <label htmlFor='email' className={styles.label}>電郵</label>
                    <input type="text" className={styles.email} name="email" value={email} onChange={e => setEmail(e.target.value)} required />
                </div>
                <div className={styles.passwordInput}>
                    <label htmlFor='password' className={styles.label}>密碼</label>
                    <input type="password" className={styles.password} name="password" value={password} onChange={e => setPassword(e.target.value)} required />
                    <div>
                        {error?
                        '輸入電郵或密碼錯誤':
                        null}
                    </div>
                </div>
                <div className={styles.confirm}>
                    <div className={styles.confirmContainer}>
                        <button className={styles.confirmButton} onClick={() => loginWithPassword()}>登入</button>
                    </div>
                </div>
                <div className={styles.facebook}>
                <ReactFacebookLogin 
                    appId =  {REACT_APP_FACEBOOK_APP_ID}
                    autoLoad={false}
                    cssClass={styles.facebookButton}
                    fields="name, email"
                    callback={info=>facebookCallback(info)}
                    

                /></div>

                {/* <div className={styles.facebook}>
                    <button className={styles.facebookButton}>
                        <img src={facebookLogo} className={styles.facebookLogo}></img>
                        Facebook 登入
                    </button>
                </div> */}
                <div className={styles.google}>
                    <button className={styles.googleButton}>
                        Login with Google
                    </button>
                </div>
                <Link to="./register">
                    <div className={styles.register}>
                        <button className={styles.registerButton}>
                            未有帳號，註冊
                        </button>
                    </div>
                </Link>
            </div>
        </div>
 </>
    )

}

export default Login;