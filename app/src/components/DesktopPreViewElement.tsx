
import styles from "./scss/DesktopPreViewElement.module.scss";
import sampleBook from "./img/book.jpg";
import { IonButton, IonItem, IonLabel, IonToggle} from '@ionic/react';
import ReactQuill from "react-quill"
import "../../node_modules/react-quill/dist/quill.snow.css"
import { useState } from "react";
import App from "../App";
import { Link } from "react-router-dom";
import Space from "./Space";



const DesktopPreViewElement = () => {
  // const [body, setBody] = useState("");
  return (
    <>
    <div className={styles.chapterPreViewArea}>
      <div className={styles.btnBar}>
        
         <IonButton color="success" className={styles.previewBtn} size="small">上傳小說</IonButton>
        
         <Link to="/desktop_edit_chapter">
         <IonButton color="primary" className={styles.previewBtn} size="small">返回編輯</IonButton>
         </Link>
      </div>
      <div className={styles.chapterPreViewBox}>
      書籍描述：
      虛幻與現實、遊戲與真相；世界樹的枯萎、哥布林的陰謀、血族的冷眼旁觀；在一層又一層的紗網之中，最後的結果，終由誰來背負？「這局，沒人能逃。」

      ０１－－望塵方online
      看著櫥窗裡各種不同顏色的導入水晶，其中一款的天藍色菱形水晶散發著耀眼的光芒，奪去她的目光。不禁想起最近新出的全球第一款虛擬實境化遊戲——「望塵方online 」。

      望塵方online，在還沒虛擬實境化時，也是一款經營高達七年以上的鍵盤式網遊，人氣源源不絕，卻在毫無預警的情況下結束營業。

      這消息令聚多網民引起騷動，發出各種訊息，要求營辦商給個合理的回覆。解釋在一夕之間，主程式像是被鎖了，怎麼也開不到的原因。不過，營運商對此卻只有一個回覆：敬請期待。

      這回覆不但沒有回答到網民們的疑問，無疑更是火上加油，讓網民的情緒更加激動。

      這遊戲至少也有七年多的營運，有新手有老手。新手可以新得連抓寵物都不懂抓，有說明看還會問的菜鳥；老手可以老到連地圖，怪物的資料全都背了出來。

      新手和老手的分別實在大啊。畢竟，一個是玩了七年多，一個是玩了一年都沒有，叫他們怎麼比呢？

      幸好，再過了三年。

      望塵方online 重出江湖，再度成為網上熱潮，不、不只是網上，連現實也已經被它所統領，小至學生，大至老人也知道這款遊戲。

      學校、公司、街道，全都聽到關於望塵方online的事。

      為什麼？

      因為它是全球第一款虛擬實境化的遊戲啊！

      由十幾隊精英科學家合力研究而出，據說是要把腦神經和網路連在一起就可以完成虛擬實境化的一項創舉，不過這也只是據說，實際上是不是這樣就無從得知。

      總之 ，全息遊戲，是真真正正的存在這個世界上。


      ※※※

      陸遙雲是一個在蛋糕店裏兼職的大學生，差不多已經兼職了三年多的時間。老闆娘平日對她很好，有些買剩的糕餅點心也會包成一盒，讓她拿回與朋友合租的房子，跟她們一起嘗嘗老闆娘的手藝。

      蛋糕店的位置不太偏遠，卻也不是一眼就能找到的地方，但是客人不會因為這樣的原因而減少。

      雖然來的客人大多都是上了年紀的老人，但生意卻沒有一天是沒有人的。

      遙雲自自然然的感到奇怪，這家店舖雖然不算偏僻，但這是怎麼維持客源呢？

      「呵呵，維持客源嗎？這也不是我可以控制的，可能是我年輕時做過很多好事才會有客人吧？」看下去年紀有四十多歲的老闆娘，只是笑着回應了我的疑問。

      接著，她又說：「每一件事都有它的原由，就像是阿遙妳一樣，在這裏兼職也有妳的原因。」

      在這裏兼職也有我的原因？原因就是要賺錢啊！雖然遙雲的工作只是幫忙掃地、包裝蛋糕等等雜碎事情，但薪水非常樂觀，所以她才會在這兼職啊！

      蛋糕店裏有兩個員工，除了她以外，還有一個叫做明傑的男生。

      他是高中的學生，因為家境貧窮，被逼出外兼職尋找家計，一路上要顧著學業，又要顧著兼職，這年頭高中生真不好當啊。

      經過三年時間的相處，已經讓遙雲瞭解到員工之間的互動、老闆的關懷都是這麼難得的可貴。

      只可惜，一間規模小小的蛋糕店又怎能逃過社會的變遷？


      社會科技發達，許多老字號鋪兒也一一倒閉，遙雲所在的蛋糕店便是其中一個。


      蛋糕店停業了，即使遙雲和明傑是多麼的不舍，仍是改變不了倒閉的命運。

      不過老闆娘卻是沒怎樣感到傷心，只是說了：「倒閉是遲早的事，只不過我又變回個無所事事的阿姨。來吧，阿遙、阿傑，我請你們吃飯！」雖然老闆娘的語氣是這麼的輕鬆，這麼的不在乎，但眼中的不捨，卻是每個人都能看見。

      他們來到一間酒樓吃飯，酒樓裏的人潮源源不斷，不過遙雲等人沒等多久便能入座。是說這家酒樓效率高，還是客人吃得快呢？不管是怎麼樣，有位子坐就行了。

      遙雲只點了幾個點心，對於酒樓，她個人倒是不太喜歡，不是食物不合口味，而是酒樓那種熱熱鬧鬧的氣氛實在讓她渾身不自在。

      每個人都說上一句話，就算是很小很小的音量，只要有很多人聚在一起說就會形成超吵的噪音，遙雲所在的酒樓便是這樣的地方。

      遙雲現在只想盡快吃完離開這個地方，不是想辜負老闆娘的好意，她現在真的很不舒服。

      不過明傑則是點了許多餐點，一會兒叫飯，一會兒叫粉，看來是要把老闆娘帶來的現金都給花得清光，遙雲可是沒有一段的時間也走不了。

      老闆娘倒是很樂意明傑盡情點菜，一面笑嘻嘻的替兩人倒茶，一點也沒有錢要被花光的心痛表情。

      「阿遙、阿傑，蛋糕店倒閉了，這可能是我跟你們最後一次見面呢。」老闆娘仍是笑著說。

      遙雲跟明傑聽到老闆娘的這一番話，都停下了進食的動作，望向老闆娘。

      最後一次見面……這麼凝重的話，遙雲重來沒有想過，更何況是蛋糕店倒閉？

      任何人都不想說再見，老闆娘自然是最不舍的一個。她看著蛋糕店從一間默默無名的小店，慢慢的走上軌道，變成一間穩定的店舖。但如今，社會的變遷卻造成大聚的遺憾。


      無奈的是，每個人都無法改變這個事實。

      就像是人一樣，到了最後，也只是死路一條，看的只是時間的早與遲。

      正因為是人，我們才會死，才能得到救贖。


      這是一早就已經決定的事，任誰也無法改變。


      然而，蛋糕店倒閉，代表著什麼？又是個什麼故事的開端？

      這一切都是這麼變幻莫測，一步一步的將她推往一個未知的世界。

      那個喜歡捉弄人的命運，又會讓她經歷一場怎麼樣冒險？



      只願平凡，只想停留在這處。

      「不想再失去了。」

      遙雲的腦海中突然浮現出這樣的一句話，這話的語氣她非常熟悉，那是一把平和恬淡的聲音、那是一把有著後悔和眷戀的聲音。

      這聲音她太熟悉了，比每天都能見上幾面的老闆娘還熟。

      不過，為什麼她就是想不起，就像是被人刻意隱瞞的那樣，不去尋找就一直都想不起。


      是啊，命運又會帶她去到那裡，見到這把聱音的主人……


      到最後，結局還是一樣……


      一直一直……

      遙雲的腦海裡一直迴蕩著那句話，那句她非常熟悉的話語。


      「遙姐，妳在發什麼呆？連老闆娘在說話妳也可以走神？」明傑用手肘推了推遙雲道。

      遙雲連忙遙了遙頭，不再想其他事情，向老闆娘道歉。

      「那倒是不用道歉，只是剛才我跟你們說的話，妳都沒聽見吧？不要緊，我來說多一遍。」老闆娘一手夾了個春卷上來，一放入口那刻，香脆的外皮「喀、喀」的響著，怕是別人不知道有多好吃。

      遙雲看著老闆娘吃得滋味，也伸手夾了塊排骨，放進口裡時，肉汁立刻充滿整個口腔，麻辣的味道刺激著她的味覺。


      真好吃！這是她的第一反應。

      雖然遙雲是不喜歡酒樓這種氣氛沸騰的地方，不過對於美味的食物，她還是樂於享受。

      「阿遙、阿傑，你們也只是十幾二十歲，蛋糕店倒閉你們還可以找另一份工作。不過，這些年來，蛋糕店賺的錢也足夠我去逛逛，所以我想到世界各地旅遊。我說的最後一次見面，大概就是這個意思。」

      「電視還不是說什麼全世界第一款全息遊戲，你們年青人最適合這些東西吧。作為老闆娘給你們的最後一份薪水，你們大大方方的收下吧！」

      老闆娘拿出兩塊不同顏色的導入水晶，一塊是綠得發青；一塊是像天空一樣蔚藍的水晶。兩個水晶都閃閃發亮，映在遙雲和明傑的眼中。

      這不就是……全球第一款虛擬實境化的遊戲－－望塵方ｏｎｌｉｎｅ？


      十年前，她十二歲，身為一個小宅女，即使是國中生的年紀，也有玩過這款遊戲。

      這遊戲陪伴著她一年又一年，很快的，七年過去了，這款遊戲卻在毫無預警的情況下結束營運。

      在遊戲裡結識的同伴，也無法再一起繼續遊玩。

      有些人就要求營運商回覆他們的訴求、有些人則是轉到其他遊戲。

      就算在網絡上還有聯絡，感情也不如當初的真誠。


      這一切就像是浮雲，消失得無影無縱。


      遙雲愣愣的看著閃得發亮的水晶，整個人都差點被光亮迷住了。


      「老……老闆娘……這是送給我們的嗎……？」明傑瞪著眼注視著那兩塊閃閃發光的水晶。

      這……這是我半年的薪水也無法買到的東西啊……老闆娘這次出手這麼闊綽啊……明傑在心裡嘀咕著。

      「不，不，這不是送……」老闆娘話音未落，便被遙雲先搶一步。

      「什麼！這不是送？」遙雲激動得起身拍桌。

      一時間，在酒樓裡進食的客人和侍應也紛紛往這邊看過來。

      遙雲見狀，紅著臉坐下來，問著老闆娘：「那……那不是送的嗎？」


      「哈哈，從一開始我就沒說是送啊。這可是你們的薪水，除了這個，還有比平常多一倍的錢，作為你們為蛋糕店工作了三年的回報吧！」
      
      
      </div>
    </div>
    </>
  );
};
export default DesktopPreViewElement;
