import { IonButton, IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from "@ionic/react";
import { useEffect, useState } from "react";
import { UserList, WriterList } from '../model/book-content';
import elementStyles from "./scss/AdminAccountManageElement.module.scss"
import { format } from 'date-fns';
import { adminIsManaging, isChangeToWriter } from "../redux/book-content/action";
import { useDispatch } from "react-redux";

type Props = {
    resultInfo: UserList;
  };

const AdminAccountManageElement = (props: Props) => {
  const dispatch = useDispatch();
    const [userList, setUserList] = useState<UserList[] | undefined>(undefined);
    const [writerList, setWriterList] = useState<WriterList[] | undefined>(undefined);

    async function getWriterList() {
        let origin;
        try {
          let { REACT_APP_API_SERVER } = process.env
          if (!REACT_APP_API_SERVER) {
            console.error('missing REACT_APP_API_SERVER in env');
            throw new Error('missing REACT_APP_API_SERVER in env');
          }
          origin = REACT_APP_API_SERVER;
        } catch (error) {
          return;
        }
        let json: any;
        try {
          let res = await fetch(`${origin}/admin/writer_list`, {
            method: 'GET'
          })
          json = await res.json();
        } catch (error) {
          console.error('failed to call load latest update list API:', error);
          return
        }
        if (json.error) {
          console.error('load latest update list response error:', json.error);
          return
        }
        let writerListResult: WriterList[] = json.writerList;
        setWriterList(writerListResult);
        try {
        console.log("load latest update list successfully");
        console.log(writerListResult);
        return
      } catch (error) {
        console.error('failed to dispatch load writer chapter record action', error);
      }}
    
    
      async function getUserList() {
        let origin;
        try {
          let { REACT_APP_API_SERVER } = process.env
          if (!REACT_APP_API_SERVER) {
            console.error('missing REACT_APP_API_SERVER in env');
            throw new Error('missing REACT_APP_API_SERVER in env');
          }
          origin = REACT_APP_API_SERVER;
        } catch (error) {
          return;
        }
        let json: any;
        try {
          let res = await fetch(`${origin}/admin/user_list`, {
            method: 'GET'
          })
          json = await res.json();
        } catch (error) {
          console.error('failed to call load latest update list API:', error);
          return
        }
        if (json.error) {
          console.error('load latest update list response error:', json.error);
          return
        }
        let userListResult: UserList[] = json.userList;
        setUserList(userListResult);
        try {
          console.log("load latest update list successfully");
          console.log(userListResult);
          return
        } catch (error) {
          console.error('failed to dispatch load update list action', error);
        }
      }
    
      async function changeToWriterRole(userId: number) {
        let origin;
        try {
          let { REACT_APP_API_SERVER } = process.env
          if (!REACT_APP_API_SERVER) {
            console.error('missing REACT_APP_API_SERVER in env');
            throw new Error('missing REACT_APP_API_SERVER in env');
          }
          origin = REACT_APP_API_SERVER;
        } catch (error) {
          return;
        }
        let json: any;
        try {
          let res = await fetch(`${origin}/admin/writer_list/add_writer/${userId}`, {
            method: 'POST'
          })
          json = await res.json();
          dispatch(isChangeToWriter(true));
        } catch (error) {
          console.error('failed to call load latest update list API:', error);
          return
        }
        if (json.error) {
          console.error('load latest update list response error:', json.error);
          return
        }
        let userListResult: UserList[] = json.userList;
        setUserList(userListResult);
        console.log("load latest update list successfully");
        console.log(userListResult);
      }
    
      async function deleteUserAccount(userId: number) {
        let origin;
        try {
          let { REACT_APP_API_SERVER } = process.env
          if (!REACT_APP_API_SERVER) {
            console.error('missing REACT_APP_API_SERVER in env');
            throw new Error('missing REACT_APP_API_SERVER in env');
          }
          origin = REACT_APP_API_SERVER;
        } catch (error) {
          return;
        }
        let json: any;
        try {
          let res = await fetch(`${origin}/admin/user_list/remove/${userId}`, {
            method: 'DELETE'
          })
          console.log('success call del user API');
          dispatch(adminIsManaging(true));
          json = await res.json();
        } catch (error) {
          console.error('failed to call load latest update list API:', error);
          return
        }
        if (json.error) {
          console.error('del user response error:', json.error);
          return
        }
        let userListResult: UserList[] = json.userList;
        setUserList(userListResult);
        console.log("del user successfully");
        console.log(userListResult);
   
      }


      
  return (
            <div className={elementStyles.adminAccountManageBox}>
              <div className={elementStyles.userID}>{props.resultInfo.id}</div>
              <div className={elementStyles.username}>{props.resultInfo.name}</div>
              <div className={elementStyles.userEmail}>{props.resultInfo.email}</div>
              <div className={elementStyles.date}>{format(new Date(props.resultInfo.created_at), 'yyyy-MM-dd ')}<br /><span>{format(new Date(props.resultInfo.created_at), 'p')}</span></div>
              <IonButton color="success" className={elementStyles.previewBtn} size="small" onClick={() => changeToWriterRole(props.resultInfo.id)}>確定</IonButton>
              <IonButton color="danger" className={elementStyles.previewBtn} size="small" onClick={() => deleteUserAccount(props.resultInfo.id)}>刪除戶口</IonButton>
            </div>
  )};

export default AdminAccountManageElement;
