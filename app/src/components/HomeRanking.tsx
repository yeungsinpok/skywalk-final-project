import styles from "./scss/HomeRanking.module.scss";
import RankingBook from "./RankingBook";
import { useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { useEffect, useState } from "react";
//import { BookSummaryInfo } from "../model/book-content";
import { BookSummaryInfo } from "../redux/book-content/state";


const HomeRanking = () => {

  // const rankList = useSelector((state: RootState) => state.bookContent.rankList);

  const [rankList, setRankList] = useState<BookSummaryInfo[] | undefined>(undefined);

  async function loadRank() {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/book_content/rank`, {
        method: 'GET'
      })
      json = await res.json();
    } catch (error) {
      console.error('failed to call load book rank API:', error);
      return
    }

    if (json.error) {
      console.error('load book rank response error:', json.error);
      return
    }

    let rankList: BookSummaryInfo[] = json.rankList;

    try {
      console.log("load book rank successfully");
      setRankList(rankList);
      return

    } catch (error) {
      console.error('failed to dispatch load rank action', error);
    }
  }

  useEffect(() => {
    console.log('Home page useEffect');
    loadRank()
  }, []);

  return (
    <div className={styles.homeRankingBooks} color="secondary">
      {
        rankList ?
          rankList.map((rank, index) => (
            <RankingBook
              key={rank.book_id}
              rankListInfo={rank}
              rank={index} />
          ))
          :
          <><div>empty list</div></>
      }
    </div>
  );
};

export default HomeRanking;
