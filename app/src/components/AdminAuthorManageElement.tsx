import styles from "./scss/AdminAuthorManage.module.scss";
import { IonButton, useIonRouter } from "@ionic/react";
import { useEffect, useState } from "react";
import { UserList, WriterList } from '../model/book-content';
import { format } from 'date-fns';

import elementStyles from "./scss/AdminAuthorManageElement.module.scss";
import { adminIsManaging, isChangeToUser } from "../redux/book-content/action";
import { useDispatch } from "react-redux";


type Props = {
    resultInfo: WriterList;
  };

const AdminAuthorManageElement = (props: Props) => {
  const dispatch = useDispatch();
  const [writerList, setWriterList] = useState<WriterList[] | undefined>(undefined);
  const [userList, setUserList] = useState<UserList[] | undefined>(undefined);
  const router = useIonRouter();
  
  async function getWriterList() {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      return;
    }
    let json: any;
    try {
      let res = await fetch(`${origin}/admin/writer_list`, {
        method: 'GET'
      })
      json = await res.json();
    } catch (error) {
      console.error('failed to call load latest update list API:', error);
      return
    }
    if (json.error) {
      console.error('load latest update list response error:', json.error);

      return
    }
    let writerListResult: WriterList[] = json.writerList;
    setWriterList(writerListResult);
    try {
    console.log("load latest update list successfully");
    console.log(writerListResult);
    return
  } catch (error) {
    console.error('failed to dispatch load writer chapter record action', error);
  }}


  async function getUserList() {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      return;
    }
    let json: any;
    try {
      let res = await fetch(`${origin}/admin/user_list`, {
        method: 'GET'
      })
      json = await res.json();
    } catch (error) {
      console.error('failed to call load latest update list API:', error);
      return
    }
    if (json.error) {
      console.error('load latest update list response error:', json.error);
      return
    }
    let userListResult: UserList[] = json.userList;
    setUserList(userListResult);
    try {
      console.log("load latest update list successfully");
      console.log(userListResult);
      return
    } catch (error) {
      console.error('failed to dispatch load update list action', error);
    }
  }



  async function depriveWriterRole(writerId: number) {
    
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      return;
    }
    let json: any;
    try {
      let res = await fetch(`${origin}/admin/writer_list/disable_writer/${writerId}`, {
        method: 'POST'
      })
      dispatch(isChangeToUser(true))
      json = await res.json();
    } catch (error) {
      console.error('failed to call load latest update list API:', error);
      return
    }
    if (json.error) {
      console.error('load latest update list response error:', json.error)
      return
    }
    let writerListResult: WriterList[] = json.writerList;
    setWriterList(writerListResult);
    console.log("load latest update list successfully");
    console.log(writerListResult);
  }

function checkWriterBook() {
  router.push('/admin_manage_book/' + props.resultInfo.id,'none')
}


  return (
            <div className={elementStyles.adminAccountManageBox}>
              <div className={elementStyles.userID}>{props.resultInfo.id}</div>
              <div className={elementStyles.username}>{props.resultInfo.name}</div>
              <div className={elementStyles.userEmail}>{props.resultInfo.email}</div>
              <div className={elementStyles.date}>{format(new Date(props.resultInfo.created_at), 'yyyy-MM-dd ')}<br /><span>{format(new Date(props.resultInfo.created_at), 'p')}</span></div>
              <IonButton color="danger" className={styles.unBlockBtn} size="small" onClick={() => depriveWriterRole(props.resultInfo.id)}>解除</IonButton>
              <IonButton color="primary" className={styles.unBlockBtn} size="small"  onClick={checkWriterBook}>管理小說</IonButton>
            </div>
  );
};

export default AdminAuthorManageElement;
