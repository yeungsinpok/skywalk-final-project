
import styles from "./scss/DesktopBookElement.module.scss";
import sampleBook from "./img/book.jpg";
import { IonButton, IonItem, IonLabel, IonToggle} from '@ionic/react';


const DesktopBookElement = () => {
  
  return (
    <>
    <div className={styles.bookInfoBox}>
        <img src={sampleBook} className={styles.bookCover}></img>
        <div className={styles.bookInfo}>
        <div className={styles.bookContentTypeBox}>
          <div className={styles.bookContentType}>小說名稱:</div>
          <div className={styles.bookContentType}>筆名:</div>
          <div className={styles.bookContentType}>類別:</div>
          <div className={styles.bookContentType}>連載情況:</div>
        </div>
        <div className={styles.bookContent}>
          <div className={styles.title}>小說名xxxxxxx</div>
          <div className={styles.author}>不知心火</div>
          <div className={styles.category}>玄幻</div>
          <div className={styles.isEnd}>連載中</div>
        </div>
        </div>
     
    </div>

    </>
  );
};
export default DesktopBookElement;
