import styles from "./scss/DesktopViewBookElement.module.scss";
import sampleBook from "./img/book.jpg";
import { Link } from "react-router-dom";
import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonItem,
  useIonRouter,
} from "@ionic/react";
import { WriterBookInfo } from "../redux/book-content/state";
//import { WriterBookInfo } from "../model/book-content";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadWriterChapterThunk } from "../redux/book-content/thunk";
import { RootState } from "../redux/state";
import { push } from "connected-react-router";
import { setEdit, setWriterBookIdChapter } from "../redux/book-content/action";
import { toImageUrl } from "../helpers/api";


type Props = {
  bookInfo: WriterBookInfo;
};

const DesktopViewBookElement = (props: Props) => {
  const [chapter, setChapter] = useState("");
  const [chapterOk, setChapterOk] = useState(true);

  // const writerChapter = useSelector(
  //   (state: RootState) => state.bookContent.writerChapter
  // );

  const chapterRegex = /^\d+$/;

  const dispatch = useDispatch();


  const checkChapterInput = () => {

    if (!chapterRegex.test(chapter) || parseInt(chapter) > props.bookInfo.chapter || parseInt(chapter) < 1) {
      setChapterOk(false);
    } else {
      dispatch(setEdit(2));
      dispatch(setWriterBookIdChapter(props.bookInfo.book_id, parseInt(chapter)))
      router.push(`/desktop_create_chapter/${props.bookInfo.book_id}/${chapter}`);
      setChapterOk(true);
    }
  };

  // useEffect(() => {
  //   if (chapterOk) {
  //     dispatch(
  //       loadWriterChapterThunk(props.bookInfo.book_id, parseInt(chapter))
  //     );
  //   }
  // }, [chapterOk]);

  
  const router = useIonRouter();
  return (
    <>
      <div className={styles.elementBox}>
        <img alt={`《${props.bookInfo.title}》的封面`} src={toImageUrl(props.bookInfo.cover_image)} className={styles.bookCover}></img>

        <div className={styles.area}>
          <div className={styles.viewBookElement}>
            <div className={styles.viewBookBox}>
              <IonCard>
                <IonCardHeader>
                  <IonCardSubtitle color="tertiary">
                    <span >
                      更新至第:{" "}
                      <span >
                        {props.bookInfo.chapter}
                      </span>
                      章
                    </span>
                  </IonCardSubtitle>
                  <IonCardTitle color="primary">
                    {" "}
                    <span >{props.bookInfo.title}</span>
                  </IonCardTitle>
                </IonCardHeader>
                <IonCardContent>
                  筆名:{" "}
                  <span className={styles.author}>{props.bookInfo.author}</span>
                  <span className={styles.bookContentType}>
                    <br />
                    類別:{" "}
                    <span className={styles.category}>
                      {props.bookInfo.category.map((category) => (
                        <span key={category}>{category}&nbsp;</span>
                      ))}
                    </span>
                  </span>
                  <span className={styles.bookContentType}>
                    <br />
                    連載情況:{" "}
                    <span className={styles.isEnd}>
                      {props.bookInfo.is_end ? "已完結" : "連載中"}
                    </span>
                  </span>
                </IonCardContent>
              </IonCard>
            </div>
          </div>

          <div className={styles.box2}>
            <IonCard>
              <IonCardContent>
                <IonButton
                  color="success"
                  className={styles.uploadNewChapter}
                  onClick={() => {
                    router.push(
                      `/desktop_create_chapter/${props.bookInfo.book_id}/${props.bookInfo.chapter}`,
                      "none"
                    )
                    dispatch(setEdit(1));
                    dispatch(setWriterBookIdChapter(props.bookInfo.book_id, props.bookInfo.chapter))
                  }
                  }
                >
                  上載小說章節
                </IonButton>

                {/* <IonButton color="danger" className={styles.delBook}>
                刪除這本小說
              </IonButton> */}


                <br />
                <span className={styles.editChapter}>
                  章節 / 番外篇{" "}  </span>
                <input
                  type="text"
                  className={styles.chapterNumber}
                  value={chapter}
                  onChange={(e) => setChapter(e.target.value)}
                />
                <br />
                <span className={styles.reminder}>(只可以輸入數字)
                  {
                    chapterOk ?
                      null :
                      '沒有此章節'
                  }
                </span>

                <div className={styles.editChapterLabelBox}>
                  <IonButton color="primary" className={styles.editChapterLabel} onClick={checkChapterInput}>
                    編輯已上傳章節{" "}
                  </IonButton>
                </div>
              </IonCardContent>
            </IonCard>

          </div>
        </div>
      </div>
    </>
  );
};

export default DesktopViewBookElement;
