import styles from "./scss/BarIcon.module.scss";
import { NavLink } from "react-router-dom";

import { reload, cellular, book, person, home } from "ionicons/icons";
import { IonButtons, IonIcon, IonTabButton, useIonRouter } from "@ionic/react";
import logo from "../components/img/buttonLogo.jpg"
import { useState } from "react";
import { BookSummaryInfo } from "../model/book-content";
import { useDispatch } from "react-redux";
import { loadBookRec } from "../redux/book-content/action";


const BarIcon = () => {
  const router = useIonRouter();
  const dispatch = useDispatch();

  function clickHandler() {
    dispatch(loadBookRec());
  }

  return (
    <footer className={styles.bar}>
      <NavLink to="/record" activeClassName={styles.selected} onClick={()=>clickHandler()}>
        <div className={styles.barIconBox} >
          <div className={styles.barIcon}>
          <IonTabButton>
            <IonIcon icon={reload}  className={styles.iconSize}></IonIcon>
            </IonTabButton>
          </div>
          <div className={styles.barIconName}>閱讀紀錄</div>
        </div>
      </NavLink>

      <NavLink to="/ranking" activeClassName={styles.selected}>
        <div className={styles.barIconBox} >
          <div className={styles.barIcon}>
          <IonTabButton>
            <IonIcon icon={cellular} className={styles.iconSize}></IonIcon>
            </IonTabButton>
          </div>
          <div className={styles.barIconName}>人氣排行榜</div>
        </div>
      </NavLink>

      <NavLink to="/" activeClassName={styles.selected}>
        <div className={styles.barIconBoxHome}>
          <div className={styles.barIconHome}>
          <IonTabButton className={styles.homeLogo} >
          <img src={logo} className={styles.logo}></img>
            </IonTabButton>
  
          </div>
          
        </div>
      </NavLink>

      <NavLink to="/update" activeClassName={styles.selected}>
        <div className={styles.barIconBox} >
          <div className={styles.barIcon}>
          <IonTabButton>
            <IonIcon icon={book}  className={styles.iconSize} ></IonIcon>
            </IonTabButton>
          </div>
          <div className={styles.barIconName}>最近更新</div>
        </div>
      </NavLink>

      <NavLink to="/about_me" activeClassName={styles.selected}>
        <div className={styles.barIconBox} >
          <div className={styles.barIcon}>
          <IonTabButton>
            <IonIcon icon={person} className={styles.iconSize} ></IonIcon>
            </IonTabButton>
          </div>
          <div className={styles.barIconName}>我</div>
        </div>
      </NavLink>
    </footer>
  );
};
export default BarIcon;
