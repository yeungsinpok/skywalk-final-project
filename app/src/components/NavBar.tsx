import styles from "./scss/NavBar.module.scss";
import { Link, useHistory } from "react-router-dom";
import logo from "./img/SkywalkSmall.jpg";
import { IonHeader, IonSearchbar } from '@ionic/react';
import { useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from "../redux/state";
import { quitReadBookContent } from "../redux/book-content/action";
import MenuButton from "./MenuButton";
import { push } from 'connected-react-router';
import { directToSearch, turnOnReminder } from "../redux/search/action";
import { getCollectionListThunk, loadRankThunk } from "../redux/book-content/thunk";


const NavBar = () => {

  const [searchText, setSearchText] = useState('');
  const isReading = useSelector((state: RootState) => state.bookContent.isReading);
  const goSearch = useSelector((state: RootState) => state.search.goSearch);
  const userId = useSelector((state: RootState) => state.auth.user?.id);


  const dispatch = useDispatch();
  const history = useHistory();

  function goHome() {
    dispatch(quitReadBookContent(false));
    dispatch(directToSearch(true, true));
    setSearchText('');
  };

  function directToSearchPage() {
    if (goSearch === true) {
      dispatch(push('/search'));
      dispatch(directToSearch(false, true));
    }
  };


  function isCancelSearch() {
    history.goBack();
    console.log('i am in cancel search');
    dispatch(directToSearch(true, true));
  };



  return (
    <header>
      <Link to="/">
        <div className={styles.logoBox}>
        <img src={logo} className={styles.logo} onClick={goHome}></img>
        </div>
      </Link>
     
      <IonSearchbar className={styles.searchBar} placeholder="" value={searchText} showCancelButton="never" onFocus={(e) => directToSearchPage()}></IonSearchbar>
      
      {
        isReading ?
          <MenuButton />
          :
          <Link to="/book_shelf">
            <div className={styles.bookshelf} >收藏</div>
          </Link>
      }
    </header >
  );
};
export default NavBar;

function getCollectionList(userId: number) {
  throw new Error("Function not implemented.");
}
