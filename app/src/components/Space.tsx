
import styles from "./scss/Space.module.scss";

const Space = () => {
  return (
  <div className={styles.space7em}></div>
  );
};

export default Space;
