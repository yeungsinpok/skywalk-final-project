
import loginPageLogo from "./img/skywalk_login.jpg";
import facebookLogo from "./img/facebook-logo.png";
import googleLogo from "./img/google-logo.png";
import styles from './scss/DesktopLogin.module.scss'
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginWithPasswordThunk } from "../redux/auth/thunk";
import { RootState } from "../redux/state";
import { push } from "connected-react-router";
import { IonModal } from "@ionic/react";
import { loadWriterBookThunk } from "../redux/book-content/thunk";


const DesktopLogin = () => {
    const [generalUser, setGeneralUser]  = useState(false)
    const user = useSelector((state:RootState)=>state.auth.user);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const dispatch = useDispatch();

    async function loginWithPassword() {
        dispatch(loginWithPasswordThunk(email, password));
       
    }

    useEffect(()=>{
        if(user?.is_admin === true) {
            dispatch(push('/admin_account_manage'));
        } else if (user?.is_writer === true){
            dispatch(loadWriterBookThunk(user.id));
            dispatch(push('/desktop_view_book'));
        }  else {
            setGeneralUser(true)
           
        }   
        },[user]); 


    return (
        <div >
            <div className={styles.loginArea}>
                <img src={loginPageLogo} className={styles.loginPageIcon}></img>
                <div className={styles.usernameInput}>
                    <label htmlFor='username' className={styles.label}>用戶電郵</label>
                    <input type="text" className={styles.username} name="username" value={email} onChange={e=>setEmail(e.target.value)} required />
                </div>
                <div className={styles.passwordInput}>
                    <label htmlFor='password' className={styles.label}>密碼</label>
                    <input type="password" className={styles.password} name="password" value={password} onChange={e=>setPassword(e.target.value)} required />
                </div>
                <div className={styles.confirm}>
                    <div className={styles.confirmContainer}>
                        <button className={styles.confirmButton} onClick={()=>loginWithPassword()}>登入</button>
                    </div>
                </div>
                {/* {generalUser ?

                <IonModal>  dsdw</IonModal>
                     : "nonGen"
                
                } */}
                <div className={styles.facebook}>
                    <button className={styles.facebookButton}>
                        <img src={facebookLogo} className={styles.facebookLogo}></img>
                        Facebook 登入
                    </button>
                </div>
                <div className={styles.google}>
                    <button className={styles.googleButton}>
                        <img src={googleLogo} className={styles.googleLogo}></img>
                        Google 登入
                    </button>
                </div>
            </div>
        </div>
    )

}

export default DesktopLogin;