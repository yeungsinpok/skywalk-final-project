import sampleBook from "./img/book.jpg";
import { IonItem } from "@ionic/react";
import styles from "./scss/RankingBook.module.scss";
import { Link } from "react-router-dom";
// import { BookSummaryInfo } from "../redux/book-content/state";
import { useDispatch, useSelector } from "react-redux";
import { setIndividualBookInfo } from "../redux/book-content/action";
import { checkCollectionThunk, loadChapterGroupThunk } from "../redux/book-content/thunk";
import { RootState } from "../redux/state";
import { BookSummaryInfo } from "../model/book-content";
import { toImageUrl } from "../helpers/api";

type Props = {
  rankListInfo: BookSummaryInfo,
  rank: number

}

const RankingBook = (props: Props) => {

  // const userId = useSelector((state: RootState) => state.auth.user?.id);
  // const dispatch = useDispatch();
  
  // function checkoutBook() {
  //     dispatch(setIndividualBookInfo(props.rankListInfo));
  //     dispatch(loadChapterGroupThunk(bookId));
  //     if (userId){
  //         dispatch(checkCollectionThunk(userId , bookId));
  //       }
  //     }
      
  let bookId = parseInt(props.rankListInfo.book_id);

  return (
      // <IonItem className={styles.rankingBox} color="secondary" routerLink={"/book/"+bookId} onClick={checkoutBook} >
      <IonItem className={styles.rankingBox} color="secondary" routerLink={"/book/"+bookId} >
        <div className={styles.rankingBarBox} >
          <div className={styles.rankingBar}></div>
          <div className={styles.rankingBarNumber}>{props.rank+1}</div>
        </div>
        {/* <img src={sampleBook} className={styles.rankingBook}></img> */}
        <img alt={`《${props.rankListInfo.title}》的封面`} src={toImageUrl(props.rankListInfo.cover_image)} className={styles.rankingBook}></img>
        <div className={styles.rankingContentBox} >
          <div className={styles.rankingBookInfoBox}>
            <div className={styles.rankingBookTitle}>{props.rankListInfo.title}</div>
            <div className={styles.rankingBookAuthor}>{props.rankListInfo.author}</div>
          </div>
          <div className={styles.rankingBookContent} >
            {props.rankListInfo.abstract}
          </div>
          <div className={styles.rankingBookStateBox}>
            {
              props.rankListInfo.is_end ?
              <div className={styles.rankingBookStateFinished}>已完結</div>:
              <div className={styles.rankingBookState}>連載中</div>
            }
            <div className={styles.rankingBookLastUpdate}>更新至第{props.rankListInfo.chapter}章</div>
          </div>
        </div>

      </IonItem>
  );
};

export default RankingBook;
