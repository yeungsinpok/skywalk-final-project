import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from "@ionic/react";
import { push } from "connected-react-router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { registerThunk } from "../redux/auth/thunk";
import { RootState } from "../redux/state";
import styles from "./scss/Register.module.scss";

const Register = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');


  const [nameOk, setNameOk] = useState(true);
  const [emailOk, setEmailOk] = useState(true);
  const [passwordOk, setPasswordOk] = useState(false);

  const user = useSelector((state:RootState)=>state.auth.user);

  const dispatch = useDispatch();

  const nameRegex = /^\w+$/;
  const emailRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const passwordRegex = /^\w{6,}$/;

  const checkInput = () => {
    if (!nameRegex.test(name)) {
      setNameOk(false);
    }else {
      setNameOk(true);
    }
    if (!emailRegex.test(email)) {
      setEmailOk(false);
    }else {
      setEmailOk(true);
    }
    if (!passwordRegex.test(password)) {
      setPasswordOk(false);
    }else {
      setPasswordOk(true);
    }
  }

  useEffect(()=>{
    if (nameOk && emailOk && passwordOk) {
      dispatch(registerThunk(name, email, password));
    }
  }, [nameOk, emailOk, passwordOk]);


  // let admin name = alice
  useEffect(()=>{
    if (user?.is_admin === true){
      return;
    }else if (user) {
      dispatch(push('/'));
    }
  }, [user]);

  return (
    
    <>
    <IonHeader>
      <IonToolbar color="primary"  >
          <IonTitle className={styles.appTitle}>註冊</IonTitle>
      </IonToolbar>
    </IonHeader>
    <IonContent>
        <div className={styles.registerArea}>
          <div className={styles.nameInput}>
            <label htmlFor="name" className={styles.label}>
              用戶名稱
            </label>
            <input
              type="text"
              className={styles.name}
              name="name"
              value={name}
              onChange={e => setName(e.target.value)}
              required
            />
          </div>
          {
            nameOk ?
              null :
              <div className={styles.reminder}>用戶名稱只可以是文字或數字</div>
          }
          <div className={styles.emailInput}>
            <label htmlFor="email" className={styles.label}>
              電郵地址
            </label>
            <input
              type="email"
              className={styles.email}
              name="email"
              value={email}
              onChange={e => setEmail(e.target.value)}
              required
            />
          </div>
          {
            emailOk ?
              null :
              <div className={styles.reminder}>電郵地址不乎合格式，請重新輸入</div>
          }
          <div className={styles.passwordInput}>
            <label htmlFor="password" className={styles.label}>
              密碼
            </label>
            <input
              type="password"
              className={styles.password}
              name="password"
              value={password}
              onChange={e => setPassword(e.target.value)}
              required
            />
          </div>
          {
            passwordOk?
            null:
            <div className={styles.reminder}>最少為6個英文子母或數字</div>
          }
          <div className={styles.confirm}>
            <div className={styles.confirmContainer}>
              <button className={styles.confirmButton} onClick={checkInput}>確定</button>
            </div>
          </div>
        </div>
        </IonContent>
    </>
  );
};

export default Register;
