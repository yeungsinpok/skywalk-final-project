import styles from "./scss/BookUpdateDate.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { readBookContent, setTotalChapter } from "../redux/book-content/action";
import { Link } from "react-router-dom";
import { RootState } from "../redux/state";
import { addChapterViewThunk } from "../redux/view/thunk";
import { loadChapterGroupThunk, loadChapterThunk } from "../redux/book-content/thunk";
import { useIonRouter } from "@ionic/react";
import { useEffect, useState } from "react";
import { ChapterGroupArray } from "../model/book-content";

type Props = {
  chapterId: number;
  latest_read_chapter?: string;
  bookId: string;
}

const UserBookRecord = (props: Props) => {
  //const chapterContent = useSelector((state: RootState) => state.bookContent.chapterContent);
  // const isReading = useSelector((state: RootState) => state.bookContent.isReading);
  // const chapterId = useSelector((state: RootState) => state.bookContent.chapterId);
  const userId = useSelector((state: RootState) => state.auth.user?.id);
 

  const dispatch = useDispatch();


  useEffect(() => {
  }, [props.chapterId])


  const router = useIonRouter();


    function readBook() {
    dispatch(readBookContent(true));
    console.log("dispatch readBookContent action", props.latest_read_chapter);
    router.push(`/bookId/${props.bookId}/chapter/${props.latest_read_chapter}`, "none");
    if (userId != undefined && props.latest_read_chapter != undefined) {
      dispatch(loadChapterThunk(props.chapterId))
    }
  }


  return (
    
        <div onClick={readBook} >繼續閱讀第{props.latest_read_chapter}章</div>
    
  )
}

export default UserBookRecord
