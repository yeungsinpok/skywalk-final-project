import { IonItem } from "@ionic/react"
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { BookSummaryInfo } from "../model/book-content";
import { setIndividualBookInfo } from "../redux/book-content/action";
// import { BookSummaryInfo } from "../redux/book-content/state";
import { checkCollectionThunk, loadChapterGroupThunk } from "../redux/book-content/thunk";
import { RootState } from "../redux/state";
import styles from "./scss/OtherRankingList.module.scss";

type Props = {
  rankListInfo: BookSummaryInfo,
  rank: number
}

const OtherRankingList = (props: Props) => {
  // const userId = useSelector((state: RootState) => state.auth.user?.id);
  // const dispatch = useDispatch();

  // function checkoutBook() {
  //   dispatch(setIndividualBookInfo(props.rankListInfo));
  //   dispatch(loadChapterGroupThunk(props.rankListInfo.book_id));
  //   if (userId){
  //     dispatch(checkCollectionThunk( userId , parseInt(props.rankListInfo.book_id)));
  //   }
  // }
  let bookId = parseInt(props.rankListInfo.book_id);
  
  return (
    // <Link to="/book" onClick={checkoutBook} >
      <IonItem color="secondary" className={styles.otherRankingListBox} routerLink={"/book/"+bookId}>
        <div className={styles.otherRankingList}>
          <div className={styles.otherRankNumber}>{props.rank+1}</div>
          <div className={styles.otherRankTitle}>{props.rankListInfo.title}</div>
          <div className={styles.otherRankAuthor}>{props.rankListInfo.author}</div>
        </div>
      </IonItem>
    // </Link >
  )
}

export default OtherRankingList
