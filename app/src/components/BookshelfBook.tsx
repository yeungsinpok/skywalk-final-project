import sampleBook from "./img/book.jpg";
import BookUpdateDate from "../components/BookUpdateDate";
import styles from "./scss/BookshelfBook.module.scss";
import { NavLink } from "react-router-dom";
import { BookSummaryInfo } from "../model/book-content";
import { IonButton, IonItem, useIonRouter } from "@ionic/react";
import { format } from "date-fns";
import { toImageUrl } from "../helpers/api";

type Props = {
  bookSummaryInfo: BookSummaryInfo;
};

const BookshelfBook = (props: Props) => {
  let bookId = parseInt(props.bookSummaryInfo.book_id);
  const router = useIonRouter();

  return (
    <div color="secondary" className={styles.bookshelfBox}>
      <img
        alt={`《${props.bookSummaryInfo.title}》的封面`} src={toImageUrl(props.bookSummaryInfo.cover_image)} 
        className={styles.bookshelfBook}
        onClick={() => router.push("/book/" + bookId)}
      ></img>
      <div className={styles.bookshelfContentBox}>
        <div className={styles.bookshelfBookInfoBox}>
          <div className={styles.bookshelfBookTitle}>
            <span className={styles.bookshelfBookTitleUnderline}>
              {props.bookSummaryInfo.title}
            </span>

            {props.bookSummaryInfo.is_end ? (
              <span className={styles.bookshelfBookStateFinished}>已完結</span>
            ) : (
              <span className={styles.bookshelfBookState}>連載中</span>
            )}
          </div>
          <div className={styles.bookshelfBookAuthor}>
            {props.bookSummaryInfo.author}
          </div>
        </div>

        <div className={styles.updateInfoBox}>

          <div className={styles.dateBox}>
            <div className={styles.bookshelfBookLastUpdate}>
              更新至第{props.bookSummaryInfo.chapter}章
            </div>
            <div className={styles.updateDate}>
              最近更新:{" "}
              {format(
                new Date(props.bookSummaryInfo.latest_update),
                "yyyy-MM-dd"
              )}
            </div>
          </div>
            <div className={styles.viewBox}>
             <div className={styles.view}>
              <BookUpdateDate
                chapterId={props.bookSummaryInfo.chapter_id}
                latest_read_chapter={
                  props.bookSummaryInfo.latest_read_chapter
                    ? props.bookSummaryInfo.latest_read_chapter
                    : undefined
                }
                bookId={props.bookSummaryInfo.book_id}
              />
              </div>
            </div>
        </div>
       
      </div>
    
    </div>
  );
};

export default BookshelfBook;
