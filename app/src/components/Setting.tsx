import styles from './scss/Setting.module.scss';
import settingPageLogo from "./img/skywalk_login.jpg";
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../redux/auth/action';
import { push } from 'connected-react-router';
import { RootState } from '../redux/state';
import { goDesktopSide } from '../redux/desktop/action';
import { Link, useHistory } from 'react-router-dom';

const Setting = () => {

    const userId = useSelector((state:RootState)=>state.auth.user?.id);
    const is_admin = useSelector((state:RootState)=>state.auth.user?.is_admin);
    const is_writer = useSelector((state:RootState)=>state.auth.user?.is_writer);
    const name = useSelector((state:RootState)=>state.auth.user?.name);
    const dispatch = useDispatch();
    
    function logoutNow() {
        dispatch(logout());
        localStorage.removeItem('access_token');
        dispatch(push('/'));
    }

    const history = useHistory();
    
    function goDesktop() {
        dispatch(goDesktopSide(true));
        console.log("dispatch go Desktop action");
        if(is_admin === true) {
            history.push("/admin_account_manage",'none');
        } else if (is_writer === true){
            history.push("/desktop_view_book");
        }  else {
            return;
        }   
      }

      
       

    return (
        <div className={styles.settingContainer}>
            <div className={styles.settingArea}>
                <img src={settingPageLogo} className={styles.settingPageIcon}></img>
                <div className={styles.settingContent}>
                    <div className={styles.accountInfo}>
                        <div className={styles.accountInfoTitle}>用戶帳號:</div>
                        <div>{userId}</div>
                    </div>
                    <div className={styles.accountInfo}>
                        <div className={styles.accountInfoTitle}>用戶名稱:</div>
                        <div>{name}</div>
                    </div>
                    <div className={styles.accountInfo}>
                        <div className={styles.version}>安裝版本:</div>
                        <div className={styles.versionNum}>v1.85</div>
                    </div>
                    <div className={styles.accountInfo}>
                        <div className={styles.version}>最新版本:</div>
                        <div className={styles.versionNum}>v1.85</div>
                    </div>
                    <div className={styles.accountInfo}>
                        <div className={styles.confirmContainer}>
                            <button className={styles.confirmButton}>立即更新</button>
                            {is_writer === true?
                           
                            <button className={styles.desktopButton} onClick={goDesktop}>作者版面</button>
                            :is_admin === true ?
                            <button className={styles.desktopButton} onClick={goDesktop}>管理員版面</button>
                                :<></>
                            }
                        </div>
                    </div>
                    <div className={styles.accountInfo}>使用者守則和條款</div>

                    <div className={styles.accountInfo}>
                        <div className={styles.confirmContainer}>
                            <button className={styles.confirmButton} onClick={()=>logoutNow()}>登出帳戶</button>
                        </div>
                        <div className={styles.confirmContainer}>
                            <button className={styles.deleteButton}>刪除帳戶</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default Setting;