
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import logo from "./img/SkywalkSmall.jpg";
import styles from "./scss/DesktopBar.module.scss";
import { push } from 'connected-react-router';
import { logout } from '../redux/auth/action';
import { Link } from "react-router-dom";

const DesktopBar = () => {
  const user = useSelector((state:RootState)=>state.auth.user);
  const email = useSelector((state:RootState)=>state.auth.user?.email);
  const name = useSelector((state:RootState)=>state.auth.user?.name);
  
  const dispatch = useDispatch();
  function logoutNow() {
    dispatch(logout());
    dispatch(push('/'));
}

  
  return (
    <header>
      <Link to="/desktop_view_book" >
      <img src={logo} className={styles.logo}></img>
      </Link>
      {
        user ?
        <>
        
        <div className={styles.accountBox}>
          <div className={styles.accountName}>您好, <span>{name}</span></div>
          <div className={styles.block}> | </div> 
          <div className={styles.logoutButton} onClick={()=>logoutNow()}>登出</div>
        </div>
        </>
        :
        <></>
      }
    </header >
  );
};
export default DesktopBar;
