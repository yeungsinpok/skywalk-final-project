import sampleBook from "./img/book.jpg";
import styles from "./scss/UpdatedBook.module.scss";
import { Link } from "react-router-dom";
import { BookSummaryInfo } from "../redux/book-content/state";
import { format } from 'date-fns';
import { useDispatch, useSelector } from "react-redux";
import { setIndividualBookInfo } from "../redux/book-content/action";
import { checkCollectionThunk, loadChapterGroupThunk } from "../redux/book-content/thunk";
import { RootState } from "../redux/state";
import { IonItem, useIonRouter } from "@ionic/react";
import { toImageUrl } from "../helpers/api";

type Props = {
  latestUpdateContent: BookSummaryInfo;
 
}

const UpdatedBook = (props:Props) => {


  // function checkoutBook() {
  //   dispatch(setIndividualBookInfo(props.latestUpdateContent));
  //   dispatch(loadChapterGroupThunk(props.latestUpdateContent.book_id));
  //   if (userId){
  //     dispatch(checkCollectionThunk( userId , parseInt(props.latestUpdateContent.book_id)));
  //   }
  // }

  let bookId = parseInt(props.latestUpdateContent.book_id);
  const router = useIonRouter()
  return (
      <>
      <div className={styles.rankingBox} color="secondary" onClick={()=>router.push("/book/"+bookId)}>
        <div className={styles.updatedBox} color="secondary">
          <img alt={`《${props.latestUpdateContent.title}》的封面`} src={toImageUrl(props.latestUpdateContent.cover_image)}  className={styles.updatedBook}></img>
          <div className={styles.updatedContentBox}>
            <div className={styles.updatedBookInfoBox}>
              <div className={styles.updatedBookTitle}>
                <span className={styles.updatedBookTitleUnderline}>{props.latestUpdateContent.title}</span>
                {
                  props.latestUpdateContent.is_end ?
                    <span className={styles.updatedBookStateFinished}>已完結</span> :
                    <span className={styles.updatedBookState}>連載中</span>
                }
              </div>
              <div className={styles.updatedBookAuthor}>{props.latestUpdateContent.author}</div>
            </div>
            <div className={styles.updateBookInfoBox}>
              <div className={styles.updatedBookLastUpdate}>更新至第{props.latestUpdateContent.chapter}章</div>
              <div className={styles.updateBookDate}>最近更新: {format(new Date(props.latestUpdateContent.latest_update), 'yyyy-MM-dd')}</div>
            </div>
          </div>
          </div>
        </div>
      </>
    
  );
};

export default UpdatedBook;
