import styles from "./scss/AdminAuthorManage.module.scss";
import { IonButton, useIonRouter } from "@ionic/react";
import { useEffect, useState } from "react";
import { OffShelfBookList, UserList, WriterList } from '../model/book-content';
import { format } from 'date-fns';

import elementStyles from "./scss/AdminOffShelfBookListPageElement.module.scss";
import { adminIsManaging, isChangeToUser } from "../redux/book-content/action";
import { useDispatch } from "react-redux";


type Props = {
    resultInfo: OffShelfBookList;
  };

const AdminOffShelfBookListPageElement = (props: Props) => {

  return (
            <div className={elementStyles.adminAccountManageBox}>
              <div className={elementStyles.userEmail}>{props.resultInfo.author_id}</div>
              <div className={elementStyles.userEmail}>{props.resultInfo.author}</div>
              <div className={elementStyles.userID}>{props.resultInfo.id}</div>
              <div className={elementStyles.username}>{props.resultInfo.title}</div>
              <div className={elementStyles.date}>{format(new Date(props.resultInfo.created_at), 'yyyy-MM-dd ')}<br /><span>{format(new Date(props.resultInfo.created_at), 'p')}</span></div>
            </div>
  );
};

export default AdminOffShelfBookListPageElement;
