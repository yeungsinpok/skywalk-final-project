import { IonContent, IonHeader, IonPage, IonToolbar } from "@ionic/react";
import { useEffect, useState } from "react";
import { RootState } from "../redux/state";
import styles from "./scss/BookChapterContent.module.scss";
import { ChapterContentList } from '../model/book-content';
import { useDispatch, useSelector } from "react-redux";
import { goDesktopSide } from "../redux/desktop/action";
import { useParams } from "react-router";
import ReactQuill from "react-quill";

type Props = {
  content: string | null;
}

const BookChapterContent = (props: Props) => {

  //let chapterId = ~~useParams<{ chapterId: string }>().chapterId;
  const fontSize = useSelector((state: RootState) => state.theme.fontSize);
  //const chapterContent = useSelector((state: RootState) => state.bookContent.chapterContent);

  return (

    <main>
      <IonContent color="secondary">
      <div className={styles.bookChapterContent}>
        <ReactQuill
          value={props.content || ""}
          readOnly={true}
          theme={"bubble"}
        > 
        <div style={{ fontSize: fontSize }} />
        </ReactQuill>
      </div>
      </IonContent>
    </main>

  )

}
export default BookChapterContent;
