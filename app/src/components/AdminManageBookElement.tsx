import styles from "./scss/AdminManageBookElement.module.scss";
import sampleBook from "./img/book.jpg";
import { Link } from "react-router-dom";
import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonItem,
  useIonRouter,
} from "@ionic/react";
import { AdminGetBookInfo } from "../redux/book-content/state";
//import { WriterBookInfo } from "../model/book-content";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadWriterChapterThunk } from "../redux/book-content/thunk";
import { RootState } from "../redux/state";
import { push } from "connected-react-router";
import { adminIsManaging, setEdit, setWriterBookIdChapter } from "../redux/book-content/action";
import { toImageUrl } from "../helpers/api";

type Props = {
  bookInfo: AdminGetBookInfo;
};

const DesktopManageBookElement = (props: Props) => {
  const [chapter, setChapter] = useState("");
  const [chapterOk, setChapterOk] = useState(true);

  // const writerChapter = useSelector(
  //   (state: RootState) => state.bookContent.writerChapter
  // );

  const chapterRegex = /^\d+$/;

  const dispatch = useDispatch();
  

  const checkChapterInput = () => {
    dispatch(adminIsManaging(true))
    if (!chapterRegex.test(chapter) || parseInt(chapter) > props.bookInfo.chapter || parseInt(chapter) < 1) {
      setChapterOk(false);
    } else {
      router.push(`/admin_manage_chapter/${props.bookInfo.book_id}/${chapter}`);
      setChapterOk(true);
    }
  };

  // useEffect(() => {
  //   if (chapterOk) {
  //     dispatch(
  //       loadWriterChapterThunk(props.bookInfo.book_id, parseInt(chapter))
  //     );
  //   }
  // }, [chapterOk]);

  const router = useIonRouter();

async function onShelf (bookId: number) {
  let origin;
  try {
    let { REACT_APP_API_SERVER } = process.env;
    if (!REACT_APP_API_SERVER) {
      console.error("missing REACT_APP_API_SERVER in env");
      throw new Error("missing REACT_APP_API_SERVER in env");
    }
    origin = REACT_APP_API_SERVER;
  } catch (error) {
    console.log(error);
    return;
  }
  let json: any;
  try {
    const keyWord = {
      bookId: bookId,
    };
    console.log("bookId:", bookId);
    let res = await fetch(`${origin}/admin/book_list/on_shelf`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(keyWord),
    });
    json = await res.json();
  } catch (error) {
    console.error("failed to onShelf API:", error);
    return;
  }
  if (json.error) {
    console.error("onShelf response error:", json.error);
    return;
  }

  try {
    console.log("onShelf successfully");
    dispatch(adminIsManaging(true))
    return;
  } catch (error) {
    console.error("failed to dispatch onShelf action", error);
  }


}

async function offShelf (bookId: number){
  let origin;
  try {
    let { REACT_APP_API_SERVER } = process.env;
    if (!REACT_APP_API_SERVER) {
      console.error("missing REACT_APP_API_SERVER in env");
      throw new Error("missing REACT_APP_API_SERVER in env");
    }
    origin = REACT_APP_API_SERVER;
  } catch (error) {
    console.log(error);
    return;
  }
  let json: any;
  try {
    const keyWord = {
      bookId: bookId,
    };
    console.log("bookId:", bookId);
    let res = await fetch(`${origin}/admin/book_list/off_shelf`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(keyWord),
    });
    json = await res.json();
  } catch (error) {
    console.error("failed to call offShelf API:", error);
    return;
  }
  if (json.error) {
    console.error("offShelf response error:", json.error);
    return;
  }
 
  try {
    console.log("offShelf successfully");
    dispatch(adminIsManaging(true))
    return;
  } catch (error) {
    console.error("failed to dispatch offShelf action", error);
  }
}


  return (
    <>
      <div className={styles.elementBox}>
        {/* <img src={sampleBook} className={styles.bookCover}></img> */}
        <img alt={`《${props.bookInfo.title}》的封面`} src={toImageUrl(props.bookInfo.cover_image)} className={styles.bookCover}></img>
<div className={styles.area}>
        <div className={styles.viewBookElement}>
          <div className={styles.viewBookBox}>
            <IonCard>
              <IonCardHeader>
                <IonCardSubtitle color="tertiary">
                  <span >
                    更新至第:{" "}
                    <span >
                      {props.bookInfo.chapter}
                    </span>
                    章
                  </span>
                </IonCardSubtitle>
                <IonCardTitle color="primary">
                  {" "}
                  <span >{props.bookInfo.title}</span>
                </IonCardTitle>
              </IonCardHeader>
              <IonCardContent>
                筆名:{" "}
                <span className={styles.author}>{props.bookInfo.author}</span>
                <span className={styles.bookContentType}>
                  <br />
                  類別:{" "}
                  <span className={styles.category}>
                    {props.bookInfo.category.map((category) => (
                      <span key={category}>{category}&nbsp;</span>
                    ))}
                  </span>
                </span>
                <span className={styles.bookContentType}>
                  <br />
                  連載情況:{" "}
                  <span className={styles.isEnd}>
                    {props.bookInfo.is_end ? "已完結" : "連載中"}
                  </span>
                </span>
                <span className={styles.bookContentType}>
                  <br />
                  上架狀況:{" "}
                  <span className={styles.isEnd}>
                    {props.bookInfo.is_off_shelf ? "已下架" : "已上架"}
                  </span>
                </span>
              </IonCardContent>
            </IonCard>
          </div>
        </div>

        <div className={styles.box2}>
          <IonCard>
          <IonCardContent>
              { props.bookInfo.is_off_shelf ?
                <><IonButton
                color="success"
                className={styles.uploadNewChapter}
                onClick={()=>{onShelf(props.bookInfo.book_id)}}>
                上架這本小說
              </IonButton>

              <IonButton  disabled color="danger" className={styles.delBook} onClick={()=>{offShelf(props.bookInfo.book_id)}}>
              下架這本小說
              </IonButton>
              </>
              :
              <>
              <IonButton
              disabled 
                color="success"
                className={styles.uploadNewChapter}
                onClick={()=>{onShelf(props.bookInfo.book_id)}}>
                上架這本小說
              </IonButton>

              <IonButton color="danger" className={styles.delBook} onClick={()=>{offShelf(props.bookInfo.book_id)}}>
              下架這本小說
              </IonButton></>

                
              }

              
        
         
        <br/>
            <span className={styles.editChapter}>
                章節 / 番外篇{" "}  </span>
              <input
                type="text"
                className={styles.chapterNumber}
                value={chapter}
                onChange={(e) => setChapter(e.target.value)}
              />
              <br/>
                <span className={styles.reminder}>(只可以輸入數字)
                {
                  chapterOk?
                  null:
                  '沒有此章節'
                }
                </span>
                <br/>
                <div className={styles.editChapterLabelBox}>
              <IonButton color="primary" className={styles.editChapterLabel} onClick={checkChapterInput}>
              預覽小說內容{" "}
              </IonButton>
              </div>
            </IonCardContent>
          </IonCard>

          </div>
        </div>
      </div>
    </>
  );
};

export default DesktopManageBookElement;
