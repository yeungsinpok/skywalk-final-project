import sampleBook from "./img/book.jpg";
import elementStyles from "./scss/CreateBookElement.module.scss";
import {
  IonButton,
  IonRadio,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonRadioGroup,
  IonCheckbox,
  
} from "@ionic/react";


const CreateBookElement = () => {
  return (
    <main>
      <div className={elementStyles.createBookArea}>
        <div className={elementStyles.coverArea}>
          <img src={sampleBook} className={elementStyles.bookCover}></img>
          <div className={elementStyles.uploadBox}>
            <IonButton color="light" className={elementStyles.uploadBtn}>
              上載小說封面
            </IonButton>
          </div>
        </div>
        <div className={elementStyles.createBookFormArea}>
          <div className={elementStyles.textArea}>

            <div className={elementStyles.inputBox}>
              <label htmlFor="bookTitle" className={elementStyles.inputLabel}>
                小說書名<span className={elementStyles.inputPS}> (80字以內)</span>
              </label>
              <br/>
              <input type="text" className={elementStyles.input} />
            </div>

            <div className={elementStyles.inputBox}>
              <label htmlFor="penName" className={elementStyles.inputLabel}>
              筆名<span className={elementStyles.inputPS}> (10字以內)</span>
              </label>
              <input type="text" className={elementStyles.input} />
            </div>
            

            <div className={elementStyles.inputBox}>
              <label htmlFor="bookSummary" className={elementStyles.inputLabel}>
              內容簡介<span className={elementStyles.inputPS}> (80字以內)</span>
              </label>
              <textarea className={elementStyles.inputSummary} />
            </div>
          

              <div className={elementStyles.checkboxArea}>
              <IonList>
                <IonRadioGroup>
                <IonListHeader className={elementStyles.checkboxTitle}>小說類型</IonListHeader>
                <IonItem>
                  <IonLabel className={elementStyles.checkboxLabel}>異界</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem>

                <IonItem>
                  <IonLabel className={elementStyles.checkboxLabel}>魔法</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem>

                <IonItem>
                  <IonLabel className={elementStyles.checkboxLabel}>校園</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem>

                <IonItem>
                  <IonLabel className={elementStyles.checkboxLabel}>愛情</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem>

                <IonItem>
                  <IonLabel className={elementStyles.checkboxLabel}>懸疑</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem>

                <IonItem>
                  <IonLabel className={elementStyles.checkboxLabel}>科幻</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem>

                <IonItem >
                  <IonLabel className={elementStyles.checkboxLabel}>冒險</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem >

                <IonItem >
                  <IonLabel className={elementStyles.checkboxLabel}>武俠</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem >

                <IonItem >
                  <IonLabel className={elementStyles.checkboxLabel}>搞笑</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem >


                <IonItem >
                  <IonLabel className={elementStyles.checkboxLabel}>玄幻</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem >

                <IonItem >
                  <IonLabel className={elementStyles.checkboxLabel}>散文</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem >

                <IonItem >
                  <IonLabel className={elementStyles.checkboxLabel}>同人</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem >

                <IonItem >
                  <IonLabel className={elementStyles.checkboxLabel}>言情</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem >

                <IonItem >
                  <IonLabel className={elementStyles.checkboxLabel}>女生</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem >

                <IonItem >
                  <IonLabel className={elementStyles.checkboxLabel}>其他</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start" className={elementStyles.checkbox}
                  ></IonCheckbox>
                </IonItem >

                <IonItem>
                  <IonLabel className={elementStyles.checkboxLabel18}>18+</IonLabel>
                  <IonCheckbox
                    color="danger"
                    slot="start" className={elementStyles.checkbox18}
                  ></IonCheckbox>
                </IonItem>
                </IonRadioGroup>
              </IonList>

              <IonList>
              <IonRadioGroup>
                <IonListHeader className={elementStyles.checkboxTitle}>連載狀態</IonListHeader>
                <IonItem>
                  <IonLabel className={elementStyles.checkboxLabel}>連載中</IonLabel>
                  <IonRadio
                    color="warning"
                    slot="start" className={elementStyles.checkbox}
                  ></IonRadio>
                </IonItem>

                <IonItem>
                  <IonLabel className={elementStyles.checkboxLabel}>已完結</IonLabel>
                  <IonRadio
                    color="success"
                    slot="start" className={elementStyles.checkbox}
                  ></IonRadio>
                </IonItem>

              
                </IonRadioGroup>
                </IonList>

                </div>
                <div className={elementStyles.submitBox}>
                <IonButton color="danger" size="small" className={elementStyles.submitBtn} >
                確定
                </IonButton>
                </div>
            </div>
        
           
          </div>
        </div>
  
    </main>
  );
};
export default CreateBookElement;
