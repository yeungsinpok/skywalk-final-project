import styles from "./scss/BookChapterElement.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { readBookContent } from "../redux/book-content/action";
import { Link } from "react-router-dom";
//import { addChapterViewThunk } from "../redux/view/thunk";
import { loadChapterThunk } from "../redux/book-content/thunk";
//import { ChapterGroupArray } from "../model/book-content";
import { ChapterGroupArray } from "../redux/book-content/state";
import { useIonRouter } from "@ionic/react";
import { useEffect } from "react";
import { useParams } from "react-router";
import { addChapterViewThunk } from "../redux/view/thunk";

type Props = {
  chapterInfo: ChapterGroupArray;
}

const BookChapterElement = (props:Props) => {
  
  let bookId = ~~useParams<{ id: string }>().id;
  const userId = useSelector((state: RootState) => state.auth.user?.id);
  const dispatch = useDispatch();

  async function addChapterView(userId: number, bookId: number, chapter: number) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      const chapterViewInfo = {
        userId,
        bookId,
        chapter
      };
      console.log("userId:", userId);
      let res = await fetch(`${origin}/view/add_chapter`, {
        method: 'POST',
        headers: {
          "Authorization": `Bearer ${localStorage.getItem('access_token')}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(chapterViewInfo)
      })
      json = await res.json();
    } catch (error) {
      console.error('Failed to add record in chapter_view table:', error);
      return
    }

    if (json.error) {
      console.error('Add chapter view response error:', json.error);
      return
    }

    if (json.success == true) {
      console.log('Add chapter view successfully');
      return
    }
  }

  function readBook() {
    dispatch(readBookContent(true));
    if (userId){
      addChapterView(userId, bookId, props.chapterInfo.chapter)
    }
    console.log("dispatch readBookContent action");
    router.push(`/bookId/${bookId}/chapter/${props.chapterInfo.chapter}`, "none");
    console.log('Dispatch chapter ID.');
    // dispatch(addChapterViewThunk(userId, props.chapterInfo.chapter_id));
    dispatch(loadChapterThunk(props.chapterInfo.chapter_id));      
  }

  const router = useIonRouter();
  return (
      <div className={styles.bookChapterElementBox}>
      <div className={styles.bookChapterElementWord} onClick={readBook}>第{props.chapterInfo.chapter}章</div>
      </div>
  )
}

export default BookChapterElement;
