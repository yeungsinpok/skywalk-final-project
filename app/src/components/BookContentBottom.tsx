import styles from "./scss/BookContentBottom.module.scss";
import { loadNextChapterThunk, loadPrevChapterThunk } from "../redux/book-content/thunk";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { addChapterViewThunk } from "../redux/view/thunk";
import { useEffect } from "react";
import { ChapterContentList, ChapterGroupArray } from "../redux/book-content/state";
import { useParams } from "react-router";
import { useIonRouter } from "@ionic/react";

interface Props {
  //   bookId: number,
  //   chapterId: number;
}

export default function BookContentBottom(props: Props) {
  const router = useIonRouter();
  const totalChapter = useSelector((state: RootState) => state.bookContent.totalChapter);
  const userId = useSelector((state: RootState) => state.auth.user?.id);
  const chapterNumber = useSelector((state: RootState) => state.bookContent.chapterGroup);
  const chapterContent = useSelector((state: RootState) => state.bookContent.chapterContent);
  //const chapterGroup = useSelector((state: RootState) => state.bookContent.chapterGroup);

  const chapter = chapterContent?.map((chapterRow) => (
    chapterRow.chapter
  ))
  const bookId = chapterContent?.map((chapterRow) => (
    chapterRow.book_id
  ))
  const chapterId = chapterContent?.map((chapterRow) => (
    chapterRow.chapter_id
  ))
  // let book_Id = ~~useParams<{ bookId: string }>().bookId;
  // let chapterBook_Id = ~~useParams<{ chapter: string }>().chapter;

  const dispatch = useDispatch();


  async function addChapterView(userId: number, bookId: number, chapter: number) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      const chapterViewInfo = {
        userId,
        bookId,
        chapter
      };
      console.log("userId:", userId);
      let res = await fetch(`${origin}/view/add_chapter`, {
        method: 'POST',
        headers: {
          "Authorization": `Bearer ${localStorage.getItem('access_token')}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(chapterViewInfo)
      })
      json = await res.json();
    } catch (error) {
      console.error('Failed to add record in chapter_view table:', error);
      return
    }

    if (json.error) {
      console.error('Add chapter view response error:', json.error);
      return
    }

    if (json.success == true) {
      console.log('Add chapter view successfully');
      return
    }
  }

  useEffect(() => {
    if (userId && chapterId && bookId && chapter && chapterNumber) {
      console.log(chapterNumber[0]);
      dispatch(addChapterViewThunk(userId, chapterId[0]));
    }
  }, [chapterId]);


  function prevPage() {
    // 防止撳到零或負數，最小係一
    if (chapterId && bookId && chapter) {
      if (chapter[0] > 1) {
        dispatch(loadPrevChapterThunk(chapterId[0], bookId[0]));
        const prevChapterNumber = chapter[0] - 1
        router.push(`/bookId/${bookId}/chapter/${prevChapterNumber}`, "none");
        if (userId) {
          addChapterView(userId, bookId[0], chapter[0])
        }
      }
    }
  }

  function nextPage() {
    if (chapterId && bookId && chapter && totalChapter) {
      if (chapter[0] < totalChapter) {
        dispatch(loadNextChapterThunk(chapterId[0], bookId[0]));
        const nextChapterNumber = chapter[0] + 1
        router.push(`/bookId/${bookId}/chapter/${nextChapterNumber}`, "none");
        if (userId) {
          addChapterView(userId, bookId[0], chapter[0])
        }
      }

    }
  }

  return (

    <div className={styles.bottomBar}>
      <div className={styles.bar}>
        <button className={styles.prevChapter} onClick={prevPage}>
          上一章
        </button>
        <div className={styles.chapterNumber}>
          <span>{`第 ${chapter} 章`}</span>
        </div>
        <button className={styles.nextChapter} onClick={nextPage}>
          下一章
        </button>
      </div>
    </div>

  )
}


