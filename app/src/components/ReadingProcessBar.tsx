import React from 'react';
import styles from './scss/BookContentBottom.module.scss';
import { IonProgressBar, IonContent } from "@ionic/react";


export default function ReadingProcessBar() {
    return (
        <IonContent>

            {/*-- Default Progressbar with 50 percent --*/}
            <IonProgressBar className={styles.progressBar} value={0.1} color='success'></IonProgressBar>

            {/* 正式既話，應該係 (current chapter number / total chapter number) 讓IonProgressBar按比例顥示*/}
            
        </IonContent>
    );
}