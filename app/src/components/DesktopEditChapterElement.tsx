
import styles from "./scss/DesktopEditChapterElement.module.scss";
import sampleBook from "./img/book.jpg";
import { IonButton, IonItem, IonLabel, IonToggle, useIonRouter} from '@ionic/react';
import ReactQuill from "react-quill"
import "../../node_modules/react-quill/dist/quill.snow.css"
import { useState } from "react";
import App from "../App";
import { Link } from "react-router-dom";



const DesktopEditChapterElement = () => {
  const router = useIonRouter()
  return (
    <>
    <div className={styles.chapterEditorArea}>
      <div className={styles.btnBar}>
         <IonButton color="success" className={styles.previewBtn} size="small" onClick={()=>router.push('/desktop_preview_chapter','none')}>預覽小說</IonButton>
         <IonButton color="danger" className={styles.previewBtn} size="small">刪除章節</IonButton>
      </div>
      <ReactQuill
      placeholder="輸入小說內容.........."
      className={styles.chapterEditorBox}
        />
      
  
    </div>

    </>
  );
};
export default DesktopEditChapterElement;
