import styles from './scss/BookChapter.module.scss'
import BookChapterElement from "../components/BookChapterElement";
import BookChapterLatestUpdateElement from "../components/BookChapterLatestUpdateElement";
import { useSelector } from 'react-redux';
import { RootState } from '../redux/state';


const BookChapter = () => {

  const chapterGroup = useSelector((state: RootState) => state.bookContent.chapterGroup);
  //const book = useSelector((state: RootState) => state.bookContent.chapterContent);
  
  return (
    <div className={styles.bookChapterArea}>
      {/* <div className={styles.bookChapterBox}>
        {
          chapterGroup ?
            chapterGroup.map((chapter, index) => (
              index < 1 ?
                <BookChapterLatestUpdateElement key={chapter.chapter_id} chapterInfo={chapter} /> :
                <BookChapterElement key={chapter.chapter_id} chapterInfo={chapter} />
            )) :
            <></>
        }

      </div> */}
    </div>
  )
}

export default BookChapter
