import styles from './scss/NotFound.module.scss';
import notFoundPageLogo from "./img/skywalk_login.jpg";
import { Link } from 'react-router-dom';

const NotFound = () => {
    return (
        <div className={styles.notFoundContainer}>
            <div className={styles.notFoundArea}>
                <img src={notFoundPageLogo} className={styles.notFoundPageIcon}></img>
                <div className={styles.notFoundContent}>
                    <div className={styles.reminder}>對不起,找不到網頁。</div>
                    <div className={styles.reminder}>現在返回主頁</div>
                    
                    <div className={styles.confirm}>
                    <div className={styles.confirmContainer}>
                        <Link to="/">
                        <button className={styles.confirmButton}>確定</button>
                        </Link>
                    </div>
                </div>
                </div>
            </div>
        </div>
    )
}

export default NotFound;