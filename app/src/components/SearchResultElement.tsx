import { IonBackButton, IonButtons, IonContent, IonHeader,IonPage,  IonTitle,  IonToolbar, useIonRouter} from "@ionic/react"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { SearchByCatList } from "../model/book-content";
import { isSearching } from "../redux/book-content/action";
import { RootState } from "../redux/state";
import elementStyles from "./scss/SearchResultElement.module.scss";
import sampleBook from "../components/img/book.jpg";
import { format } from "date-fns";
import Space from "./Space";
import { toImageUrl } from "../helpers/api";


type Props = {
    resultInfo: SearchByCatList
  }

const SearchResultElement = (props: Props)=> {
const router = useIonRouter()
let resultData = props.resultInfo.book_id;

return (
            <>
            <div className={elementStyles.Box} color="secondary" onClick={()=>router.push("/book/"+resultData)}>
                <div className={elementStyles.updatedBox} color="secondary">
                {props.resultInfo.cover_image ?
                <img alt={`《${props.resultInfo.title}》的封面`} src={toImageUrl(props.resultInfo.cover_image)}  className={elementStyles.updatedBook}></img>
                : <></>
            }
                <div className={elementStyles.updatedContentBox}>
                    <div className={elementStyles.updatedBookInfoBox}>
                    <div className={elementStyles.updatedBookTitle}>
                        <span className={elementStyles.updatedBookTitleUnderline}>{props.resultInfo.title}</span>
                        {
                        props.resultInfo.is_end ?
                            <div className={elementStyles.updatedBookStateFinished}>已完結</div> :
                            <div className={elementStyles.updatedBookState}>連載中</div>
                        }
                    </div>
                    <div className={elementStyles.updatedBookAuthor}>{props.resultInfo.author}</div>
                    </div>
                    <div className={elementStyles.updateBookInfoBox}>
                    <div className={elementStyles.updatedBookLastUpdate}>更新至第{props.resultInfo.chapter}章</div>
                    <div className={elementStyles.updateBookDate}>最近更新: {format(new Date(props.resultInfo.latest_update), 'yyyy-MM-dd')}</div>
                    </div>
                </div>
                </div>
                </div>
            </>   
    )

}

export default SearchResultElement;