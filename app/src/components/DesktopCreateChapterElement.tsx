
import styles from "./scss/DesktopCreateChapterElement.module.scss";
import sampleBook from "./img/book.jpg";
import { IonButton, IonItem, IonLabel, IonToggle, useIonRouter } from '@ionic/react';
import ReactQuill from "react-quill"
import "../../node_modules/react-quill/dist/quill.snow.css"
import { useEffect, useState } from "react";
import App from "../App";
import { Link } from "react-router-dom";
import EditorToolbar, { modules, formats } from './DesktopCreateChapterElementToolBar';
import { useParams } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { isSearching, setEdit } from "../redux/book-content/action";
import { ChapterContentList } from "../model/book-content";


const DesktopCreateChapterElement = () => {

  const isEditing = useSelector((state: RootState) => state.bookContent.isEditing);
  const dispatch = useDispatch();

  const bookId = useSelector((state: RootState) => state.bookContent.bookId);
  const chapter = useSelector((state: RootState) => state.bookContent.chapter);

  const [chapterContent, setChapterContent] = useState({ editorHtml: "" });
  const handleChange = (html: any) => {
    setChapterContent({ editorHtml: html });
  };

  async function uploadChapter(bookId: number, chapter: number, chapterContent: { editorHtml: string }) {
    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    let newChapterContent = {
      bookId,
      chapter,
      chapterContent
    }
    try {
      let res = await fetch(`${origin}/book_content/upload_chapter`, {
        method: 'POST',
        headers: {
          "Authorization": `Bearer ${localStorage.getItem('access_token')}`,
          "Content-Type": "application/json"
        },
        body: JSON.stringify(newChapterContent)
      })

      json = await res.json();

    } catch (error) {
      console.error('failed to upload chapter API:', error);
      return
    }

    if (json.error) {
      console.error('upload chapter response error:', json.error);
      return
    }

    if (json.success == true) {
      console.log("upload chapter successfully");
      // dispatch(isSearching(true));
      router.push('/desktop_create_chapter_success', 'none');
      return
    }

  }

  async function downloadChapter(bookId: number, chapter: number) {

    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    try {
      let res = await fetch(`${origin}/book_content/bookId/${bookId}/chapter/${chapter}`, {
        method: 'GET'
      })
      json = await res.json();
    } catch (error) {
      console.error('failed to call load writer chapter record API:', error);
      return
    }

    if (json.error) {
      console.error('load writer chapter record response error:', json.error);
      return
    }

    let chapterToBeEdit: ChapterContentList[] = json.writerChapterRec;
    setChapterContent({ editorHtml: chapterToBeEdit[0].content });
    console.log("load writer chapter record successfully");
  }

  async function updateEditedChapter(bookId: number, chapter: number, chapterContent: { editorHtml: string }) {

    let origin;
    try {
      let { REACT_APP_API_SERVER } = process.env
      if (!REACT_APP_API_SERVER) {
        console.error('missing REACT_APP_API_SERVER in env');
        throw new Error('missing REACT_APP_API_SERVER in env');
      }
      origin = REACT_APP_API_SERVER;
    } catch (error) {
      console.log(error);
      return;
    }

    let json: any;
    let updateChapterContent = {
      bookId,
      chapter,
      chapterContent
    }
    try {
      let res = await fetch(`${origin}/book_content/update_edited_chapter`, {
        method: 'POST',
        headers: {
          "Authorization": `Bearer ${localStorage.getItem('access_token')}`,
          "Content-Type": "application/json"
        },
        body: JSON.stringify(updateChapterContent)
      })
      json = await res.json();
    } catch (error) {
      console.error('failed to call update edited chapter API:', error);
      return
    }

    if (json.error) {
      console.error('load update edited chapter response error:', json.error);
      return
    }

    if (json.success == true) {
      console.log("update edited chapter successfully");
      // router.push('/desktop_view_book', 'none');
      router.push('/desktop_create_chapter_success', 'none');
      return
    }




  }



  function handleOnClick() {
    if (isEditing == 1 && bookId && chapter != undefined) {
      uploadChapter(bookId, chapter, chapterContent);
      // dispatch(setEdit(0));
    }
    else if (isEditing == 2 && bookId && chapter) {
      updateEditedChapter(bookId, chapter, chapterContent);
      // dispatch(setEdit(0));
    }
  }



  useEffect(() => {
    if (isEditing == 2 && bookId && chapter) {
      console.log("Hello");
      console.log('In useEffect: book id:', bookId, 'chapter:', chapter);
      downloadChapter(bookId, chapter);
    } else if (isEditing == 1) {
      setChapterContent({ editorHtml: "" });
    }
  }, [isEditing]);



  const router = useIonRouter()
  return (
    <>
      <div className={styles.chapterEditorArea}>
        <div className={styles.btnBar}>

          <IonButton color="success" className={styles.previewBtn} size="small" onClick={handleOnClick}>
            {
              isEditing == 1?
              '上載新章節':
              isEditing == 2?
              '上載更新章節':
              null
            }
          </IonButton>

        </div>
        <EditorToolbar />
        <ReactQuill
          theme="snow"
          value={chapterContent.editorHtml || ""}
          onChange={handleChange}
          placeholder={"輸入小說內容.........."}
          modules={modules}
          formats={formats}
          className={styles.chapterEditorBox}
        />


      </div>

    </>
  );
};
export default DesktopCreateChapterElement;
