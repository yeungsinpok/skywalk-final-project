import styles from "./scss/NavBar.module.scss";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import { readBookContent } from "../redux/book-content/action";
import { RootState } from "../redux/state";
import BarIcon from "./BarIcon";
import BookContentBottom from "./BookContentBottom";
import { ChapterGroupArray, ChapterContentList } from "../model/book-content";

// type Props = {
//   isReading: boolean;
//   chapterNumber: ChapterGroupArray[];
//   chapterContent: ChapterContentList[];
//   chapterGroup: ChapterGroupArray[];
// };


const BottomBar = () => { 

const isReading = useSelector((state: RootState) => state.bookContent.isReading);
// const dispatch = useDispatch();
// dispatch(readBookContent(true));

  return (
    <>
         { isReading ? <BookContentBottom/> : <BarIcon /> }
    </>
  );
};

export default BottomBar;