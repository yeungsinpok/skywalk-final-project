import { useIonRouter } from "@ionic/react";
import { useDispatch, useSelector } from "react-redux";
import { isSearching, setEdit } from "../redux/book-content/action";
import { RootState } from "../redux/state";
import styles from "./scss/DesktopEditBar.module.scss";


const DesktopEditBar = () => {

  const dispatch = useDispatch();
  
  const router = useIonRouter()
  return (
    <div className={styles.desktopEditBar}>
      <div className={styles.desktopEditBarBtnBox}>
        <div className={styles.desktopEditBarBtn} onClick={
          () => {
            router.push('/desktop_view_book', 'none')
            dispatch(setEdit(0));
            // dispatch(isSearching(false));
          }
        }>管理書籍</div>
        <div className={styles.desktopEditBarBtn} onClick={() => router.push('/desktop_create_new', 'none')}>上載新書</div>
      </div>
    </div>

  )
}

export default DesktopEditBar


