import styles from "./scss/BookCommentData.module.scss";


const BookCommentData = () => {
    return (
      
     <div className={styles.commentBox}>
      <div className={styles.commentUser}>Eric</div>
      <div className={styles.commentDate}>2021-10-12</div>
      <div className={styles.commentDetail}>xxxxxxxxxxxxxxxxx!?</div>
      </div>
    )
  }
  
  export default BookCommentData
 