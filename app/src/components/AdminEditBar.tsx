import { useIonRouter } from "@ionic/react";
import { useDispatch } from "react-redux";
import { isChangeToUser, isChangeToWriter } from "../redux/book-content/action";
import styles from "./scss/AdminEditBar.module.scss";
import { useHistory } from "react-router";
import React from 'react';
import { IonButton, IonContent, IonPage, useIonLoading } from '@ionic/react';

interface LoadingProps {}

const AdminEditBar: React.FC<LoadingProps> = () => {
  const [present, dismiss] = useIonLoading();
  const history = useHistory();
  const dispatch = useDispatch();
  const router = useIonRouter();

function changeWriter(){
    dispatch(isChangeToWriter(false));
    router.push(`/admin_author_manage`, "none");
    // history.go(0)
}

function changeUer(){
  dispatch(isChangeToUser(false));
  router.push(`/admin_account_manage`, "none");
  // history.go(0)
}

  
    return (
      <div className={styles.desktopEditBar}>
        <div className={styles.desktopEditBarBtnBox}>
          <div className={styles.adminEditBarBtn} onClick={changeUer}>用戶管理</div>
          <div className={styles.adminEditBarBtn} onClick={changeWriter} >作者管理</div>
          <div className={styles.adminEditBarBtn} onClick={()=>router.push('/admin_off_shelf_book_list','none')}>未上架小說</div>
          <div className={styles.adminEditBarBtn} onClick={()=>router.push('/admin_adv_manage','none')}>廣告管理</div>
          <div className={styles.adminEditBarBtn} onClick={()=>router.push('/admin_create_adv','none')}>上載廣告</div>
        </div>
      </div>
      
    )
  }
  
  export default AdminEditBar;
 
