export type BookAd = {
    id: number;
    bookId: number;
    title: string;
    image: string;
}