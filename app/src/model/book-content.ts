export type BookSummaryInfo = {
    book_id: string;
    chapter: number;
    latest_update: string;
    title: string;
    author: string;
    abstract: string;
    is_end: boolean;
    rank: number;
    cover_image: string;
    chapter_id: number;
    latest_read_chapter?: string;
    rank_num?: number;
}

export type ChapterContentList = {
    book_id: number;
    chapter_id: number;
    chapter: number;
    content: string;
}

export type ChapterGroupArray = {
    chapter_id: number;
    chapter: number;
}

export type WriterBookInfo = {
    author_id: number
    book_id: number;
    chapter_id: number;
    chapter: number;
    title: string;
    author: string;
    is_end: boolean;
    cover_image: string;
    category: string[];
}

export type AdminGetBookInfo = {
    author_id: number
    book_id: number;
    chapter_id: number;
    chapter: number;
    title: string;
    author: string;
    is_end: boolean;
    is_off_shelf: boolean;
    cover_image: string;
    category: string[];
}

export type UserList = {
    id: number; //Should be id, instead of user_id???
    name: string;
    email: string;
    created_at: string;
}

export type WriterList = {
    id: number;
    name: string;
    email: string;
    created_at: string;
    is_writer: boolean;
    
    updated_at: string;
}


export type OffShelfBookList = {
    id: number;
    title: string;
    author_id: number;
    author: string;

    created_at: string;

}


export type AdvList = {
    id: number;
    image: string;  //file path
    title: string   //got by joining books table
    created_at: string;
    is_active: boolean;
    updated_at: string;
}

export type CollectionList = {
    book_id: number;
    chapter_id: number;
    chapter: number;
    rank: number;
    title: string;
    author: string;
    abstract: string;
    is_end: boolean;
    latest_update: string;
    cover_image: string;
}

export type CommentList = {
    comment_id: number;
    user_id: number;
    name: string;
    content: string;
    created_at: string;
}


export type SearchByCatList = {
    
      book_id: number;
      category: string;
      category_id: number;
      title: string;
      author: string;
      chapter: number;
      abstract: string;
      is_end: boolean;
      rank: number;
      cover_image: string | null;
      chapter_id: number;
      latest_update: string;
}


