import { Redirect, Switch, Route, NavLink } from "react-router-dom";
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

import React from "react";

// app page
import "./App.scss";
import BottomBar from "./components/BottomBar";
import HomePage from "./pages/HomePage";
import Record from "./pages/RecordPage";
import RankingPage from "./pages/RankingPage";
import Update from "./pages/UpdatePage";
import AboutMe from "./pages/AboutMePage";
import Bookshelf from "./pages/CollectionPage";
import BookPage from "./pages/BookPage";
import BookContent from "./pages/BookContentPage";
import NotFoundPage from "./pages/NotFoundPage";
import WarningMsg from "./pages/WarningMsgPage";
import SearchPage from "./pages/SearchPage";
import SearchResult from "./pages/SearchResult";
import SearchKeyWordResult from "./pages/SearchKeyWordResult";


// author page
import DesktopViewBook from "./pages/DesktopViewBookPage";
import DesktopCreateNew from "./pages/DesktopCreateNewPage";
import DesktopCreateChapter from "./pages/DesktopCreateChapterPage";
import DesktopEditChapter from "./pages/DesktopEditChapterPage";
import DesktopPreViewChapter from "./pages/DesktopPreViewChapterPage";
import DesktopCreateChapterSuccess from "./pages/DesktopCreateChapterSuccessPage";


//admin page
import AdminAccountManage from "./pages/AdminAccountManagePage";
import AdminAdvManage from "./pages/AdminAdvManagePage";
import AdminAuthorManage from "./pages/AdminAuthorManagePage";
import AdminCreateAdv from "./pages/AdminCreateAdvPage";
import AdminManageBook from "./pages/AdminManageBookPage";
import AdminManageChapter from "./pages/AdminManageChapterPage";
import AdminOffShelfBookList from "./pages/AdminOffShelfBookListPage";




/* Theme variables */
import "./theme/variables.css";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import { RootState } from "./redux/state";
import { RootDispatch } from "./redux/dispatch";
import { checkTokenThunk } from "./redux/auth/thunk";
import { connect} from "react-redux";
import BookContentBottom from "./components/BookContentBottom";
import { ChapterContentList, ChapterGroupArray } from "./redux/book-content/state";

type Props = {
  email: string | null;
  checkToken: () => void;
  goDesktop: boolean;
};

class App extends React.Component<Props> {
  componentDidMount() {
    this.props.checkToken();
  } 



  public render() {
    return (
      <IonApp color="secondary">
        <IonReactRouter>
        <IonRouterOutlet>
          {/* APP page */}
          <Route path="/" exact component={HomePage} />
          <Route path="/record" component={Record} />
          <Route path="/ranking" component={RankingPage} />
          <Route path="/update" component={Update} />
          <Route path="/about_me" component={AboutMe} />
          <Route path="/book_shelf" component={Bookshelf} />
          <Route path="/book/:id" component={BookPage} />
          <Route path="/bookId/:bookId/chapter/:chapter"component={BookContent}/>
          <Route path="/login" component={LoginPage} />
          <Route path="/register" component={RegisterPage} />
          <Route path="/search" component={SearchPage} />
          <Route path="/searching/by_category_id/:categoryId"component={SearchResult}/>
          <Route path="/searching/by_keyword"component={SearchKeyWordResult}/>
          <Route path="/warning_msg" component={WarningMsg} />
          <Route component={NotFoundPage} />
           {/* AUTHOR page */}
          <Route path="/desktop_view_book" component={DesktopViewBook} />
          <Route path="/desktop_create_new" component={DesktopCreateNew} />
          <Route path="/desktop_create_chapter/:bookId/:chapter"component={DesktopCreateChapter}/>
          <Route path="/desktop_edit_chapter" component={DesktopEditChapter} />
          <Route path="/desktop_preview_chapter"component={DesktopPreViewChapter}/>
          <Route path="/desktop_create_chapter_success"component={DesktopCreateChapterSuccess}/>
          {/* ADMIN page */}
          <Route path="/admin_account_manage" component={AdminAccountManage} />
          <Route path="/admin_author_manage" component={AdminAuthorManage} />
          <Route path="/admin_adv_manage" component={AdminAdvManage} />
          <Route path="/admin_create_adv" component={AdminCreateAdv} />
          <Route path="/admin_manage_book/:writerId" component={AdminManageBook} />
          <Route path="/admin_manage_chapter/:bookId/:chapter" component={AdminManageChapter} />
          <Route path="/admin_off_shelf_book_list" component={AdminOffShelfBookList} />
          
        </IonRouterOutlet>
        {/* {this.props.goDesktop ? <></> : (this.props.isReading ? <BookContentBottom book_id={this.props.chapterContent[0].book_id} chapterId={this.props.chapterContent[0].chapter_id} chapter={this.props.chapterContent[0].chapter}/> : <BottomBar />)} */}
        {this.props.goDesktop ? <></> : <BottomBar />}

        </IonReactRouter>
      </IonApp>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    email: state.auth.user?.email || null,
    goDesktop: state.desktop.goDesktop,
  };
};

const mapDispatchToProps = (dispatch: RootDispatch) => {
  return {
    checkToken: () => dispatch(checkTokenThunk()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);