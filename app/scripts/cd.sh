#!/usr/bin/bash
set -e
set -o pipefail
set -x

npm run build
scp -r ./build skywalk:~/app