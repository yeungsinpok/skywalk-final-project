#!/usr/bin/bash
set -e
set -o pipefail
set -x

npm run build
npx cap sync android
npx cap open android
