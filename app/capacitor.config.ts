import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.skywalknovel.app',
  appName: 'skywalk',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
